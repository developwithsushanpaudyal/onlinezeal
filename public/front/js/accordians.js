jQuery(function($){
	var collapse = function(opt) {
		var $target = $(this).closest('.panel-heading').next();
		if ( $target.data('bs.collapse') ) {
			$target
			.collapse('toggle');
		} else {
			$target
			// Toggle active class to the element
			.on('show.bs.collapse hide.bs.collapse', function(e){
				var panel = $(e.target).closest('.panel');
				if ( e.type == 'hide')
					panel
					.removeClass( 'panel-active' );
				else if ( e.type == 'show' )
					panel
					.addClass( 'panel-active' )
					
				if ( $(this).data( 'toggle' ) == 'collapse-parent-next' )
					panel
					.siblings()
					.removeClass( 'panel-active' );
			})
			.collapse(opt);
		}
	}
	
	$('body')
	// This one collapse items as an "accordion"
	.on('click.collapse-parent-next.data-api', '[data-toggle=collapse-parent-next]', function (e) {
		e.preventDefault();

		collapse
		.apply(this, [
			{
				'parent': $(this).closest('.panel-group')
			}
		]);
	})
	// This one collapse items separately without closing the others
	.on('click.collapse-next.data-api', '[data-toggle=collapse-next]', function (e) {
		e.preventDefault();

		collapse
		.apply(this);
	})
});
