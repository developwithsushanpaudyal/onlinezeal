 
    
    
    
    <!-- ++++ footer ++++ -->
    <footer id="footer">
        <!--scroll to top-->
        <a class="top-btn page-scroll" href="#top">
            <span class="icon-chevron-up b-clor regular-text text-center"></span>
        </a>
        <!--end scroll to top-->
        <!--newsletter section-->
     
        <!--end newsletter section-->
        <!--footer content-->
        <div class="light-ash-bg">
            <div class="container">
                <ul>
                    <li>
                        <!-- footer left content-->
                        <ul>
                            <li>
                                <a href="index.php">
                                    <img src="images/logo-footer.png" alt="logo" class="img-responsive logo">
                                </a>
                            </li>
                            <li>
                                <p class="extra-small-text">&copy; 2018</p>
                            </li>
                            <li>
                                <p class="extra-small-text">OnlineZeal PVT. LTD</p>
                            </li>
                            <li>
                                <p class="extra-small-text">Tinkune,Kathmandu</p>
                            </li>
                        </ul>
                        <!--end footer left content-->
                    </li>
                    <li>
                        <!--footer service list-->
                        <ul>
                            <li>
                                <a class="regular-text text-color-light">Services  </a>
                            </li>
                            <li>
                                <a href="Web-design.php" class="extra-small-text">Web Design</a>
                            </li>
                            <li>
                                <a href="graphic-design.php" class="extra-small-text">Graphic Design</a>
                            </li>
                            <li>
                                <a href="mobile-app-development.php" class="extra-small-text"> Software  Development</a>
                            </li>
                            <li>
                                <a href="social-media-marketing.php" class="extra-small-text">Social Media Marketing</a>
                            </li>
                            <!-- <li>
                                <a href="services.php#bm-business" class="extra-small-text">Business</a>
                            </li> -->
                            <li>
                                <a href="digital-strategy.php" class="extra-small-text">NetWork Security</a>
                            </li>
                        </ul>
                        <!--end footer service list-->
                    </li>
                    <li>
                        <!--footer Resources list-->
                        <ul>
                            <li>
                                <a class="regular-text text-color-light">Resources</a>
                            </li>
                            <li>
                                <a href="portfolio.php" class="extra-small-text">Portfolio</a>
                            </li>
                            <li>
                                <a href="about.php" class="extra-small-text">About Us</a>
                            </li>
                            <!-- <li>
                                <a href="blog.php" class="extra-small-text">Blog</a>
                            </li> -->
                        </ul>
                        <!--end footer Resources list-->
                    </li>
                    <li>
                        <!--footer Support list-->
                        <ul>
                            <li>
                                <a class="regular-text text-color-light">Support</a>
                            </li>
                            <li>
                                <a href="contact.php" class="extra-small-text">Contact</a>
                            </li>
                            <!-- <li>
                                <a href="privacy-policy.php" class="extra-small-text">Privacy Policy</a>
                            </li> -->
                            <!-- <li>
                                <a href="terms-conditions.php" class="extra-small-text">Terms &amp; Conditions</a>
                            </li> -->
                        </ul>
                        <!--end footer Support list-->
                    </li>
                    <li class="big-width">
                        <!--footer social media list-->
                        <ul class="list-inline">
                            <li>
                                <p class="regular-text text-color-light">Get in Touch</p>
                                <ul class="social-links">
                                    <li>
                                        <a href="https://www.facebook.com/onlinezeal/">
                                            <span class="icon-facebook"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-twitter"></span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <span class="icon-linkedin"></span>
                                        </a>
                                    </li>
                                    <!-- <li>
                                        <a href="#">
                                            <span class="icon-google-plus"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-instagram"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-pinterest"></span>
                                        </a>
                                    </li> -->
                                    
                                </ul>
                            </li>
                        </ul>
                        <!--end footer social media list-->
                    </li>
                </ul>
            </div>
        </div>
        <!--end footer content-->
    </footer>
    <!--end footer-->


    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

      <!-- ++++ Javascript libraries ++++ -->
    <!--js library of jQuery-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!--js library of bootstrap-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!--js library for number counter-->
    <script src="js/waypoints.min.js"></script>
    <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
    <!--js library for video popup-->
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <!-- js library for owl carousel -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <!--modernizer library-->
    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/isotope.min.js">
    </script>
    <!-- Slider Revolution core JavaScript files -->
    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Slider Revolution extension scripts. Remove these scripts before uploading 
    to server -->
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <!--custom js-->
    <script type="text/javascript" src="js/script.js"></script>
</body>

</html>