<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>OnlineZeal :: Next Generation, Majestic Innovation</title>
    <!-- description -->
    <meta name="description" content="Best IT company in Nepal">
    <!-- keywords -->
    <meta name="keywords" content="css3, html5, bootstrap">
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <!-- ++++ favicon ++++ -->
    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
    <!-- <link rel="icon" type="image/png" sizes="96x96" href="icon/favicon-96x96.png">
    <link rel="apple-touch-icon" sizes="57x57" href="icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-touch-icon-114x114.png"> -->
    <!-- ++++ bootstrap ++++ -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- ++++ owl carousel ++++ -->
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css" type="text/css">
    <!-- ++++ magnific-popup  ++++ -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- ++++ font Icon ++++ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fonts.css" />
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
    <!-- ++++ style ++++ -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- responsive css -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- [if IE]>
        <script src="js/html5shiv.js"></script>
    <![endif]  -->
</head>

<body>
    <?php $var= "http://localhost/onlinezeal/index.php"; 
     $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if($var==$actual_link){?>
    <div class="header-wrapper1">
    <?php } else{ ?>
        <div class="header-wrapper">
   <?php } ?>
   <div class="headerr-wrapper">
        <!-- most top information -->
        <header id="top">
            <div class="container">
                <div class="form-element hidden-xs pull-left">
                    <form action="#" method="get" class="form-inline">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <button type="submit" class="search-btn">
                            <i class="icon-magnifier"></i>
                        </button>
                    </form>
                </div>
                <!-- End of .form-element -->
                <div class="contact-info clearfix">
                    <ul class="pull-right list-inline">
                        <li>
                            <a href="tel:012.345.1234">
                                <i class="icon-telephone"></i>01-5199625 , 9801079608</a>
                        </li>
                        <li>
                            <a href="mailto:info@company.com">
                                <i class="icon-envelope"></i>contact@onlinezeal.com</a>
                        </li>
                    </ul>
                </div>
                <!-- End of .contact-info -->
            </div>
        </header>
        <!-- end most top information -->
        <!--navigation-->
        <div class="container no-padding">
            <nav id="navbar-main" class="navbar main-menu">
                <!--Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">
                        <img src="images/logo.png" alt="Brand" class="img-responsive" />
                    </a>
                </div>
                <!--Collect the nav links, and other content for toggling-->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown not-relative">
                            <a href="services.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <i class="fa fa-chevron-down text-muted"></i>  </a>
                            <div class="dropdown-menu dropdown-megamenu">
                                <ul class="megamenu">
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Design</li>
                                            <!-- <li>
                                                <a href="logo-and-branding.php">
                                                    <i class="icon-palette"></i> Logo &amp; Branding</a>
                                            </li> -->
                                            <li>
                                                <a href="website-design.php">
                                                    <i class="icon-laptop-phone"></i> Web Design</a>
                                            </li>
                                            <!-- <li>
                                                <a href="mobile-app-design.php">
                                                    <i class="icon-phone"></i> Mobile App Design</a>
                                            </li> -->
                                            <li>
                                                <a href="graphic-design.php">
                                                <i class="icon-palette"></i> Graphic Design</a>
                                            </li>
                                            <!-- <li>
                                                <a href="video-production.php">
                                                    <i class="icon-camera"></i> Video Production</a>
                                            </li> -->
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Development</li>
                                            
                                            <li>
                                                <a href="mobile-app-development.php">
                                                    <i class="icon-smartphone-embed"></i> Software development</a>
                                            </li>

                                            <li>
                                                <a href="content-management-system.php">
                                                    <i class="icon-papers"></i> Content Management System</a>
                                            </li>
                                            <!-- <li>
                                                <a href="ecommerce.php">
                                                    <i class="icon-cart"></i> eCommerce</a>
                                            </li> -->
                                            <!-- <li>
                                                <a href="ecommerce.php">
                                                <i class="icon-register"></i> Inventory Management System</a>
                                            </li> -->
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Online Marketing</li>
                                            <li>
                                                <a href="social-media-marketing.php">
                                                    <i class="icon-share"></i> Social Media Marketing</a>
                                            </li>
                                            <li>
                                                <a href="content-marketing.php">
                                                    <i class="icon-news"></i> Content marketing</a>
                                            </li>

                                            <!-- <li>
                                                <a href="content-marketing.php">
                                                    <i class="icon-news"></i> E-mail Marketing </a>
                                            </li> -->
                                           
                                            <!-- <li>
                                                <a href="search-engine-optimization.php">
                                                    <i class="icon-magnifier"></i> Digital Marketing</a>
                                            </li> -->

                                             <li>
                                                <a href="search-engine-optimization.php">
                                                    <i class="icon-magnifier"></i> Search Engine Optimization</a>
                                            </li>


                                            <!-- <li>
                                                <a href="pay-per-click.php">
                                                    <i class="icon-select2"></i> Pay-Per-Click</a>
                                            </li> --> 
                                             <!-- <li>
                                                <a href="email-marketing.php">
                                                    <i class="icon-envelope-open"></i> Email Marketing</a>
                                            </li> -->
                                           
                                            
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Technology</li>
                                            <li>
                                                <a href="digital-strategy.php">
                                                    <i class="icon-chart-settings"></i> Network Security</a>
                                            </li>
                                            <!-- <li>
                                                <a href="business-consulting.php">
                                                    <i class="icon-bubble-user"></i> Business Consulting</a>
                                            </li>
                                            <li>
                                                <a href="content-writing.php">
                                                    <i class="icon-register"></i> Content Writing</a>
                                            </li>
                                            <li>
                                                <a href="reporting.php">
                                                    <i class="icon-chart-growth"></i> Reporting</a>
                                            </li> -->
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Web Hosting</li>
                                            <!-- <li>
                                                <a href="domain.php">
                                                    <i class="icon-magnifier"></i> Domain Registration </a>
                                            </li> -->
                                            <li>
                                                <a href="hosting.php">
                                                    <i class="icon-server"></i> Web Hosting</a>
                                            </li>
                                            <!-- <li>
                                                <a href="big-data-analysis.php">
                                                    <i class="icon-pie-chart"></i> Reseller</a>
                                            </li> -->
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                </ul>
                            </div>
                            <!-- End of .dropdown-menu -->
                        </li>
                        <li>
                            <a href="portfolio.php">Portfolio</a>
                        </li>
                      


                        
                        
                        <!-- <li>
                            <a href="case-studies.php">Case Studies</a>
                        </li> -->
                        <li class="dropdown">
                            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <i class="fa fa-chevron-down text-muted"></i>  </a>
                            <div class="dropdown-menu">
                                <ul class="megamenu">
                                    <li>
                                        <a href="about.php">Overview</a>
                                    </li>
                                    <!-- <li>
                                        <a href="team.php">Team</a>
                                    </li> -->
                                    <!-- <li>
                                        <a href="awards-and-recognitions.php">Awards &amp; Recognition</a>
                                    </li> -->
                                    <!-- <li>
                                        <a href="career.php">Career</a>
                                    </li> -->
                                    <!-- <li>
                                        <a href="customer-reviews.php">Customer Reviews</a>
                                    </li> -->
                                    <!-- <li>
                                        <a href="faqs.php">FAQs</a>
                                    </li> -->
                                </ul>
                                <!-- End of .dropdown-menu -->
                            </div>
                        </li>
                        <li>
                            <a href="pricing.php">Pricing</a>
                        </li>
                        <!-- <li class="dropdown">
                            <a href="blog.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog</a>
                            <div class="dropdown-menu">
                                <ul class="megamenu">
                                    <li>
                                        <a href="blog.php">Blog Version 1</a>
                                    </li>
                                    <li>
                                        <a href="blog-version-2.php">Blog Version 2</a>
                                    </li>
                                </ul>
                            </div> -->
                            <!-- End of .dropdown-menu -->
                        <!-- </li> -->

                        <!-- <li>
                            <a href="career.php">Career</a>
                        </li> -->

                        <li>
                            <a href="contact.php">Contact Us</a>
                        </li>
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other <i class="fa fa-chevron-down text-muted"></i>  </a>
                            <div class="dropdown-menu">
                                <ul class="megamenu">
                                    <li>
                                        <a href="404-error.php">404 Error Page</a>
                                    </li>
                                    <li>
                                        <a href="search-results.php">Search Results</a>
                                    </li>
                                    <li>
                                        <a href="under-construction.php">Under Construction</a>
                                    </li>
                                    <li>
                                        <a href="terms-conditions.php">Terms &amp; Conditions</a>
                                    </li>
                                    <li>
                                        <a href="privacy-policy.php">Privacy Policy</a>
                                    </li>
                                </ul>
                            </div>
                        </li>  -->
                    
                        <!-- <li class="menu-btn" data-toggle="modal" data-target="#getAQuoteModal">
                            <a class="btn btn-fill" href="#">GET A QUOTE
                                <span class="icon-chevron-right"></span>
                            </a>
                        </li> -->

                    </ul>
                </div>
            </nav>
            <!--end navigation-->
        </div>
    </div>
    <!-- End of .header-wrapper -->