<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
	'uses' => 'FrontEndController@index',
	'as' => 'index'
]);
Auth::routes();

Route::get('/about-us', 'FrontEndController@about')->name('about');
Route::get('/team','TeamController@index')->name('team');
Route::get('/demo','DemoController@index')->name('demo');
Route::get('/blog','BlogController@index')->name('blog');
Route::get('/contact-us', 'FrontEndController@contact')->name('contact');
Route::get('/carrer', 'FrontEndController@carrer')->name('carrers');
Route::get('/pricing', 'FrontEndController@price')->name('price');
Route::get('/services/{slug}', 'FrontEndController@servicesSingle')->name('services.single');
//Blog
Route::get('/blog','FrontEndController@blog')->name('blog');
Route::get('/blogcatdetail/{slug}','FrontEndController@blogcatdetail')->name('blogcatdetail');
Route::get('/blogdetail/{slug}','FrontEndController@blogdetail')->name('blogdetail');

Route::get('/demo','FrontEndController@demo')->name('demo');

Route::get('/portfolios', 'FrontEndController@allportfolio')->name('portfolio.single');
Route::get('/hosting', 'FrontEndController@hosting')->name('hosting');

Route::get('/portfolio/schoolprojects', 'FrontEndController@schoolProjects')->name('schoolprojects');

Route::post('/contactSend','FrontEndController@sendMail')->name('email.send');

Route::post('/quoteSend','FrontEndController@sendQuote')->name('quote.send');
Route::get('/termsandconditions', 'FrontEndController@terms')->name('termsfront');
Route::get('/privacyandpolicy', 'FrontEndController@privacy')->name('privacyfront');

Route::post('/email/send', 'MailController@email')->name('emailme');


Route::post('/getaquote','GetaquoteController@store')->name('getaquote.store');

Route::post( '/search', function () {
    $q = Input::get ( 'q' );
    if($q != ""){
        $service = App\Services::where ( 'title', 'LIKE', '%' . $q . '%' )->orWhere ( 'content', 'LIKE', '%' . $q . '%' )->get ();
        $titlepage = App\Titlepage::all();
        if (count ( $service ) > 0)
            return view ( 'frontend.search' )->withDetails ( $service )->withQuery ( $q )->with('titlepage',$titlepage);
        else
            return view ( 'frontend.search' )->withMessage ( 'Can’t find the thing you’re looking for? Let us help you!' )->with('titlepage',$titlepage);
    }
    return view ( 'frontend.search' )->withMessage ( 'Can’t find the thing you’re looking for? Let us help you!' );
} )->name('search');

Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});







Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

  Route::get('/home', 'HomeController@index')->name('home');
// Slider Routes
Route::get('/slider/create', 'SlidersController@create')->name('slider.create');
Route::post('/slider/store', 'SlidersController@store')->name('slider.store');
Route::get('/slider/index', 'SlidersController@index')->name('slider.index');
Route::post('/slider/update/{id}', 'SlidersController@update')->name('slider.update');
Route::get('/slider/edit/{id}', 'SlidersController@edit')->name('slider.edit');
Route::get('/slider/delete/{id}', 'SlidersController@destroy')->name('slider.delete');

//Title Page Routes
Route::get('/titlepage/index', 'TitlePageController@index')->name('titlepage.index');
Route::get('/titlepage/create', 'TitlePageController@create')->name('titlepage.create');
Route::post('/titlepage/store', 'TitlePageController@store')->name('titlepage.store');
Route::get('/titlepage/edit/{id}', 'TitlePageController@edit')->name('titlepage.edit');
Route::post('/titlepage/update/{id}', 'TitlePageController@update')->name('titlepage.update');
Route::get('/titlepage/delete/{id}', 'TitlePageController@delete')->name('titlepage.delete');


// ServicesCategories
Route::get('/servicescategory/index', 'ServicesCategoryController@index')->name('category.index');
Route::get('/servicescategory/create', 'ServicesCategoryController@create')->name('category.create');
Route::post('/servicescategory/store', 'ServicesCategoryController@store')->name('category.store');
Route::get('/servicescategory/edit/{id}', 'ServicesCategoryController@edit')->name('category.edit');
Route::post('/servicescategory/update/{id}', 'ServicesCategoryController@update')->name('category.update');
Route::get('/servicescategory/delete/{id}', 'ServicesCategoryController@destroy')->name('category.delete');

// Services
Route::get('/services/index', 'ServicesController@index')->name('service.index');
Route::get('/services/create', 'ServicesController@create')->name('service.create');
Route::post('/services/store', 'ServicesController@store')->name('service.store');
Route::get('/services/edit/{id}', 'ServicesController@edit')->name('service.edit');
Route::post('/services/update/{id}', 'ServicesController@update')->name('service.update');
Route::get('/services/delete/{id}', 'ServicesController@destroy')->name('service.delete');




// Processes Routes
Route::get('/process/index', 'ProcessController@index')->name('process.index');
Route::get('/process/create', 'ProcessController@create')->name('process.create');
Route::post('/process/store', 'ProcessController@store')->name('process.store');
Route::get('/process/edit/{id}', 'ProcessController@edit')->name('process.edit');
Route::post('/process/update/{id}', 'ProcessController@update')->name('process.update');
Route::get('/process/delete/{id}', 'ProcessController@destroy')->name('process.delete');

// Portfolio Category Routes
Route::get('/portfolicategory/index', 'PortfolioCategoryController@index')->name('portfoliocategory.index');
Route::get('/portfolicategory/create', 'PortfolioCategoryController@create')->name('portfolicategory.create');
Route::post('/portfolicategory/store', 'PortfolioCategoryController@store')->name('portfoliocategory.store');
Route::get('/portfolicategory/edit/{id}', 'PortfolioCategoryController@edit')->name('portfoliocategory.edit');
Route::post('/portfolicategory/update/{id}', 'PortfolioCategoryController@update')->name('portfoliocategory.update');
Route::get('/portfolicategory/delete/{id}', 'PortfolioCategoryController@destroy')->name('portfoliocategory.delete');

// Category Routes
Route::get('/portfolio/index', 'PortfolioController@index')->name('portfolio.index');
Route::get('/portfolio/create', 'PortfolioController@create')->name('portfolio.create');
Route::post('/portfolio/store', 'PortfolioController@store')->name('portfolio.store');
Route::get('/portfolio/edit/{id}', 'PortfolioController@edit')->name('portfolio.edit');
Route::post('/portfolio/update/{id}', 'PortfolioController@update')->name('portfolio.update');
Route::get('/portfolio/delete/{id}', 'PortfolioController@destroy')->name('portfolio.delete');

//Demo
Route::get('/demo/index', 'DemoController@index')->name('demo.index');
Route::get('/demo/create', 'DemoController@create')->name('demo.create');
Route::post('/demo/store', 'DemoController@store')->name('demo.store');
Route::get('/demo/edit/{id}', 'DemoController@edit')->name('demo.edit');
Route::post('/demo/update/{id}', 'DemoController@update')->name('demo.update');
Route::get('/demo/delete/{id}', 'DemoController@destroy')->name('demo.delete');

// Success Routes
Route::get('/success', 'SuccessController@index')->name('success.index');
Route::get('/success/edit/{id}', 'SuccessController@edit')->name('success.edit');
Route::post('/success/update/{id}', 'SuccessController@update')->name('success.update');

// Clients Routes
Route::get('/clients/create', 'ClientsController@create')->name('clients.create');
Route::get('/clients/index', 'ClientsController@index')->name('clients.index');
Route::post('/clients/store', 'ClientsController@store')->name('clients.store');
Route::get('/clients/edit/{id}', 'ClientsController@edit')->name('clients.edit');
Route::post('/clients/update/{id}', 'ClientsController@update')->name('clients.update');
Route::get('/client/delete/{id}', 'ClientsController@destroy')->name('clients.delete');

//Blogs
Route::get('/blog/index', 'BlogController@index')->name('blog.index');
Route::get('/blog/create', 'BlogController@create')->name('blog.create');
Route::post('/blog/store', 'BlogController@store')->name('blog.store');
Route::get('/blog/edit/{id}', 'BlogController@edit')->name('blog.edit');
Route::post('/blog/update/{id}', 'BlogController@update')->name('blog.update');
Route::get('/blog/delete/{id}', 'BlogController@destroy')->name('blog.delete');

//BlogsCategories
Route::get('/blogcategory/index', 'BlogsCategoryController@index')->name('blogcategory.index');
Route::get('/blogcategory/create', 'BlogsCategoryController@create')->name('blogcategory.create');
Route::post('/blogcategory/store', 'BlogsCategoryController@store')->name('blogcategory.store');
Route::get('/blogcategory/edit/{id}', 'BlogsCategoryController@edit')->name('blogcategory.edit');
Route::post('/blogcategory/update/{id}', 'BlogsCategoryController@update')->name('blogcategory.update');
Route::get('/blogcategory/delete/{id}', 'BlogsCategoryController@destroy')->name('blogcategory.delete');

//BlogsTags
Route::get('/blogtag/index', 'BlogsTagController@index')->name('blogtag.index');
Route::get('/blogtag/create', 'BlogsTagController@create')->name('blogtag.create');
Route::post('/blogtag/store', 'BlogsTagController@store')->name('blogtag.store');
Route::get('/blogtag/edit/{id}', 'BlogsTagController@edit')->name('blogtag.edit');
Route::post('/blogtag/update/{id}', 'BlogsTagController@update')->name('blogtag.update');
Route::get('/blogtag/delete/{id}', 'BlogsTagController@destroy')->name('blogtag.delete');


//About Us Controller
Route::get('/about/index', 'AboutController@index')->name('about.index');
Route::post('/about/store', 'AboutController@store')->name('about.store');
Route::get('/about/edit/{id}', 'AboutController@edit')->name('about.edit');
Route::post('/about/update/{id}', 'AboutController@update')->name('about.update');


//Teams
Route::get('/team/create','TeamController@create')->name('team.create');
Route::post('/team/create','TeamController@store')->name('team.save');

Route::get('/team/index','TeamController@index')->name('team.index');
Route::get('/team/edit/{id}','TeamController@edit')->name('team.edit');
Route::post('/team/update/{id}','TeamController@update')->name('team.update');
Route::get('/team/view','TeamController@view')->name('team.view');
Route::get('/team/delete/{id}','TeamController@delete')->name('team.delete');

//Designation
Route::get('/designation/create','DesignationController@create')->name('designation.create');
Route::get('/designation/view','DesignationController@view')->name('designation.view');
Route::get('/designation/edit/{id}','DesignationController@edit')->name('designation.edit');
Route::post('/designation/update/{id}','DesignationController@update')->name('designation.update');
Route::get('/designation/delete/{id}','DesignationController@delete')->name('designation.delete');
Route::post('/designation/store','DesignationController@store')->name('designation.store');

//Demo
Route::get('/demo/create','DemoController@create')->name('demo.create');
Route::post('/demo/create','DemoController@store')->name('demo.save');

Route::get('/demo/index','  DemoController@index')->name('demo.index');
Route::get('/demo/edit/{id}','DemoController@edit')->name('demo.edit');
Route::get('/demo/update','DemoController@update')->name('demo.update');
Route::get('/demo/view','DemoController@view')->name('demo.view');
Route::get('/demo/delete','DemoController@delete')->name('demo.delete');

//Contact US Controller
Route::get('/contact/index', 'ContactsController@index')->name('contact.index');
Route::get('/contact/edit/{id}', 'ContactsController@edit')->name('contact.edit');
Route::post('/contact/update/{id}', 'ContactsController@update')->name('contact.update');

// Package Routes
Route::get('/package/create', 'PackageController@create')->name('package.create');
Route::get('/package/index', 'PackageController@index')->name('package.index');
Route::post('/package/store', 'PackageController@store')->name('package.store');
Route::get('/package/edit/{id}', 'PackageController@edit')->name('package.edit');
Route::post('/package/update/{id}', 'PackageController@update')->name('package.update');
Route::get('/package/delete/{id}', 'PackageController@destroy')->name('package.delete');

// Hosting Routes
Route::get('/hosting/create', 'HostingController@create')->name('hosting.create');
Route::get('/hosting/index', 'HostingController@index')->name('hosting.index');
Route::post('/hosting/store', 'HostingController@store')->name('hosting.store');
Route::get('/hosting/edit/{id}', 'HostingController@edit')->name('hosting.edit');
Route::post('/hosting/update/{id}', 'HostingController@update')->name('hosting.update');
Route::get('/hosting/delete/{id}', 'HostingController@destroy')->name('hosting.delete');

//Profile
    Route::get('/profile', 'ProfilesController@profile')->name('profile');
    Route::post('/profile/update', 'ProfilesController@update')->name('userprofileupdate');

    //Vacancy
    Route::get('/vacancy/create', 'VacancyController@create')->name('vacancy.create');
    Route::get('/vacancy/index', 'VacancyController@index')->name('vacancy.index');
    Route::post('/vacancy/store', 'VacancyController@store')->name('vacancy.store');
    Route::get('/vacancy/edit/{id}', 'VacancyController@edit')->name('vacancy.edit');
    Route::post('/vacancy/update/{id}','VacancyController@update')->name('vacancy.update');
    Route::get('/vacancy/delete/{id}','VacancyController@destroy')->name('vacancy.delete');

    Route::get('/carrer/index','CarrerController@index')->name('carrer');
    Route::get('/carrer/edit/{id}', 'CarrerController@edit')->name('carrer.edit');
    Route::post('/carrer/update/{id}','CarrerController@update')->name('carrer.update');

//    School Projects
    Route::get('/school/create','SchoolsController@create')->name('school.create');
    Route::post('/school/store','SchoolsController@store')->name('school.store');
    Route::get('/school/index','SchoolsController@index')->name('school.index');
    Route::get('/school/edit/{id}','SchoolsController@edit')->name('school.edit');
    Route::post('/school/update/{id}','SchoolsController@update')->name('school.update');
    Route::get('/school/delete/{id}','SchoolsController@destroy')->name('school.delete');

//    SEO Settings
    Route::get('/seosettings', 'SeoController@seo')->name('seosettings');
//Terms and Conditions
    Route::get('/termsandconditions', 'TermsController@index')->name('termsandconditions');
    Route::get('/termsandconditions/edit/{id}', 'TermsController@edit')->name('terms.edit');
    Route::post('/terms/update/{id}', 'TermsController@update')->name('terms.update');

    Route::get('/privacypolicy', 'PrivacyController@index')->name('privacypolicy');
    Route::get('/privacy/edit/{id}', 'PrivacyController@edit')->name('privacy.edit');
    Route::post('/privacy/update/{id}', 'PrivacyController@update')->name('privacy.update');

    Route::get('/messages', 'MailController@show')->name('email.show');

    Route::get('/quotes/view', 'GetaquoteController@view')->name('getaquote.view');


    




});
