<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGetaquotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('getaquotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('useremail');
            $table->string('userphone');
            $table->string('userwebsite');
            $table->text('usermessage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('getaquotes');
    }
}
