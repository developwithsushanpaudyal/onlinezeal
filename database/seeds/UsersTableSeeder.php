<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user =  App\User::create([
            'name' => 'Online Zeal',
            'email' => 'contact@onlinezeal.com',
            'password' => bcrypt('password'),
            'admin' => 1,
        ]);
        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'public/uploads/avatars/avatar.png',
            'about' => 'Company',
            'facebook' => 'facebook',
            'linkedin' => 'linkedin'
        ]);
    }
}
