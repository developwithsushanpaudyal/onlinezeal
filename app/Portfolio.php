<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = ['image', 'name', 'link', 'categories', 'description'];

   // public function portfoliocategories(){
   //     return $this->belongsToMany('App\Category', 'portfolio_categories')->withTimestamps();
   // }
}
