<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titlepage extends Model
{
    protected $fillable = ['name','content'];
}
