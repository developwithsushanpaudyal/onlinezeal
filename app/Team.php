<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
   
 protected $fillable = ['name', 'designation', 'image', 'description', 'sort_id','facebook', 'twitter','instagram', 'pinterest','linkedin'];
 
    public function designation(){
       return $this->belongsTo('App\Designation'); 
    }
}

