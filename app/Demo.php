<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $fillable = ['name','subname','description','slug','image','link']; 

        public function demo(){

        return $this->belongsTo('App\portfolio');
      
    }

    
}
