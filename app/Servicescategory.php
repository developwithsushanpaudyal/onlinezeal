<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicescategory extends Model
{
    protected $fillable = ['name', 'icon', 'description'];

    public function services(){
    	return $this->hasMany('App\Services');
    }
}
