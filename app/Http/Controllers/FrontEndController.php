<?php

namespace App\Http\Controllers;

use App\Package;
use App\Positions;
use App\School;
use Illuminate\Http\Request;
use App\Clients;
use App\Success;
use App\Blog;
use App\Blogscategory;
use App\Portfolio;
use App\Category;
use App\Sliders;
use App\About;
use App\contact;
use App\Services;
use App\Servicescategory;
use App\Hosting;
use App\carrer;
use App\Terms;
use App\Privacy;
use App\Titlepage;
use Session;
use Mail;

class FrontEndController extends Controller
{
    public function index(){
      return view('frontend.index')
      ->with('clients', Clients::all())
      ->with('success', Success::all())
      ->with('schools',School::all())
      ->with('titlepage',Titlepage::all())
      ->with('portfolios', Portfolio::all())
      ->with('portfoliocategories', Category::all())
      ->with('sliders', Sliders::where('id', 1)->get())
          ->with('sliders2', Sliders::where('id', 2)->get())
          ->with('services', Services::all())
          ->with('servicescategories', Servicescategory::all())
          ->with('services1', Services::where('servicescategory_id', 1)->get())
          ->with('services2', Services::where('servicescategory_id', 2)->get())
          ->with('services3', Services::where('servicescategory_id', 3)->get())
          ->with('services4', Services::where('servicescategory_id', 4)->get())
          ->with('services5', Services::where('servicescategory_id', 5)->get())
          ->with('categories1', Servicescategory::where('id', 1)->get())
          ->with('categories2', Servicescategory::where('id', 2)->get())
          ->with('categories3', Servicescategory::where('id', 3)->get())
          ->with('categories4', Servicescategory::where('id', 4)->get())
        ->with('categories5', Servicescategory::where('id', 5)->get())
          ->with('servicesfooter', Services::take(5)->get());


    }

    public function servicesSingle($slug){
        $service = Services::where('slug', $slug)->first();
        return view('frontend.services-single')
      ->with('service', $service)
      ->with('services1', Services::where('servicescategory_id', 1)->get())
      ->with('services2', Services::where('servicescategory_id', 2)->get())
      ->with('services3', Services::where('servicescategory_id', 3)->get())
      ->with('services4', Services::where('servicescategory_id', 4)->get())
      ->with('services5', Services::where('servicescategory_id', 5)->get())
      ->with('titlepage',Titlepage::all());
    }

    
    public function blog(){
        $blogcategory = Blogscategory::all();
        $blog = Blog::all();
            return view('frontend.blog')->with(compact('blog','blogcategory'))->with('titlepage',Titlepage::all());
    } 

    public function blogcatdetail($slug){
        $blogcategory = Blogscategory::where('slug',$slug)->get();
        $blogs = Blog::all();
            return view('frontend.blogcatdetail')->with(compact('blogs','blogcategory'))->with('titlepage',Titlepage::all());

    }

    public function blogdetail($slug){

        $blogDetails = Blog::where('slug',$slug)->first();
        $blogs = Blog::inRandomOrder()->take(3)->get();
        return view('frontend.blogdetail')->with(compact('blogDetails','blogs'))->with('titlepage',Titlepage::all());
    } 

      

   


    public function about(){
        $about = About::first();
        return view ('frontend.about')->with('about', $about)
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('titlepage',Titlepage::all());
    }

    public function contact(){
        $contact = contact::first();
        return view ('frontend.contact')->with('contact', $contact)
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('titlepage',Titlepage::all());
    }

    public function price(){
        return view ('frontend.price')
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('packages', Package::all())
            ->with('titlepage',Titlepage::all());
    }

    public function allportfolio(){
        return view('frontend.portfolioall')
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('portfolios', Portfolio::all())
            ->with('portfoliocategories', Category::all())
            ->with('titlepage',Titlepage::all());

    }

    public function hosting(){
        return view ('frontend.hosting')
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('hostings', Hosting::all())
            ->with('titlepage',Titlepage::all());
    }


    public function carrer(){
        return view('frontend.carrer')
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('positions', Positions::all())
            ->with('carrer' , carrer::first())
            ->with('titlepage',Titlepage::all());
    }

    public function schoolProjects(){
        return view ('frontend.schoolprojects')
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('schools1', School::where('category', 'Government Schools')->get())
            ->with('schools2', School::where('category', 'Others')->get())
            ->with('titlepage',Titlepage::all());

    }



    public function sendMail(Request $request){

//         $user = $request->all();

        $user['name'] = $request->input('name');
        $user['email'] = $request->input('email');
        $user['phone'] = $request->input('phone');
        $user['address'] = $request->input('address');
        $user['msg'] = $request->input('message');
        // return $user['name'];

        mail::send('frontend.includes.emailtemplate', $user, function($message) use ($user){

            $message->from($user['email']);

            $message->to('contactonlinezeal@gmail.com');

            $message->subject('Mail from Contact Us Page');

        });

        return redirect('/contact-us')->with('success','Your Mail has been send Successfully')->with('titlepage',Titlepage::all());

    }


    public function sendQuote(Request $request){


        $user['full_name'] = $request->input('full_name');
        $user['email'] = $request->input('email');
        $user['phone'] = $request->input('phone');
        $user['address'] = $request->input('address');
        $user['msg'] = $request->input('msg');
        // return $user['name'];

        mail::send('frontend.includes.quotetemplate', $user, function($message) use ($user){

            $message->from($user['email']);

            $message->to('contactonlinezeal@gmail.com');

            $message->subject('Get The Quote Message');

        });

        return redirect('/about-us')->with('success','Your Mail has been send Successfully')->with('titlepage',Titlepage::all());

    }

    public function terms(){
        $service = Services::all();

        return view ('frontend.terms')
            ->with('service', $service)
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('terms', Terms::first())
            ->with('titlepage',Titlepage::all());
    }

    public function privacy(){
        $service = Services::all();
        return view ('frontend.privacy')
            ->with('service', $service)
            ->with('services1', Services::where('servicescategory_id', 1)->get())
            ->with('services2', Services::where('servicescategory_id', 2)->get())
            ->with('services3', Services::where('servicescategory_id', 3)->get())
            ->with('services4', Services::where('servicescategory_id', 4)->get())
            ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('privacy', Privacy::first())
            ->with('titlepage',Titlepage::all());
    }

}
