<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clients;
use File;
use Session;

class ClientsController extends Controller
{
  public function create()
{
  $clients = Clients::all();
  return view('admin.clients.create')->with('clients', Clients::all());
}

public function store(Request $request)
	{
		$this->validate($request,[
			'title' => 'required',
			'links' => 'required',
		]);
		if($request->hasFile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('uploads/clients/', $filename);
		}
		$teams = Clients::create([
			'title' => $request->title,
			'image' => 'uploads/clients/'. $filename,
			'links' => $request->links,
		]);
		Session::flash('success', 'Client Was Inserted Successfully');
		return redirect()->route('clients.index');
	}

  public function index()
	{
		return view('admin.clients.index')->with('clients', Clients::all());
	}

  public function edit($id)
	{
		$clients = Clients::find($id);
		return view('admin.clients.edit')->with('clients',$clients);
	}
	public function update(Request $request, $id)
	{
		$clients = Clients::find($id);

		if($request->hasFile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('uploads/clients/', $filename);
			$clients->image = 'uploads/clients/'.$filename;
		}
		$clients->title = $request->title;
		$clients->links = $request->links;
		$clients->save();
		Session::flash('success', 'Clients Updated Successfully');
		return redirect()->route('clients.index');
	}


  public function destroy($id)
	{
		$clients = Clients::find($id);
		$image_path = $clients->image;
		if(File::exists($image_path)){
			File::delete($image_path);
		}
		$clients->delete();
		Session::flash('delete', 'Teams Deleted Successfully');
		return redirect()->route('clients.index');
	}
}
