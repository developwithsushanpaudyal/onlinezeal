<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contact;
use Session;
class ContactsController extends Controller
{
    public function index(){
        return view('admin.contact.index')->with('contact', Contact::all());
    }

    public function edit($id)
    {
        $contact =  contact::find($id);
        return view('admin.contact.edit')->with('contact',$contact);
    }


    public function update(Request $request, $id)
    {
        $contact = contact::find($id);
        $contact->mobile = $request->mobile;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->facebook = $request->facebook;
        $contact->twitter = $request->twitter;
        $contact->linkedin = $request->linkedin;
        $contact->save();
        Session::flash('info', 'Contact Updated Successfully');
        return redirect()->route('contact.index');
    }
}
