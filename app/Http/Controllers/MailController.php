<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use Session;
class MailController extends Controller
{
    public function email(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'message' => 'required'
        ]);

        $emails = Email::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'message' => $request->message
        ]);
        Session::flash('success', 'Message Sent Successfully');
        return redirect()->back();
    }

    public function show()
    {
        $email = Email::all();
        return view('admin.email')->with('email', $email);
    }
}
