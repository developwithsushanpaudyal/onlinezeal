<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use File;
use Session;

class PortfolioCategoryController extends Controller
{
  public function create()
    {
      $portfoliocategory = Category::all();
      return view('admin.portfoliocategory.create');
    }

    public function store(Request $request)
     {
       $this->validate($request,[
         'name' => 'required',
       ]);
       $portfoliocategory = Category::create([
         'name' => $request->name,
       ]);
       Session::flash('success', 'Portfolio Category Was Successfully Created');
       return redirect()->route('portfoliocategory.index');
     }

     public function index()
       {
   	    return view('admin.portfoliocategory.index')->with('portfoliocategories', Category::all());

       }

       public function edit($id)
        {
    	    $portfoliocategory = Category::find($id);
    	    return view('admin.portfoliocategory.edit')->with('portfoliocategory',$portfoliocategory);
        }

        public function update(Request $request, $id)
        {
    	    $portfoliocategory = Category::find($id);
    	    $portfoliocategory->name = $request->name;
    	    $portfoliocategory->save();
    	    Session::flash('info', 'Portfolio Category Updated Successfully');
    	    return redirect()->route('portfoliocategory.index');
        }

        public function destroy($id)
       {
         $portfoliocategory = Category::find($id);
         $portfoliocategory->delete();
         Session::flash('delete', 'Portfolio Category Deleted Successfully');
         return redirect()->route('portfoliocategory.index');
       }
}
