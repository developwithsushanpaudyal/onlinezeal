<?php

namespace App\Http\Controllers;
use App\Blogstag;
use Session;


use Illuminate\Http\Request;

class BlogsTagController extends Controller
{
   
    public function index()
    {
        return view('admin.blogtags.index')->with('blogtags', Blogstag::all());
    }

    
    public function create()
    {
        return view('admin.blogtags.create');
        
    }

    
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
            ]);
          $blogtag = Blogstag::create([
            'name' => $request->name
            ]);
            Session::flash('success', 'Blog tag  Successfully Created');
          return redirect()->route('blogtag.index');
    }

   
    public function edit($id)
    {
        $blogtag = Blogstag::find($id);
	    return view('admin.blogtags.edit')->with('blogstag',$blogtag);
    }

    
    public function update(Request $request, $id)
    {
        $blogtag = Blogstag::find($id);
        $blogtag->name = $request->name;
        
        $blogtag->save();
	    Session::flash('info', 'Blogs tag Updated Successfully');
	    return redirect()->route('blogtag.index');
    }

    
    public function destroy($id)
    {
        $blogtag = Blogstag::find($id);
     $blogtag->delete();
     Session::flash('delete', 'Blogs tag Deleted Successfully');
     return redirect()->route('blogtag.index');
    }
}
