<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sliders;
use Session;
use File;

class SlidersController extends Controller
{

  public function index()
    {
        return view('admin.slider.index')->with('sliders', Sliders::all());
    }

  public function create()
  {
      return view('admin.slider.create');
  }


  public function store(Request $request)
      {
  	    $this->validate($request,[
  		    'title' => 'required',
  		    'subtitle' => 'required',
  	    ]);
  	    if($request->hasFile('image')){
  	    	$file = $request->file('image');
  		    $extension = $file->getClientOriginalExtension();
  		    $filename = time().'.'.$extension;
  		    $file->move('public/uploads/sliders/', $filename);
  	    }
  	    $slider = Sliders::create([
  	    	'title' => $request->title,
  		    'subtitle' => $request->subtitle,
  		    'image' => 'public/uploads/sliders/'. $filename,
  		    'link' => $request->link,
  	    ]);
  	    Session::flash('success', 'Slider Was Successfully Created');
  	    return redirect()->route('slider.index');
      }


      public function edit($id)
    {
        $slider = Sliders::find($id);
        return view('admin.slider.edit')->with('slider',$slider);
    }

    public function update(Request $request, $id)
   {
     $slider = Sliders::find($id);

     if($request->hasFile('image')){
       $file = $request->file('image');
       $extension = $file->getClientOriginalExtension();
       $filename = time().'.'.$extension;
       $file->move('public/uploads/sliders/', $filename);
       $slider->image = 'public/uploads/sliders/'.$filename;
     }
     $slider->title = $request->title;
     $slider->subtitle = $request->subtitle;
     $slider->link = $request->link;
     $slider->save();
     Session::flash('info', 'Slider Updated Successfully');
     return redirect()->route('slider.index');
   }

   public function destroy($id)
   {
       $slider = Sliders::find($id);
       $image_path = $slider->image;
//       dd($image_path);
       if(File::exists($image_path)){
         File::delete($image_path);
       }
       $slider->delete();
       Session::flash('delete', 'Slider Deleted Successfully');
       return redirect()->route('slider.index');
   }

}
