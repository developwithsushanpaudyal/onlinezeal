<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Input;
use App\Team;
use App\Designation;
use App\Titlepage;
use Session;
use Image;
use DB;

class TeamController extends Controller
{
    public function index()
    {
        $teams = Team::all();
        
        $team = Team::orderBy('sort_id', 'asc')->get();
      return view('frontend.team',compact('teams','team'))->with('titlepage',Titlepage::all());

    }

    public function create()
    {
        $team = Team::all();
        $designation = Designation::all();
       return view('admin.team.create')->with('designation',$designation);
    }

    public function store(Request $request, Team $team )
    {  
      $this->validate($request,[
          'name' => 'required',
          'designation' => 'required',
          //'sort_id'=>'required',
          'image' => 'required',
          'description' => 'required',
          
      ]);
      
            $team = New Team();
            
            $team->name = $request->name;
            $team->designation = $request->designation;
            $team->description = $request->description;
            $team->sort_id = $request->sort_id;
            $team->facebook = $request->facebook;
            $team->twitter = $request->twitter;
            $team->instagram = $request->instagram;
            $team->pinterest = $request->pinterest;
            $team->linkedin = $request->linkedin;
            //Upload Images
               if($request->hasFile('image')){
                 $image_tmp = Input::file('image');

                 if($image_tmp->isValid()){
                   $extension = $image_tmp->getClientOriginalExtension();
                   $filename = rand(111,99999).'.'.$extension;
                   $large_image_path = 'public/uploads/team/large/'.$filename;
                   $medium_image_path = 'public/uploads/team/medium/'.$filename;
                   $small_image_path = 'public/uploads/team/small/'.$filename;
                  
                   //Resize Images
                   Image::make($image_tmp)->save($large_image_path);
                   Image::make($image_tmp)->resize(615,520)->save($medium_image_path);
                   Image::make($image_tmp)->resize(360,300)->save($small_image_path);
            
             $team->image=$filename;      
                  }
               }
       
           $team->save();

         Session::flash('success', 'team Added Successfully ');
        return redirect()->route('team.view');



    }

    public function edit($id)
    {
        $designation = Designation::all();
        $team = Team::where('id',$id)->first();
        return view ('admin.team.edit',compact('team','designation'));
    }
  

    public function view(){

      $teams = Team::all();
      return view('admin.team.view',compact('teams'));
    }
    public function update(Request $request,$id) {
      
        $team =Team::findOrFail($id);
 
        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/uploads/team/large/'.$filename;
                $medium_image_path = 'public/uploads/team/medium/'.$filename;
                $small_image_path = 'public/uploads/team/small/'.$filename;
 
            
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(615,520)->save($medium_image_path);
                Image::make($image_tmp)->resize(360,300)->save($small_image_path);
 
                $team->image = $filename;
                $unlink = $request->current_image;
            }else{
                $team->image = $request->current_image;
 
            }
        }
        
        $team->name = $request->name;
        $team->designation = $request->designation;
        $team->description = $request->description;
        $team->facebook = $request->facebook;
        $team->twitter = $request->twitter;
        $team->instagram = $request->instagram;
        $team->pinterest = $request->pinterest;
        $team->linkedin = $request->linkedin;
        
        $team->save();
 
        Session::flash('flash_message_success', 'Team Member Updated Successfully ');
        return redirect()->route('team.view');
    }

    public function delete($id){
        $team =Team::findOrFail($id);
    $team->delete();
    return redirect()->route('team.view');
    }

}
