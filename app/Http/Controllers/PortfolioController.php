<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Portfolio;
use portfolio_category;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use Session;


class PortfolioController extends Controller
{
  public function create()
    {
      $portfolio = Portfolio::all();
      $categories = Category::all();
      return view('admin.portfolio.create', compact('categories'));
    }




      public function store(Request $request)
          {

      	    $this->validate($request,[
              'name' => 'required',
              'link' => 'required',
              'description' => 'required',
                ]);
        
                //dd($request);
                if($request->hasFile('image')){
                    $file = Input::file('image');
                    if($file->isValid()) {
                        //die('hfhdhg');
        
                        $extension = $file->getClientOriginalExtension();
              $filename = rand(111,99999).'.'.$extension;
                    
        
                    $large_image_path = 'public/front/images/portfolio/large/'.$filename;
                    $medium_image_path = 'public/front/images/portfolio/medium/'.$filename;
                    $small_image_path = 'public/front/images/portfolio/small/'.$filename;
        
                    //Resize Images
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(505,516)->save($medium_image_path);
                    Image::make($file)->resize(360,280)->save($small_image_path);
        
                     } 
                    
            }
      	    $portfolio = Portfolio::create([
      	    	'name' => $request->name,
      		    'link' => $request->link,
              'categories' => $request->categories,
              'description'=> $request->description,
              'image' => $filename,
      	    ]);

      	    Session::flash('success', 'Portfolio Was Successfully Created');
      	    return redirect()->route('portfolio.index');
          }

          public function index(){
            return view('admin.portfolio.index')->with('portfolios', Portfolio::all());
          }

          public function edit($id)
        {
            $portfolio = Portfolio::find($id);
            return view('admin.portfolio.edit')->with('portfolio',$portfolio)->with('categories', Category::all());
        }

        public function update(Request $request, $id)
       {
         $portfolio = Portfolio::find($id);
         

         
         if($request->hasFile('image')){
          $file = Input::file('image');
          if($file->isValid()) {
              //die('hfhdhg');

              $extension = $file->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
          

          $large_image_path = 'public/front/images/portfolio/large/'.$filename;
          $medium_image_path = 'public/front/images/portfolio/medium/'.$filename;
          $small_image_path = 'public/front/images/portfolio/small/'.$filename;

          //Resize Images
          Image::make($file)->save($large_image_path);
          Image::make($file)->resize(505,516)->save($medium_image_path);
          Image::make($file)->resize(360,280)->save($small_image_path);

           } 
          
        }
         
         $portfolio->name = $request->name;
         $portfolio->link = $request->link;
           $portfolio->categories = $request->categories;
           $portfolio->description = $request->description;
           $portfolio->image = $filename;
         $portfolio->save();
         Session::flash('info', 'Portfolio Updated Successfully');
         return redirect()->route('portfolio.index');
       }

       public function destroy($id)
       {
           $portfolio = Portfolio::find($id);
           $image_path = $portfolio->image;
           if(File::exists($image_path)){
             File::delete($image_path);
           }
           $portfolio->delete();
           Session::flash('delete', 'Portfolio Deleted Successfully');
           return redirect()->route('portfolio.index');
       }
}
