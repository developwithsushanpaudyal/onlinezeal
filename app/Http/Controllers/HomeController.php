<?php

namespace App\Http\Controllers;

use App\Success;
use Illuminate\Http\Request;
use App\Services;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.includes.index')
            ->with('services', Services::all())
            ->with('category', Category::all())
            ->with('servicesdashboard', Services::take(5)->get())
            ->with('success', Success::first());
    }
}
