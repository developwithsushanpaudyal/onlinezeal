<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->method('get'))
        {
            $packages = Package::get();
        }   
        return view('admin.package.index')->with(compact('packages'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->method('post'))
        {
            $package = New Package();
            //echo "<pre>"; print_r($data ); die;
    
            $desc[] = $request->input('desc');
            
            $description = json_encode($desc);

            $package->name = $request->input('name');
            $package->price = $request->input('price');
            $package->description = $description;
            $package->save();
            return redirect('admin/package/index');
        }
        return view('admin.package.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id=null)
    {
        $packageDetails = Package::where(['id'=>$id])->first();
        return view('admin.package.edit')->with(compact('packageDetails'));   
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        if($request->isMethod('post')){
            // echo "test"; die;
            $data = $request->all();

            $desc[] = $request->input('desc');
            
            $description = json_encode($desc);

            //echo "<pre>"; print_r($data); die; 
            Package::where(['id'=>$id])->update([
            'name'=>$data['name'],
            'price'=>$data['price'],
            'description'=>$description]);
            return redirect('admin/package/index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id=null)
    {
        if(!empty($id))
        {
            Package::where(['id'=>$id])->delete();
            return redirect('admin/package/index');
        }
    }
}
