<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Privacy;
use Session;
class PrivacyController extends Controller
{
    public function index()
    {
        $privacy = Privacy::first()->get();
        return view('admin.privacy.index')->with('privacy', $privacy);
    }

    public function edit($id){
        $privacy= Privacy::findOrFail($id);
        return view ('admin.privacy.edit')->with('privacy', $privacy);
    }
    public function update(Request $request, $id){
        $privacy = Privacy::find($id);
        $privacy->privacy = $request->privacy;
        $privacy->save();
        Session::flash('success', 'Privacy Updated Successfully');
        return redirect()->route('privacypolicy');
    }
}
