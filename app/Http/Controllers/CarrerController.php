<?php

namespace App\Http\Controllers;

use App\carrer;
use Illuminate\Http\Request;
use Session;

class CarrerController extends Controller
{
    public function index(){
        return view ('admin.carrer.index')->with('carrer', carrer::all());
    }
    public function edit($id)
    {
        $carrer =  carrer::find($id);
        return view('admin.carrer.edit')->with('carrer',$carrer);
    }

    public function update(Request $request, $id)
    {
        $carrer = carrer::find($id);
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('public/uploads/carrer/', $filename);
            $carrer->image = 'public/uploads/carrer/'.$filename;
        }

        $carrer->description = $request->description;
        $carrer->subtitle = $request->subtitle;
        $carrer->save();
        Session::flash('success', 'Carrer Updated Successfully');
        return redirect()->route('carrer');
    }

}
