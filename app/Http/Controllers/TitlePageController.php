<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Titlepage;
use Session;

class TitlePageController extends Controller
{
    
    public function index()
    {
        $titlepages =Titlepage::all();
        return view('admin.titlepage.index')->with('titlepage',$titlepages);
    }

    
    public function create()
    {
         return view('admin.titlepage.create');
    }

    
    public function store(Request $request)
    {
        //dd('test');
        $this->validate($request,[
            'name' => 'required',
            'content' => 'required',
        ]);

        $titlepages = Titlepage::create([
            'name' => $request->name,
            'content' => $request->content,
        
        ]);

        Session::flash('success','Name and Content stored successfully');
        return redirect()->route('titlepage.index');

    }

    
  
    
    public function edit($id)
    {
        $titlepage = Titlepage::find($id);
        return view('admin.titlepage.edit')->with('titlepage',$titlepage);
    }

    
    public function update(Request $request, $id)
    {
        $titlepages = Titlepage::find($id);

        $titlepages->name = $request->name;
        $titlepages->content = $request->content;
        $titlepages->save();

        Session::flash('info','Title Page Updated Successfully');
        return redirect()->route('titlepage.index');
    }

   
    public function delete($id)
    {
        
        $titlepages = Titlepage::find($id);
        $titlepages->delete();
        Session::flash('delete', 'Titlepage Deleted Successfully');
        return redirect()->route('titlepage.index');
    }
}
