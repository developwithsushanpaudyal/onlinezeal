<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use Session;
use File;

class AboutController extends Controller
{
    public function index(){
      return view('admin.about.index')->with('about', About::all());
    }

    public function store(Request $request){
  	    $this->validate($request,[
  	    	'slogan' => 'required',
	        'our_mission' => 'required'
        ]);
		if($request->hasFile('cover_image')){
			$file = $request->file('cover_image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('public/uploads/about/cover/', $filename);
		}
    if($request->hasFile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('public/uploads/about/', $filename);
		}
		$about = About::create([
			'slogan' => $request->slogan,
			'our_mission' => $request->our_mission,
      'our_vision' => $request->our_vision,
      'objectives' => $request->objectives,
      'chooseus' => $request->chooseus,
      'cover_image' => 'public/uploads/about/cover/'. $filename,
			'image' => 'public/uploads/about/'. $filename,
		]);
		Session::flash('success', 'About Was Inserted Successfully');
		return redirect()->route('about.index');
	}

  public function edit($id)
   {
       $about =  About::find($id);
       return view('admin.about.edit')->with('about',$about);
   }


   public function update(Request $request, $id)
    {
	    $about = About::find($id);

	    if($request->hasFile('image')){
		    $file = $request->file('image');
		    $extension = $file->getClientOriginalExtension();
		    $filename = time().'.'.$extension;
		    $file->move('public/uploads/about/', $filename);
		    $about->image = 'public/uploads/about/'.$filename;
	    }
	    if($request->hasFile('cover_image')){
		    $file = $request->file('cover_image');
		    $extension = $file->getClientOriginalExtension();
		    $filename = time().'.'.$extension;
		    $file->move('public/uploads/about/cover/', $filename);
		    $about->cover_image = 'public/uploads/about/cover/'.$filename;
	    }

	    $about->slogan = $request->slogan;
	    $about->our_mission = $request->our_mission;
	    $about->our_vision = $request->our_vision;
	    $about->objectives = $request->objectives;
	    $about->chooseus = $request->chooseus;
	    $about->save();
	    Session::flash('success', 'About Updated Successfully');
	    return redirect()->route('about.index');
    }
}
