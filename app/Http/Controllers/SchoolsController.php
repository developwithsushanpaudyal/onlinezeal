<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use Session;
use Image;
use Illuminate\support\Facades\Input;
use File;

class SchoolsController extends Controller
{
    public function create()
    {
        return view('admin.school.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'website' => 'required',
            'logo' => 'required',
        ]);
                    //Upload Logo
                    if($request->hasfile('logo')){
                
                        $image_tmp = Input::file('logo');
        
                        if($image_tmp->isValid()){
                            
                            $extension = $image_tmp->getClientOriginalExtension();
                            $filename=rand(111,99999).'.'.$extension;
                            $large_image_path = 'public/uploads/schools/logo/large/'.$filename;
                            $medium_image_path = 'public/uploads/schools/logo/medium/'.$filename;
                            $small_image_path = 'public/uploads/schools/logo/small/'.$filename;
        
                            //Resize Logo
                            Image::make($image_tmp)->save($large_image_path);
                            Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                            Image::make($image_tmp)->resize(300,300)->save($small_image_path);
                        }
                    }
        
        $school = School::create([
            'name' => $request->name,
            'website' => $request->website,
            'description' => $request->description,
            'category' => $request->category,
            'logo' => $filename,
          
        ]);
        Session::flash('success', 'School Was Inserted Successfully');
        return redirect()->route('school.index');
    }

    public function index(){

        //$school = School::find($id);

        return view ('admin.school.index')->with('schools', School::all());
    }

    public function edit($id)
    {
        $school = School::where('id',$id)->first();
        return view('admin.school.edit')->with('school',$school);
    }

    public function update(Request $request, $id)
    {
        $school = School::findOrFail($id);
        $school->name = $request->name;
        $school->website = $request->website;
        $school->description = $request->description;
        $school->category = $request->category;

        if($request->hasFile('logo')){
            $image_tmp = Input::file('logo');

                if($image_tmp->isValid()){
                    
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename=rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/uploads/schools/logo/large/'.$filename;
                    $medium_image_path = 'public/uploads/schools/logo/medium/'.$filename;
                    $small_image_path = 'public/uploads/schools/logo/small/'.$filename;

                    //Resize Logo
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                    $school->logo = $filename;
                    $unlink = $request->current_logo;
        }
        else{
            $school->logo = $request->current_logo;
        }
    }

        $school->save();
        Session::flash('success', 'School Updated Successfully');
        return redirect()->route('school.index');
}

    public function destroy($id)
    {
        $school = School::find($id);
        $school->delete();
        Session::flash('delete', 'Client Deleted Successfully');
        return redirect()->route('school.index');
    }
}
