<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use App\Servicescategory;
use Session;
use File;

class ServicesController extends Controller
{
  public function index()
{
  return view('admin.services.index')->with('services', Services::all());
}

public function create()
	{
		$services = Services::all();
		return view('admin.services.create')->with('servicescategories', Servicescategory::all());
	}


  public function store(Request $request)
	{
		$this->validate($request,[
			'title' => 'required',
			'subtitle' => 'required',
			'content' => 'required',
		]);
		if($request->hasFile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('public/uploads/services/', $filename);
		}
		$services = Services::create([
			'title' => $request->title,
			'subtitle' => $request->subtitle,
            'slug' => str_slug($request->title),
            'icon' => $request->icon,
              'keywords'=> $request->keywords,
'description' => $request->description,
'meta_title' => $request->meta_title,
			'content' => $request->content,
      'meta_description' => str_limit($request->content, 100),
      'servicescategory_id' => $request->servicescategory_id,
			'image' => 'public/uploads/services/'. $filename,
		]);
		Session::flash('success', 'Services Was Inserted Successfully');
		return redirect()->route('service.index');
	}


  public function edit($id)
	{
		$service = Services::find($id);
		return view('admin.services.edit')->with('service',$service)->with('servicescategories', Servicescategory::all());
	}


  public function update(Request $request, $id)
	{
		$service = Services::find($id);
		if($request->hasFile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time().'.'.$extension;
			$file->move('public/uploads/services/', $filename);
			$service->image = 'public/uploads/services/'.$filename;
		}
		$service->title = $request->title;
		$service->subtitle = $request->subtitle;
        $service->slug = str_slug($request->title);
        $service->icon = $request->icon;
       $service->keywords = $request->keywords;
$service->description = $request->description;
$service->meta_title = $request->meta_title;
		$service->content = $request->content;
        $service->meta_description = str_limit($request->content, 100);
    $service->servicescategory_id = $request->servicescategory_id;
		$service->save();
		Session::flash('success', 'Services Updated Successfully');
		return redirect()->route('service.index');
	}


  public function destroy($id)
	{
		$service = Services::find($id);
		$image_path = $service->image;
		if(File::exists($image_path)){
			File::delete($image_path);
		}
		$service->delete();
		Session::flash('delete', 'Service Deleted Successfully');
		return redirect()->route('service.index');
	}
}
