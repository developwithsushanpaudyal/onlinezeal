<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Designation;
use Session;

class DesignationController extends Controller
{
  
   public function create()
   {
      return view('admin.designation.create');
   }

   public function view(){
      $designation = Designation::all();
      return view('admin.designation.view',compact('designation'));
   }

   public function store(Request $request)
   {
      $designation = New Designation();
      $designation->name = $request->name;
      $designation->save();
      
      Session::flash('success', 'team Added Successfully ');
      return redirect()->route('designation.view');
   }

   public function edit($id){
      $designation = Designation::where('id',$id)->first();
     return view('admin.designation.edit',compact('designation'));
   }

   public function update(Request $request,$id)
    {
      $designation =Designation::findOrFail($id);
	    $designation->name = $request->name;
              
       $designation->save();
	    Session::flash('success', 'Designation Updated Successfully');
	    return redirect()->route('designation.view');
    }

   public function delete($id){
      $designation =Designation::findOrFail($id);
      $designation->delete();
      return redirect()->route('designation.view');
   }
}
