<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terms;
use Session;
class TermsController extends Controller
{
    public function index(){
        $terms = Terms::all();
        return view ('admin.terms.index')->with('terms', $terms);
    }

    public function edit($id){
        $term = Terms::findOrFail($id);
        return view ('admin.terms.edit')->with('term', $term);
    }

    public function update(Request $request, $id){
        $term = Terms::find($id);
        $term->terms = $request->terms;
        $term->save();
        Session::flash('success', 'Terms Updated Successfully');
        return redirect()->route('termsandconditions');
    }
}
