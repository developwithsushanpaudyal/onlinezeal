<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Blogscategory;
use Illuminate\Support\Facades\Input;
use Image;
use Session;
use File;

class BlogController extends Controller
{
    
    public function index()
    {
        //dd('test');
        $blog = Blog::all();
        return view('admin.blog.index')->with('blog', Blog::all());
    }

    
    public function create()
    {
        
        $blog = Blog::all();
		return view('admin.blog.create')->with('blogscategories', Blogscategory::all());
    }

    
    public function store(Request $request)
    {
        
        $this->validate($request,[
			'title' => 'required',
			'body' => 'required',
        ]);

        //dd($request);
       
        if($request->hasFile('image')){
            $file = Input::file('image');
            if($file->isValid()) {
                //die('hfhdhg');

                $extension = $file->getClientOriginalExtension();
			$filename = rand(111,99999).'.'.$extension;
            

            $large_image_path = 'public/front/images/blog/large/'.$filename;
            $medium_image_path = 'public/front/images/blog/medium/'.$filename;
            $small_image_path = 'public/front/images/blog/small/'.$filename;

            //Resize Images
            Image::make($file)->save($large_image_path);
            Image::make($file)->resize(600,600)->save($medium_image_path);
            Image::make($file)->resize(360,280)->save($small_image_path);

             } else{
                 $filename = $data('current_image');
             }
            
		}
		
		$blog = Blog::create([
			'title' => $request->title,
            'subtitle' => $request->subtitle,
            'author'=> $request->author,
            'body' => $request->body,
            'slug' => str_slug($request->title),
            'video'=> $request->video,
            'blogscategory_id' => $request->blogscategory_id,
            'image' => $filename,
           
		]);
		Session::flash('success', 'Blogs Was Inserted Successfully');
		return redirect()->route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogs = Blog::find($id);
		return view('admin.blog.edit')->with('blog',$blogs)->with('blogscategories', Blogscategory::all());
    }

    
    public function update(Request $request, $id)
    {
        $blogs = Blog::find($id);
        if($request->hasFile('image')){
            $file = Input::file('image');
            if($file->isValid()) {
                //die('hfhdhg');

                $extension = $file->getClientOriginalExtension();
			$filename = rand(111,99999).'.'.$extension;
            

           $large_image_path = 'public/front/images/blog/large/'.$filename;
           $medium_image_path = 'public/front/images/blog/medium/'.$filename;
           $small_image_path = 'public/front/images/blog/small/'.$filename;

            //Resize Images
            Image::make($file)->save($large_image_path);
            Image::make($file)->resize(600,600)->save($medium_image_path);
            Image::make($file)->resize(360,280)->save($small_image_path);

             } else{
                 $filename = $data('current_image');
             }
            
		}

		
			$blogs->title = $request->title;
            $blogs->subtitle = $request->subtitle;
            $blogs->author= $request->author;
            $blogs->slug = str_slug($request->title);
            $blogs->body = $request->body;
            $blogs->video = $request->video;
            $blogs->blogscategory_id = $request->blogscategory_id;
            $blogs->image = $filename;
            
            
			$blogs->save();
		Session::flash('success', 'Blogs Was Updated Successfully');
		return redirect()->route('blog.index');
    }

    
    public function destroy($id)
    {
        $blogs = Blog::find($id);
		$image_path = $blogs->image;
		if(File::exists($image_path)){
			File::delete($image_path);
		}
		$blogs->delete();
		Session::flash('delete', 'Blogs Deleted Successfully');
		return redirect()->route('blog.index');
    }

    
}
