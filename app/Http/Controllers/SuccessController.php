<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Success;
use File;
use Session;

class SuccessController extends Controller
{
    public function index(){
      $success = Success::all();
      return view('admin.success.index')->with('success', $success);
    }

    public function edit($id)
  {
      $success = Success::find($id);
      return view('admin.success.edit')->with('success',$success);
  }


  public function update(Request $request, $id)
 {
   $success = Success::find($id);

   if($request->hasFile('coverimage')){
     $file = $request->file('coverimage');
     $extension = $file->getClientOriginalExtension();
     $filename = time().'.'.$extension;
     $file->move('public/uploads/success/', $filename);
     $success->coverimage = 'public/uploads/success/'.$filename;
   }
   $success->clients = $request->clients;
   $success->hours = $request->hours;
   $success->projects = $request->projects;
   $success->save();
   Session::flash('info', 'Success Updated Successfully');
   return redirect()->route('success.index');
 }
}
