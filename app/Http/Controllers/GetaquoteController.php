<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Getaquote;
use Session;

class GetaquoteController extends Controller
{
    public function store(Request $request){

        $this->validate($request,[
            'username'=> 'required',
            'useremail' => 'required',
            'userphone' => 'required',
            'userwebsite' => 'required',
            'usermessage' => 'required',
            
        ]);
       
        $getaquote = Getaquote::create([
			'username' => $request->username,
            'useremail' => $request->useremail,
            'userphone'=> $request->userphone,
            'userwebsite'=> $request->userwebsite,
            'usermessage' => $request->usermessage,
        
           
		]);
        Session::flash('success','Your Quote is send Successfully');
        return redirect()->back();
	
    }

    public function view()
    {
        $getaquote = Getaquote::all();
        return view('admin.getaquote')->with('getaquote', $getaquote);
    }


}
