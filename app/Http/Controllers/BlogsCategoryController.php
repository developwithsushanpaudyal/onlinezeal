<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blogscategory;
use Session;

class BlogsCategoryController extends Controller
{
    
    public function index()
    {
        return view('admin.blogscategories.index')->with('blogscategories', Blogscategory::all());
    }

   
    public function create()
    {
        return view('admin.blogscategories.create');
    }

   
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'icon' => 'required',
            'description' => 'required',
          ]);
          $blogcategory = Blogscategory::create([
            'name' => $request->name,
            'icon' => $request->icon,
            'slug' => str_slug($request->name),
            'description' => $request->description,
          ]);
          Session::flash('success', 'Blogs Category Was Successfully Created');
          return redirect()->route('blogcategory.index');
        
    }

    public function edit($id)
    {
        $blogcategory = Blogscategory::find($id);
	    return view('admin.blogscategories.edit')->with('blogscategory',$blogcategory);
    }

   
    public function update(Request $request, $id)
    {
        $blogcategory = Blogscategory::find($id);
	    $blogcategory->name = $request->name;
        $blogcategory->icon = $request->icon;
        $blogcategory->slug = str_slug($request->name);        
      $blogcategory->description = $request->description;
	    $blogcategory->save();
	    Session::flash('info', 'Blogs Category Updated Successfully');
	    return redirect()->route('blogcategory.index');
    }

    
    public function destroy($id)
    {
        $blogcategory = Blogscategory::find($id);
     $blogcategory->delete();
     Session::flash('delete', 'Blogs Category Deleted Successfully');
     return redirect()->route('blogcategory.index');
    }
}
