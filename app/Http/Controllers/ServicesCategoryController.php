<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicescategory;
use Session;

class ServicesCategoryController extends Controller
{

  public function index()
    {
	    return view('admin.servicescategories.index')->with('servicescategories', Servicescategory::all());

    }

  public function create()
  {
      return view('admin.servicescategories.create');
  }

  public function store(Request $request)
   {
     $this->validate($request,[
       'name' => 'required',
       'icon' => 'required',
       'description' => 'required',
     ]);
     $servicescategory = Servicescategory::create([
       'name' => $request->name,
       'icon' => $request->icon,
       'description' => $request->description,
     ]);
     Session::flash('success', 'Services Category Was Successfully Created');
     return redirect()->route('category.index');
   }

   public function edit($id)
    {
	    $servicescategory = Servicescategory::find($id);
	    return view('admin.servicescategories.edit')->with('servicescategory',$servicescategory);
    }

    public function update(Request $request, $id)
    {
	    $servicescategory = Servicescategory::find($id);
	    $servicescategory->name = $request->name;
	    $servicescategory->icon = $request->icon;
      $servicescategory->description = $request->description;
	    $servicescategory->save();
	    Session::flash('info', 'Services Category Updated Successfully');
	    return redirect()->route('category.index');
    }

    public function destroy($id)
   {
     $servicescategory = Servicescategory::find($id);
     $servicescategory->delete();
     Session::flash('delete', 'Services Category Deleted Successfully');
     return redirect()->route('category.index');
   }
}
