<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Process;
use File;
use Session;

class ProcessController extends Controller
{
  public function create()
    {
      $process = Process::all();
      return view('admin.process.create');
    }


    public function store(Request $request)
        {
          $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
          ]);
          if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('public/uploads/process/', $filename);
          }
          $slider = Process::create([
            'title' => $request->title,
            'icon' => $request->icon,
            'image' => 'public/uploads/process/'. $filename,
            'content' => $request->content,
          ]);
          Session::flash('success', 'Process Was Successfully Created');
          return redirect()->route('process.index');
        }

        public function index()
          {
              return view('admin.process.index')->with('processes', Process::all());
          }

          public function edit($id)
        {
            $process = Process::find($id);
            return view('admin.process.edit')->with('process',$process);
        }


        public function update(Request $request, $id)
       {
         $process = Process::find($id);
         if($request->hasFile('image')){
           $file = $request->file('image');
           $extension = $file->getClientOriginalExtension();
           $filename = time().'.'.$extension;
           $file->move('public/uploads/process/', $filename);
           $process->image = 'public/uploads/process/'.$filename;
         }
         $process->title = $request->title;
         $process->content = $request->content;
         $process->icon = $request->icon;
         $process->save();
         Session::flash('info', 'Process Updated Successfully');
         return redirect()->route('process.index');
       }

       public function destroy($id)
       {
           $process = Process::find($id);
           $image_path = public_path().'/'.$process->image;
           if(File::exists($image_path)){
             File::delete($image_path);
           }
           $process->delete();
           Session::flash('delete', 'Process Deleted Successfully');
           return redirect()->route('process.index');
       }
}
