<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;

class ProfilesController extends Controller
{
    public function profile()
    {
        return view('admin.profile')->with('user', Auth::user());
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
        ]);
        $user = Auth::user();
        if($request->hasFile('avatar'))
        { $avatar = $request->avatar;
            $avatar_new_name = time().$avatar->getClientOriginalName();
            $avatar->move('public/uploads/avatars/', $avatar_new_name);
            $user->profile->avatar = 'public/uploads/avatars/'.$avatar_new_name;
            $user->profile->save();
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile->facebook = $request->facebook;
        $user->profile->linkedin = $request->linkedin;
        $user->profile->about = $request->about;
        $user->save();
        $user->profile->save();
        if($request->has('password'))
        {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        Session::flash('success', 'Account Profile Updated');
        return redirect()->route('profile');
    }
}
