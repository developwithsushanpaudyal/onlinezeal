<?php

namespace App\Http\Controllers;

use App\Positions;
use Illuminate\Http\Request;
use Session;

class VacancyController extends Controller
{
    public function create()
    {
        return view('admin.vacancy.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'position' => 'required',
            'department' => 'required',
        ]);
        $vacancy = Positions::create([
            'position' => $request->position,
            'department' => $request->department,
            'information' => $request->information,
            'positionnumber' => $request->positionnumber,
        ]);
        Session::flash('success', 'Vacancy Was Successfully Created');
        return redirect()->route('vacancy.index');
    }

    public function index()
    {
        return view('admin.vacancy.index')->with('vacancies', Positions::all());
    }

    public function edit($id)
    {
        $vacancy = Positions::find($id);
        return view('admin.vacancy.edit')->with('vacancy',$vacancy);
    }

    public function update(Request $request, $id)
    {
        $vacancy = Positions::find($id);

        $vacancy->position = $request->position;
        $vacancy->department = $request->department;
        $vacancy->information = $request->information;
        $vacancy->positionnumber = $request->positionnumber;
        $vacancy->save();
        Session::flash('info', 'Vacancy Updated Successfully');
        return redirect()->route('vacancy.index');
    }

    public function destroy($id)
    {
        $vacancy = Positions::find($id);
        $vacancy->delete();
        Session::flash('delete', 'Vacancy Deleted Successfully');
        return redirect()->route('vacancy.index');
    }

}
