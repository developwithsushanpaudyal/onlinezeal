<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hosting;

class HostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->method('get'))
        {
            $hostings = Hosting::get();
        }   
        return view('admin.hosting.index')->with(compact('hostings'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hosting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->method('post'))
        {
            $hosting = New Hosting();
            //echo "<pre>"; print_r($data ); die;
    
            $desc[] = $request->input('desc');
            
            $description = json_encode($desc);

            $hosting->name = $request->input('name');
            $hosting->price = $request->input('price');
            $hosting->description = $description;
            $hosting->save();
            return redirect('admin/hosting/index');
        }
        return view('admin.hosting.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id=null)
    {
        $hostingDetails = Hosting::where(['id'=>$id])->first();
        return view('admin.hosting.edit')->with(compact('hostingDetails'));   
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        if($request->isMethod('post')){
            // echo "test"; die;
            $data = $request->all();

            $desc[] = $request->input('desc');
            
            $description = json_encode($desc);

            //echo "<pre>"; print_r($data); die; 
            Hosting::where(['id'=>$id])->update([
            'name'=>$data['name'],
            'price'=>$data['price'],
            'description'=>$description]);
            return redirect('admin/hosting/index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id=null)
    {
        if(!empty($id))
        {
            Hosting::where(['id'=>$id])->delete();
            return redirect('admin/hosting/index');
        }
    }
}
