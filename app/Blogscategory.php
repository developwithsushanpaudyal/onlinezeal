<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blogscategory extends Model
{
    protected $fillable = ['name', 'icon', 'slug', 'description'];

    public function blogs(){
        
        return $this->hasMany('App\Blog');
    }
}
