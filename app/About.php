<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = ['slogan', 'cover_image', 'our_mission', 'our_vision', 'objectives', 'chooseus', 'image'];
}
