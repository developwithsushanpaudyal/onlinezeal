<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Getaquote extends Model
{
    protected $fillable = ['username','useremail','userphone','userwebsite','usermessage'];
}
