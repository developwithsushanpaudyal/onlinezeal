<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $fillable = ['title', 'subtitle', 'content', 'icon', 'image',  'servicescategory_id', 'slug'];

    public function servicescategory(){
    	return $this->belongsTo('App\Servicescategory');
    }
}
