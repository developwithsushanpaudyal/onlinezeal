<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $fillable = ['name'];

    public function portfolios(){
       
        return $this->belongsToMany('App\Portfolio', 'portfolio_categories')->withTimestamps();
    }
}
