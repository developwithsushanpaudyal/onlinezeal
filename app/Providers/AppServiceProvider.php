<?php

namespace App\Providers;

use App\contact;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use App\Services;
use App\TitlePage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer(['frontend.includes.footer', 'frontend.includes.header', 'frontend.partials.meta_dynamic' ], function($view){
            $view->with('servicesfooter', Services::take(5)->get())
            ->with('contacts', contact::all()->first())
                ->with('services1', Services::where('servicescategory_id', 1)->get())
                ->with('services2', Services::where('servicescategory_id', 2)->get())
                ->with('services3', Services::where('servicescategory_id', 3)->get())
                ->with('services4', Services::where('servicescategory_id', 4)->get())
                ->with('services5', Services::where('servicescategory_id', 5)->get())
            ->with('services', Services::all());

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
