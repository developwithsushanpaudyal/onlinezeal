<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carrer extends Model
{
    protected $fillable = ['subtitle', 'image', 'description'];
}
