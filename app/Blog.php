<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $fillable = ['title', 'subtitle','image', 'video', 'author', 'slug', 'body',  'blogscategory_id', 'slug','like','dislike'];

    public function blogcategory(){

    	return $this->belongsTo('App\Blogscategory');
    }
}
