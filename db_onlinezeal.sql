-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2018 at 11:06 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_onlinezeal`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `slogan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `our_mission` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `our_vision` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `objectives` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `chooseus` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `slogan`, `cover_image`, `our_mission`, `our_vision`, `objectives`, `chooseus`, `image`, `created_at`, `updated_at`) VALUES
(1, 'What makes a company great? That’s simple. It’s the people who work here.', 'public/uploads/about/cover/1534826765.jpg', 'To deliver optimal solutions with quality services at reasonable prices.\r\nTo assist the nation in becoming digitally advance. \r\nTo be able to motivate Nepali youth to strive towards the digital world.', 'We aspire to step forward towards Digital Nepal, working for the betterment of the country and its people through continuous improvement and integration of Technologies with a vision to see every citizen using Technology for a better future.', 'In this information technology era, IT plays an integral role in every industry, helping companies improve business processes, achieve cost efficiencies, drive revenue growth and maintain a competitive advantage in the marketplace. OnlineZeal is fully dedicated and committed in work to achieve a targeted-goal. Below are some objectives that represents core values and objectives:\r\n•	Introduction of new IT services in the market to meet the user requirements\r\n•	Ensuring consistent quality of services\r\n•	Be a leader in the market if IT in Nepal\r\n•	Proposing cooperation at solving security problems', 'With a pool of experienced minds, OnlineZeal is a modern day tech agency that creates innovative and meaningful digital products and services with strategies that power them. Everything you entrust on us will be handled with utmost care and professionalism. We are a team driven by workmanship and commitment to quality. We cover a wide range of creative sectors and skills, and tailor our work to meet your objectives so that you get the best while we do the rest. \r\nTo assure your customer gets the best website browsing experience, we will collaborate with you in every milestone of your project. We build your brand identity and ensure it remains consistent.', 'public/uploads/about/1534662587.jpg', '2018-08-19 01:24:47', '2018-10-08 14:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `carrers`
--

CREATE TABLE `carrers` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carrers`
--

INSERT INTO `carrers` (`id`, `description`, `image`, `subtitle`, `created_at`, `updated_at`) VALUES
(1, '<p>We respect our morals and live up to our values. We take pride in having established a culture that is built upon a unified set of values. We work hard and yet have fun. More than just wor', 'public/uploads/carrer/1534841288.jpg', 'We Help Your Career In IT Get Moving Rapidly', NULL, '2018-10-08 14:29:22');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Website', '2018-08-18 15:07:58', '2018-08-18 15:07:58'),
(3, 'WebApps', '2018-08-18 15:08:08', '2018-08-18 15:08:08'),
(4, 'GraphicDesign', '2018-08-18 15:08:21', '2018-09-13 14:18:26'),
(5, 'DigitalMarketing', '2018-08-18 15:08:32', '2018-09-13 14:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `links` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `title`, `image`, `links`, `created_at`, `updated_at`) VALUES
(1, 'Beyond Boundries', 'uploads/clients/1534625694.jpg', 'http://www.beyondboundaries.com.np/', '2018-08-18 15:09:54', '2018-08-18 15:09:54'),
(3, 'Silver lining', 'uploads/clients/1535448302.jpg', 'https://www.facebook.com/silverliningeventsnepal/', '2018-08-28 16:25:02', '2018-09-13 17:12:01'),
(6, 'Glow to Glow', 'uploads/clients/1535448458.jpg', 'https://www.facebook.com/glowtoglow/', '2018-08-28 16:27:38', '2018-09-13 16:59:12'),
(7, 'Zeal Sale', 'uploads/clients/1535448496.jpg', 'https://www.facebook.com/zealsalektm/', '2018-08-28 16:28:16', '2018-09-13 16:58:35'),
(8, 'MU', 'uploads/clients/1535448536.jpg', 'http://www.modyuniversity.ac.in/', '2018-08-28 16:28:56', '2018-09-13 16:59:57'),
(9, 'zeal education', 'uploads/clients/1535448623.jpg', 'http://test.zealtecheducation.com/test/', '2018-08-28 16:30:23', '2018-09-13 17:05:52'),
(11, 'NDC Education', 'uploads/clients/1535521679.jpg', 'https://www.facebook.com/ndcedu/', '2018-08-29 12:47:59', '2018-09-13 16:36:26'),
(12, 'Sales Nepal', 'uploads/clients/1535522133.jpg', 'https://www.facebook.com/salesnepall/', '2018-08-29 12:55:33', '2018-09-13 16:35:01'),
(13, 'Miss & Mrs. Femina International', 'uploads/clients/1535522221.jpg', 'https://www.facebook.com/missmrsfeminainternational/', '2018-08-29 12:57:01', '2018-09-13 17:01:17'),
(15, 'Schems Public School', 'uploads/clients/1535522325.jpg', 'http://schems.edu.np/', '2018-08-29 12:58:45', '2018-09-13 16:39:06');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `mobile`, `phone`, `email`, `facebook`, `twitter`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, '9801079608', '01-5199625', 'contact@onlinezeal.com', 'https://www.facebook.com/onlinezeal/', 'https://www.facebook.com/onlinezeal/', 'https://www.facebook.com/onlinezeal/', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `phone`, `address`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Sushan Paudyal', 'susahn.paudyal@gmail.com', '98039839898', 'Shanhuaiki', 'uad;uia diasd duisabds aisudbasds aobsad diuba di dsafobdsfd ofbcsafuds iasubdudisdsfsafs', '2018-10-15 06:34:55', '2018-10-15 06:34:55'),
(2, 'Sush', 'sushan.paudyal@mjk.com', '89898', 'dnahd', 'jnaskdnsasasadasd', '2018-10-15 06:37:46', '2018-10-15 06:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `hostings`
--

CREATE TABLE `hostings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostings`
--

INSERT INTO `hostings` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Basic', '2000', '[[\"1 Website\",\"2 Sub-Domain\",\"2 FTP Accounts\",\"1 Database\",\"10GB Monthly Bandwidth\",\"10 Email Accounts\",\"No SSL\",\"Server Location- USA\",\"99.9% Uptime Guarantee\",\"Disater Recovery & Backups\"]]', '2018-08-19 01:27:39', '2018-08-28 14:34:58'),
(2, 'Pro', '7000', '[[\"5 Website\",\"5 Databases\",\"10 FTP Accounts\",\"Unlimited Bandwidth\",\"100 Monthly Bandwidth\",\"50 Email Accounts\",\"FREE SSL\",\"Server Location- USA\",\"99.9% Uptime Guarantee\",\"Disater Recovery & Backups\"]]', '2018-08-19 01:28:19', '2018-08-28 14:40:16'),
(3, 'Unlimited', '10000', '[[\"Unlimited Disk Space\",\"Unlimited FTP Accounts\",\"Unlimited Database\",\"Unlimited Bandwidth\",\"Unlimited Email Accounts\",\"FREE SSL\",\"Cloudfare Server\",\"99.9% Uptime Guarantee\",\"Disater Recovery & Backups\"]]', '2018-08-19 01:28:50', '2018-08-28 14:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_06_063811_create_sliders_table', 1),
(4, '2018_08_06_112525_create_processes_table', 1),
(5, '2018_08_06_112539_create_services_table', 1),
(6, '2018_08_06_112553_create_servicescategories_table', 1),
(7, '2018_08_08_095636_create_portfolios_table', 1),
(8, '2018_08_08_112255_create_successes_table', 1),
(9, '2018_08_08_152641_create_clients_table', 1),
(10, '2018_08_09_094015_create_abouts_table', 1),
(11, '2018_08_10_061707_create_contacts_table', 1),
(12, '2018_08_10_084619_create_portfolio_categories_table', 1),
(13, '2018_08_10_085132_create_categories_table', 1),
(14, '2018_08_16_061824_create_packages_table', 1),
(15, '2018_08_16_062336_create_hostings_table', 1),
(16, '2018_08_18_172856_create_profiles_table', 1),
(17, '2018_08_21_054030_add_metadescription_column_to_services_table', 1),
(18, '2018_08_21_061206_create_carrers_table', 1),
(19, '2018_08_21_062259_create_positions_table', 1),
(20, '2018_08_21_070043_add_positionnumber_column_to_positions_table', 1),
(21, '2018_08_22_070601_create_schools_table', 1),
(95, '2014_10_12_000000_create_users_table', 1),
(96, '2014_10_12_100000_create_password_resets_table', 1),
(97, '2018_08_06_063811_create_sliders_table', 1),
(98, '2018_08_06_112525_create_processes_table', 1),
(99, '2018_08_06_112539_create_services_table', 1),
(100, '2018_08_06_112553_create_servicescategories_table', 1),
(101, '2018_08_08_095636_create_portfolios_table', 1),
(102, '2018_08_08_112255_create_successes_table', 1),
(103, '2018_08_08_152641_create_clients_table', 1),
(104, '2018_08_09_094015_create_abouts_table', 1),
(105, '2018_08_10_061707_create_contacts_table', 1),
(106, '2018_08_10_084619_create_portfolio_categories_table', 1),
(107, '2018_08_10_085132_create_categories_table', 1),
(108, '2018_08_16_061824_create_packages_table', 1),
(109, '2018_08_16_062336_create_hostings_table', 1),
(110, '2018_08_18_172856_create_profiles_table', 1),
(111, '2018_08_21_054030_add_metadescription_column_to_services_table', 2),
(112, '2018_08_21_061206_create_carrers_table', 3),
(113, '2018_08_21_062259_create_positions_table', 4),
(114, '2018_08_21_070043_add_positionnumber_column_to_positions_table', 5),
(115, '2018_08_22_070601_create_schools_table', 6),
(116, '2018_10_14_115417_create_terms_table', 7),
(117, '2018_10_15_060209_create_privacies_table', 8),
(118, '2018_10_15_121148_create_emails_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Entry Level', '9,999', '[[\"Basic Feature Included\",\"6 Responsive Webpages\",\"Social Media Integration\",\"Google Map Integration\",\"One Time Content\",\"SEO Friendly\",\"5 Gb Disk Space\",\"Unlimited Bandwidth\",\"6 E-mail Accounts\",\"NO SSL\",\".np Domain\",\"1 Year Free Technical Support\"]]', '2018-08-19 01:30:16', '2018-08-28 15:50:27'),
(2, 'Medium Level', '19,999', '[[\"Basic to Advance Features Included\",\"10-12 Responsive Webpages\",\"Social Media Integration\",\"Google Map Integration\",\"One Time Content\",\"SEO Friendly\",\"25 Gb Disk Space\",\"Unlimited Bandwidth\",\"15 E-mail Accounts\",\"SSL Included\",\".np Domain\",\"1 Year Free Technical Support\"]]', '2018-08-19 01:30:38', '2018-08-28 15:52:32'),
(3, 'Advanced Level', '39,999', '[[\"Core and Advance Features Included\",\"20-25 Responsive Webpages\",\"Social Media Integration\",\"Google Map Integration\",\"One Time Content\",\"SEO Friendly\",\"Unlimited Disk Space\",\"Unlimited Bandwidth\",\"Unlimited E-mail Accounts\",\"SSL Included\",\".np Domain\",\"1 Year Free Technical Support\"]]', '2018-08-19 01:31:06', '2018-08-28 15:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `name`, `link`, `image`, `categories`, `created_at`, `updated_at`) VALUES
(1, 'Online GRE /SAT Examination', 'http://www.beyondboundaries.com.np/testexam/index.php?page=login', 'public/uploads/portfolio/1534625661.jpg', 'WebApps', '2018-08-18 15:09:21', '2018-08-18 15:09:21'),
(2, 'ZealTech Education', 'http://test.zealtecheducation.com/test/', 'public/uploads/portfolio/1535449691.jpg', 'Website', '2018-08-28 16:48:11', '2018-09-13 18:09:37'),
(4, 'Mody University', 'http://www.modyuniversity.ac.in/', 'public/uploads/portfolio/1535450185.jpg', 'Digital Marketing', '2018-08-28 16:56:25', '2018-08-28 17:15:44'),
(5, 'Silverlining Events', 'https://www.facebook.com/silverliningeventsnepal/', 'public/uploads/portfolio/1536837432.jpg', 'DigitalMarketing', '2018-09-13 18:17:12', '2018-09-13 18:17:12'),
(9, 'Alpha Korean Language School', 'http://alphakls.com.np/', 'public/uploads/portfolios/1539500169.JPG', 'Website', '2018-10-14 13:39:06', '2018-10-14 13:56:09'),
(10, 'Gaurishankar Thanka Painting & School Pvt. Ltd.', 'http://gaurishankartps.com.np', 'public/uploads/portfolios/1539500200.JPG', 'Website', '2018-10-14 13:50:17', '2018-10-14 13:56:40'),
(11, 'Kayakalpa Ayurved Clinic', 'https://www.facebook.com/kayakalpaayurvedclinic/', 'public/uploads/portfolio/1539501245.png', 'GraphicDesign', '2018-10-14 14:14:05', '2018-10-14 14:14:05'),
(12, 'Kayakalpa Ayurved Clinic', 'https://www.facebook.com/kayakalpaayurvedclinic/', 'public/uploads/portfolio/1539501536.jpg', 'DigitalMarketing', '2018-10-14 14:18:56', '2018-10-14 14:18:56'),
(13, 'Graphics Designing Service', 'http://www.onlinezeal.com/servicessingle/graphic-design', 'public/uploads/portfolio/1539501763.jpg', 'DigitalMarketing', '2018-10-14 14:22:43', '2018-10-14 14:22:43'),
(14, 'Web Design & Development', 'http://www.onlinezeal.com/servicessingle/website-design', 'public/uploads/portfolio/1539501811.jpg', 'DigitalMarketing', '2018-10-14 14:23:31', '2018-10-14 14:23:31'),
(22, 'Glow to Glow', 'https://www.facebook.com/glowtoglow/', 'public/uploads/portfolio/1536837235.jpg', 'DigitalMarketing', '2018-09-13 18:13:55', '2018-09-13 18:13:55');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_categories`
--

CREATE TABLE `portfolio_categories` (
  `portfolio_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `positionnumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `position`, `department`, `information`, `created_at`, `updated_at`, `positionnumber`) VALUES
(5, 'Content Writer', 'Marketing', '<h4>Requirements</h4>\r\n<p>Excellent writing and editing skills in English<br />Creative and innovative thinker and planner<br />&nbsp;Familiarity with web publications<br />Assists team members when needed to accomplish team goal<br />Ability to work independently or as an active member of team</p>', '2018-08-28 15:59:41', '2018-08-28 16:02:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacies`
--

CREATE TABLE `privacies` (
  `id` int(10) UNSIGNED NOT NULL,
  `privacy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `privacies`
--

INSERT INTO `privacies` (`id`, `privacy`, `created_at`, `updated_at`) VALUES
(1, '<p class=\"regular-text\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>\r\n<h3>Nam liber tempor cum soluta</h3>\r\n<p class=\"regular-text\">Nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Mirum est notare quam</h3>\r\n<p class=\"regular-text\">Littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h3>Nobis eleifend option congue nihil</h3>\r\n<p class=\"regular-text\">Imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Littera gothica, quam nunc</h3>\r\n<p class=\"regular-text\">Putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h3>Nam liber tempor cum soluta</h3>\r\n<p class=\"regular-text\">Nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Mirum est notare quam</h3>\r\n<p class=\"regular-text\">Littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>', '2018-10-15 00:19:50', '2018-10-15 00:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `avatar`, `user_id`, `about`, `facebook`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'public/uploads/avatars/avatar.png', 1, '<p>Company</p>', 'facebook', 'linkedin', '2018-08-18 14:57:53', '2018-08-18 15:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `website`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Kankali Secondary School', 'http://kankaliss.edu.np', 'Government Schools', '2018-08-22 01:36:08', '2018-08-22 01:47:21'),
(3, 'Zealtecheducation', 'http://www.zealtecheducation.com.np', 'Others', '2018-08-22 01:37:35', '2018-08-22 01:59:08'),
(4, 'Shanti Nikunj Secondary School', 'http://snss.edu.np', 'Government Schools', '2018-08-22 01:55:31', '2018-08-22 01:55:31'),
(5, 'Mahendra Ratna School', 'http://mrjlhsschool.edu.np', 'Government Schools', '2018-08-22 16:13:19', '2018-08-22 16:13:19'),
(6, 'Shree Bishwamitra School', 'http://vmgss.edu.np', 'Government Schools', '2018-08-29 12:26:39', '2018-08-29 12:26:39'),
(7, 'Bhanu Secondary School', 'http://bhanuss.edu.np', 'Government Schools', '2018-08-29 12:27:17', '2018-08-29 12:27:17'),
(8, 'Shree Pancha Secondary School', 'http://panchass.edu.np', 'Government Schools', '2018-08-29 12:27:43', '2018-08-29 12:27:43'),
(9, 'Surya Chandra Secondary School', 'http://suryachandrass.edu.np', 'Government Schools', '2018-08-29 12:28:13', '2018-08-29 12:28:13'),
(10, 'Shree Gadhimai Secondary School', 'http://gadhimaitms.edu.np', 'Government Schools', '2018-08-29 12:28:44', '2018-08-29 12:28:44'),
(11, 'Shree Nepal Rastriya Secondary School', 'http://nrssbadakholi.edu.np', 'Government Schools', '2018-08-29 12:29:06', '2018-08-29 12:29:06'),
(12, 'Prithivi Secondary School', 'http://pss.edu.np', 'Government Schools', '2018-08-29 12:30:00', '2018-08-29 12:30:00'),
(14, 'Shree Narad Adarsha Secondary School', 'http://nasschool.edu.np', 'Government Schools', '2018-08-29 12:30:23', '2018-08-29 12:30:23'),
(16, 'Adarsha Secondary School', 'http://aadarshass.edu.np', 'Government Schools', '2018-08-29 12:31:52', '2018-08-29 12:31:52'),
(17, 'Narendra Memorial Public Secondary School', 'http://nmpss.edu.np', 'Government Schools', '2018-08-29 12:32:23', '2018-08-29 12:32:23'),
(18, 'Shree Saraswati Secondary School', 'http://sssdakaha.edu.np', 'Government Schools', '2018-08-29 12:32:44', '2018-08-29 12:32:44'),
(19, 'Goraksha RatnaNath Secondary School', 'http://goraksharatna.edu.np', 'Government Schools', '2018-08-29 12:33:08', '2018-08-29 12:33:08'),
(20, 'Shree Dilpeshwar Secondary School', 'http://dilpeshwarss.edu.np', 'Government Schools', '2018-08-29 12:33:28', '2018-08-29 12:33:28'),
(21, 'Shree Surya Prakash Secondary School', 'http://suryaprakashss.edu.np', 'Government Schools', '2018-08-29 12:33:51', '2018-08-29 12:33:51'),
(22, 'Shree Satyavadi Secondary School', 'http://satyavadiss.edu.np', 'Government Schools', '2018-08-29 12:34:13', '2018-08-29 12:34:13'),
(23, 'Shree Bhawani Prasad Sakal Prasad Ram Prasad Janta School', 'http://bsrjss.edu.np', 'Government Schools', '2018-08-29 12:34:42', '2018-08-29 12:34:42'),
(24, 'Janasewa Secondary School', 'http://janasewasec.edu.np', 'Government Schools', '2018-08-29 12:35:32', '2018-08-29 12:35:32'),
(25, 'Shree Janta Secondary School', 'http://jantasskhurhuriya.edu.np', 'Government Schools', '2018-08-29 12:36:10', '2018-08-29 12:36:10'),
(26, 'Sukuna Secondary School', 'http://sukunass.edu.np', 'Government Schools', '2018-08-29 12:36:46', '2018-08-29 12:36:46'),
(27, 'Shree Tribhuvan Secondary School', 'http://tribhuwanss.edu.np', 'Government Schools', '2018-08-29 12:37:51', '2018-08-29 12:37:51'),
(28, 'Shree Bhanu Jan Secondary School', 'http://bhanujss.edu.np', 'Government Schools', '2018-08-29 12:38:31', '2018-08-29 12:38:31'),
(29, 'Brahma Rupa Secondary School', 'http://brahmarupass.edu.np', 'Government Schools', '2018-08-29 12:38:54', '2018-08-29 12:38:54'),
(30, 'Shree La Cha Mu Secondary School', 'http://lcmssjaleshwar.edu.np', 'Government Schools', '2018-08-29 12:39:49', '2018-08-29 12:39:49'),
(31, 'Shree Secondary School Ladavir', 'http://ladavirss.edu.np', 'Government Schools', '2018-08-29 12:40:11', '2018-08-29 12:40:11'),
(32, 'Janata Secondary School', 'http://jssm.edu.np', 'Government Schools', '2018-08-29 12:40:31', '2018-08-29 12:40:31'),
(33, 'Shree Manilek Secondary School', 'http://manilekss.edu.np', 'Government Schools', '2018-08-29 12:40:49', '2018-08-29 12:40:49'),
(34, 'Ram Janaki Hotel', 'http://www.ramjanakiguesthouse.com', 'Others', '2018-08-29 12:41:42', '2018-08-29 12:41:42'),
(35, 'NDC Education', 'http://www.ndceducation.com.np', 'Others', '2018-08-29 12:42:07', '2018-08-29 12:42:15'),
(36, 'Zealsale', 'http://www.zealsale.com.np', 'Others', '2018-08-29 12:42:50', '2018-08-29 12:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `servicescategory_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `subtitle`, `slug`, `content`, `image`, `icon`, `servicescategory_id`, `created_at`, `updated_at`, `meta_description`, `keywords`, `description`, `meta_title`) VALUES
(3, 'Website Design', 'Serving Creative Designs For Creative Branding', 'website-design', '<h3 class=\"semi-bold\">We create the best and competitive website design in Kathmandu for our clients.</h3>\r\n<p>Our design team qualify in planning and implementing the ideas as per the requirements. It has become mandatory in today&rsquo;s digital world to have a functional website to promote your business and get in touch with as much clients as possible throughout the globe. We, here at OnlineZeal, can help you to achieve that by providing you with some of the best website design in Kathmandu. It is already a tough task to build a website that can stand out from other websites with similar content. Website design in Kathmandu has become tricky in recent times. However, our designer team are capable of building unique and dynamic websites for our clients. We prioritize client satisfaction to upmost level. We coordinate with the clients timely to validate the progress so that the final outcome of the design meets the specified requirements. Our aim is to create responsive websites that are conveniently accessible by the end-users. Furthermore, we also provide Search Engine Optimization (SEO) for your website to keep your business always forward. This collaboratively produces the best website designs&nbsp;and we are more than capable of doing so. &nbsp;&nbsp;&nbsp;&nbsp;</p>', 'public/uploads/services/1538722229.jpg', 'icon-laptop-phone', 1, '2018-08-21 00:01:25', '2018-10-11 13:42:31', '<h3 class=\"semi-bold\">We create the best and competitive website design in Kathmandu for our clients...', 'Website Design in Kathmandu, Best website design company in nepal', 'Online Zeal is a leading IT company who provides website design in Kathmandu. Website design is a must for every business sectors these days. We follow modern Web Development technology to build beautiful Websites. We design and develop websites considering all aspects of Web Designs like color, brand identity and user experience.', 'Website Design in Kathmandu | Best website development company in Nepal'),
(4, 'Software Development', 'Before Software Can Be Reusable It First Has To Be Usable', 'software-development', '<h3>OnlineZeal is an IT company with a team of professionals capable of the best Software Development in Nepal.</h3>\r\n<p>Questions related to the feasibility and challenges of software are no problem for us when we are assigned to develop a software. We understand that a software is of utmost importance for a small as well as big business as of now in Nepal. So, Software development in Nepal has become a big game changer for an IT company. Whether it is an Inventory Management, Accounting, Invoicing, Billing or other, every company at some point needs a software. With our software development strategies, why not get one beforehand. Our team of expertise are familiar with the methodology in use today and can resolve any issues of our clients efficiently. The functional and robust software that we develop along with our routine validation with the clients are the key reasons for you to choose us for the best Software Development in Nepal. In addition to that, we are cost-effective in development and deployment of software applications. Our professional team are dedicated to automate your day to day business with the best GUI for your software requirements. We make sure the software you seek becomes the one you put in use for your daily business activities with the assurance of frequent maintenance and upgrades. When you take into account how much a custom software can improve your business&rsquo;s productivity, its lifetime costs are often lower than off-the-shelf products.</p>\r\n<p class=\"regular-text\">&nbsp;</p>', 'public/uploads/services/1538735201.jpg', 'icon-smartphone-embed', 2, '2018-08-22 16:10:57', '2018-10-11 17:49:15', '<h3>OnlineZeal is an IT company with a team of professionals capable of the best Software Developmen...', 'Software development in Nepal, Best software development in Nepal', 'Online Zeal is a leading IT company providing Software Development in Nepal. We have 6+ years of experienced  software  developer sand designer . We provide the best software development solution to maximize productivity.', 'Software Development in Nepal|Best software development company in Nepal'),
(5, 'Graphic Design', 'Reach New Height In Success Graph With Our Graphics Design', 'graphic-design', '<h3 class=\"regular-text\">OnlineZeal is an IT company that creates the best Graphic Design in Nepal.</h3>\r\n<p class=\"regular-text\">We have a team of problem solvers that are passionate about their work. The Nepal based graphic design team that we have in our disposal help people make their lives easier. Turning visions and ideas to reality is our forte. We have a unique structure which reflects our passion and creativity in the designs that we create. Understanding the importance of a professional looking Graphic Design in Nepal, we create a visual art for your website, business cards, logo etc. that represents your brand. You can remember us for assisting your business promotion through advertisement designs, logo, brochure designs and all sorts of branding. &nbsp;Creative branding is a must in today&rsquo;s world to stand out from your competitors in business world. The key feature that makes us unique is that we follow different methodology for any graphic design process. Your business success is our commitment and we help you get ahead with some of the best Graphic Design in Nepal. We, here at OnlineZeal, can help you grow your business with creative, beautiful yet inexpensive solutions.</p>', 'public/uploads/services/1535436633.jpg', 'icon-palette', 1, '2018-08-28 13:10:33', '2018-10-11 17:03:59', '<h3 class=\"regular-text\">OnlineZeal is an IT company that creates the best Graphic Design in Nepal.<...', 'Graphic design in kathmandu, Best graphic design company in nepal', 'Online Zeal is a leading IT company providing Graphic design in Nepal. We create graphic designs that represent the identity of your brand. we solve problems with creative,beautiful yet inexpensive graphic designs.', 'Graphic design in Kathmandu | Best Graphic design company in Nepal'),
(6, 'Content Management System', 'Best User Experience For Creating And Modifying Digital Contents', 'content-management-system', '<h3>Facilitating the control and management of contents for your websites with the best Content Management System in Nepal is a major working area of OnlineZeal.</h3>\r\n<p>A Content Management System enriches your experience by allowing creation and modification of digital contents for your website. Our team of developers are more than capable to integrate one of the entirely customizable and best Content Management System in Nepal into your existing website. We can even build such a system from scratch depending on the client&rsquo;s requirements. Our team works with the best Content Management System in Nepal currently used and we are trusted by our clients to fulfil their content management needs. Our optimized CMS will provide multiple users with varying authorizations to manage and update website contents. This will allow the users to create, edit, archive, publish, report, and distribute website contents and information saving them from the hassle of making changes in the code itself. Moreover, the CMS we integrate for our clients are completely responsive for every device for user convenience and efficiency. You can make necessary changes and update content in your website or web application by accessing the CMS anytime anywhere. The Content Management System (CMS) platform that we specialize in is WordPress.</p>', 'public/uploads/services/1535437257.jpg', 'icon-papers', 2, '2018-08-28 13:20:57', '2018-10-11 18:40:54', '<h3>Facilitating the control and management of contents for your websites with the best Content Mana...', 'Content Management System in Nepal', 'Online Zeal is a leading IT company providing Content Management  in Nepal. We  specialize in responsive web templates and design for content management. We make sure multiple users can manage the digital contents through the content management system.', 'Content Management System in Nepal|Best content management system in Nepal'),
(7, 'Social Media Marketing', 'Create Better Customer Engagement With Our Social Media Marketing Strategy', 'social-media-marketing', '<h3>Social Media Marketing in Kathmandu is a booming marketing strategy and one of the many valuable services that OnlineZeal provides.</h3>\r\n<p>It is a powerful way for businesses of all scales like education, travels, hotels, consultancies etc. to reach new heights and get in touch with more customers in Kathmandu and other parts of the world. It has become a necessity to get in direct touch with your customers today since they are already interacting with brands through social media platforms like Facebook, Twitter, Instagram, LinkedIn, etc. It would be a mistake and a loss for your business if you are missing out on this golden opportunity to boost your market with the best Social Media Marketing in Kathmandu. Bring remarkable success to your business by inducing great marketing on such social media platforms, attracting devoted brand advocates in Kathmandu and even driving leads and sales around the world. We, at OnlineZeal, focus in strategy development and facilitate reliable and cost-effective Social Media Marketing in Kathmandu. Our team thoroughly studies the current market and marketing strategies in use. We make sure the advertisements for your business or company is delivered to the targeted customers and help you raise your brand awareness.</p>', 'public/uploads/services/1535437684.jpg', 'icon-share', 3, '2018-08-28 13:27:05', '2018-10-11 18:43:26', '<h3>Social Media Marketing in Kathmandu is a booming marketing strategy and one of the many valuable...', 'Social Media Marketing in kathmandu, Best social media marketing in nepal', 'Online Zeal is a leading IT company providing social media marketing in Nepal. social media marketing can bring remarkable success to your business in the digital world. We help your business prosper through our social media marketing.', 'Social Media Marketing in Kathmandu|Best social media marketing company in Nepal'),
(9, 'Content Marketing', 'Exceed Your Customer Reach With Our Content Marketing Strategies', 'content-marketing', '<h3 style=\"text-align: left;\">OnlineZeal, as a leading IT company, focuses on building strong relationship with targeted audience with unique content marketing in Kathmandu.</h3>\r\n<p style=\"text-align: left;\">Marketing your business with a high quality content is the new trend of today&rsquo;s digital world. The business who succeeds to do that will have succeeded to attract a rapidly growing amount of customers each day. Content Marketing in Kathmandu has become a form of long-term strategic marketing initiative that helps to expand the customer base and attain profitable customer action. The core of your business marketing should be useful content and if you want to be counted as one of the leading brands, content marketing is the most essential and quick way to make it possible. Our target is to boost your business content as far as possible so that our content marketing in Kathmandu&nbsp;helps you collect clients from all over the world. A good and consistent content is the best way to reach and captivate rewarding customers by increasing traffic into the website. It is noticed that B2B decision makers and clients are positively inclined towards a branded content. Our Kathmandu based team has a better understanding of this process and is dedicated to create a better customer engagement for your website and web applications. &nbsp;&nbsp;&nbsp;&nbsp;</p>', 'public/uploads/services/1538732848.jpg', 'icon-news', 3, '2018-08-28 13:49:58', '2018-10-11 18:51:13', '<h3 style=\"text-align: left;\">OnlineZeal, as a leading IT company, focuses on building strong relati...', 'Content Marketing in kathmandu, Best content marketing  in nepal', 'Online Zeal is a leading IT company providing content marketing in Nepal. we assist you on building strong relationship with the targeted audience with our content marketing strategy. our content marketing focuses on attracting and expanding the customer base.', 'Content Marketing in Kathmandu|Best Content Marketing company in Nepal'),
(10, 'Search Engine Optimization', 'Level Up Your Rank In Top Search Engines By Increasing Traffic', 'search-engine-optimization', '<h3>Providing one of the best Search Engine Optimization in Kathmandu is a vital service that OnlineZeal, as an IT company, delivers to our valuable clients.</h3>\r\n<p>We help you improve your website&rsquo;s visibility and put it high on the list of results in popular search engines, especially on google.com. When your website improves in search engine rankings, you can easily maximize the number of visitors to your site. That is how Search Engine Optimization helps you increase traffic into you website and gain the numbers of visits beyond your expectations. The Search Engine Optimization in Kathmandu has developed a lot in recent times and use of google has been increasing rapidly. It has hence become extremely crucial to have a website ranked at least in top 5 of search engine results as the ones very down below or even at the second page will go unvisited. Our team at OnlineZeal understands the necessity of Search Engine Optimization in Kathmandu at today&rsquo;s date&nbsp;and helps our clients solve their problem of reaching out to more people every day. Our SEO expert is well aware of the on and offsite Google marketing strategies and can broaden your contents across your website, social media platforms, blogs and so much more through the increase in keyword and landing page optimization.</p>', 'public/uploads/services/1538734809.jpg', 'icon-magnifier', 3, '2018-08-28 13:55:20', '2018-10-11 18:53:42', '<h3>Providing one of the best Search Engine Optimization in Kathmandu is a vital service that Online...', 'Search Engine Optimization in kathmandu, Best search engine optimization in nepal', 'Online Zeal is a leading IT company providing Search Engine Optimization in Kathmandu. We have 6+ years of experience on search engine optimization(seo). we can optimize your website to boost your business by increasing traffic using our search engine optimization technique.', 'Search Engine Optimization in Kathmandu |Best Search Engine Optimization company in Nepal'),
(11, 'Network Security', 'Cyber-Security Is A Global Responsibility', 'network-security', '<h3>With all your essential data flowing over network, OnlineZeal makes sure they have the best network security in Kathmandu.</h3>\r\n<p>Network security helps keep your proprietary information private and secured. It helps target a wide range of threats and stops them from spreading on your network. We enforce the best network security in Kathmandu always implementing policy and controls. We provide these services to protect the company&rsquo;s internal infrastructures from unwanted and unauthorized access, mishandlings, and attacks. There have been a lot of recent cases of cyber-attack in Kathmandu. We evaluate the security of your network connections to implement firewalls and other hardware or software security factors for protection of your personal/company data at OnlineZeal. We make sure the threats like malware and phishing are detectable and preventable. Network Security is one of the mandatory service that an IT company should provide and we, at OnlineZeal assure to meet the client&rsquo;s privacy needs to secure their network and infrastructures. In case of Network Security in Kathmandu, it is&nbsp; difficult to preserve data integrity from unauthorized access, but our team of experienced members will help you keep check on your data security thereby protecting your reputation. With the hackers getting better each day around the world and even Kathmandu, we make sure our network security methodologies are updated and unbreakable.</p>', 'public/uploads/services/1538732165.jpg', 'icon-chart-settings', 4, '2018-08-28 14:11:43', '2018-10-11 18:58:30', '<h3>With all your essential data flowing over network, OnlineZeal makes sure they have the best netw...', 'Network Security in kathmandu, Best network security company in nepal', 'Online Zeal is a leading IT company providing Network Security in Kathmandu. We provide network security that protects your proprietary information from attack . we protect your integrity and prevent unauthorized access with our network security.', 'Network Security in Kathmandu|Best Network Security company in Nepal'),
(12, 'Web Hosting', 'The Fast And Reliable Managed Hosting Solution For Your Business', 'web-hosting', '<h3>Experience the best and convenient Web Hosting in Kathmandu from OnlineZeal Pvt. Ltd.</h3>\r\n<p>We also provide domain names for your web hosting and make your FTP access highly comfortable. OnlineZeal is your one stop solution if you are anxious about having a functional website set up just after opening a brand new business in Kathmandu.&nbsp; We assure you that our web hosting plans are WordPress compatible so that you can access and update your files with the CMS without any hassle. We also offer plans for the best web hosting in Kathmandu for our clients. The fastest and reliable hosting solution for your business in such affordable costs. OnlineZeal helps you get all the benefits of our hosting services. Like most companies that provide web hosting in Kathmandu, we also offer Linux hosting as it is more economical and supports maximum technologies in use today. You can refer to our hosting plans to clear your confusions regarding packages and pricing.</p>\r\n<p>We have various hosting plans. <a href=\"/hosting\">View hosting plans &gt;&gt;.</a></p>', 'public/uploads/services/1535440869.jpg', 'icon-server', 5, '2018-08-28 14:21:09', '2018-10-15 01:27:54', '<h3>Experience the best and convenient Web Hosting in Kathmandu from OnlineZeal Pvt. Ltd.</h3>\r\n<p>W...', 'Web Hosting in kathmandu, Best web hosting company in nepal', 'Online Zeal is a leading IT company providing Web Hosting  in Kathmandu. We have 6+ years of experience in web hosting. We follow modern Web hosting technique to fulfill client queries.', 'Web Hosting in Kathmandu|Best Web Hosting in Nepal');

-- --------------------------------------------------------

--
-- Table structure for table `servicescategories`
--

CREATE TABLE `servicescategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicescategories`
--

INSERT INTO `servicescategories` (`id`, `name`, `icon`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Design', 'icon-palette', 'Our competitive and trend-setting designs will help your business bloom online.', '2018-08-18 15:03:48', '2018-10-05 14:44:55'),
(2, 'Development', 'icon-laptop-phone', 'A convenient software with the best GUI is the utmost need in the digital world.', '2018-08-18 15:04:09', '2018-10-05 14:45:19'),
(3, 'Online Marketing', 'icon-chart-growth', 'Digitizing your market strategies to accomplish a global branding of your business.', '2018-08-18 15:04:22', '2018-10-05 14:45:35'),
(4, 'Technology', 'icon-puzzle', 'With great technology comes great responsibility. We help you use your tech securely.', '2018-08-18 15:04:35', '2018-10-05 15:44:01'),
(5, 'Web Hosting', 'icon-cloud-upload', 'Get your website a worthy domain and storage with our optimal hosting plans.', '2018-08-18 15:04:47', '2018-10-05 14:46:04');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `subtitle`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Great Website Adds Great Value', 'We Craft Beautiful Websites To Promote Your Brand', 'public/uploads/sliders/1534625259.jpg', NULL, '2018-08-18 15:02:39', '2018-10-05 14:42:05'),
(2, 'Increase Your Return On Investment', 'Digital Marketing Can Increase Your ROI Significantly', 'public/uploads/sliders/1538737524.jpg', NULL, '2018-08-18 15:02:53', '2018-10-05 18:05:24');

-- --------------------------------------------------------

--
-- Table structure for table `successes`
--

CREATE TABLE `successes` (
  `id` int(10) UNSIGNED NOT NULL,
  `clients` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projects` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coverimage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `successes`
--

INSERT INTO `successes` (`id`, `clients`, `projects`, `hours`, `coverimage`, `created_at`, `updated_at`) VALUES
(1, '22', '34', '4577', 'public/uploads/success/1233.jpg', NULL, '2018-09-07 19:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `terms`, `created_at`, `updated_at`) VALUES
(1, '<p>&nbsp;</p>\r\n<p class=\"regular-text\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>\r\n<h3>Nam liber tempor cum soluta</h3>\r\n<p class=\"regular-text\">Nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Mirum est notare quam</h3>\r\n<p class=\"regular-text\">Littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h3>Nobis eleifend option congue nihil</h3>\r\n<p class=\"regular-text\">Imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Littera gothica, quam nunc</h3>\r\n<p class=\"regular-text\">Putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h3>Nam liber tempor cum soluta</h3>\r\n<p class=\"regular-text\">Nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>\r\n<h3>Mirum est notare quam</h3>\r\n<p class=\"regular-text\">Littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>', '2018-10-14 06:10:36', '2018-10-14 06:58:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Online Zeal', 'contact@onlinezeal.com', '$2y$10$KtnlZIvyPVEtnNEzIaa2zedSmccbm1IEZqtW83F8TRXmgo1QUsvna', 1, 'QSmKWuW3WIsmBCdjFqiwRcPNkve7Gfa5nNDxHpDFTiEbnQrtFo1E0Fw3vNOR', '2018-08-18 14:57:53', '2018-09-30 14:28:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrers`
--
ALTER TABLE `carrers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostings`
--
ALTER TABLE `hostings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  ADD KEY `portfolio_categories_portfolio_id_index` (`portfolio_id`),
  ADD KEY `portfolio_categories_category_id_index` (`category_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privacies`
--
ALTER TABLE `privacies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicescategories`
--
ALTER TABLE `servicescategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `successes`
--
ALTER TABLE `successes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `carrers`
--
ALTER TABLE `carrers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hostings`
--
ALTER TABLE `hostings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `privacies`
--
ALTER TABLE `privacies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `servicescategories`
--
ALTER TABLE `servicescategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `successes`
--
ALTER TABLE `successes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  ADD CONSTRAINT `portfolio_categories_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
