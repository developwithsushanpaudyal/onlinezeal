@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
   <section id="basic-examples">
     <div class="row">
       <div class="col-12 mt-3 mb-1">
         <h4 class="text-uppercase">View Success Details</h4>
       </div>
     </div>
     <div class="row match-height">
       @foreach($success as $d)
       <div class="col-xl-3 col-md-6 col-sm-12">
         <div class="card">
           <div class="card-content">
             <div class="card-body">
               <h4 class="card-title"><span>Clients</span> : {{$d->clients}}</h4>
               <h6 class="card-title"><span>Projects </span> : {{$d->projects}}</h6>
               <h6 class="card-title"><span>Work Hours </span> : {{$d->hours}}</h6>

             </div>
             <img class="img-fluid" src="{{asset($d->coverimage)}}" alt="">
             <div class="card-body">
               <a href="{{route('success.edit', ['id' => $d->id])}}">
                 <button class="btn btn-success"><i class="la la-edit"></i></button>
               </a>

             </div>
           </div>
         </div>
       </div>
       @endforeach





     </div>
   </section>
   <!-- // Basic example section end -->
@endsection
