@extends('admin.includes.app')
@section('content')
<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Insert Proceess</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('process.update', ['id' => $process->id ])}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Services Process</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="title">Title</label>
                        <div class="col-md-9">
                          <input type="text" id="title" class="form-control" placeholder="Title"
                          name="title" value="{{$process->title}}">
                        </div>
                      </div>


                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="icon-picker">Icon</label>
                      <div class="col-md-9">
                        <select class="form-control" name="icon">
                          <option value="{{$process->icon}}" @if($process->icon == $process->icon) selected @endif>{{$process->title}}</option>
                          <option value="icon-bubble-user">Consultation</option>
                          <option value="icon-pencil-ruler">Wire Frame</option>
                          <option value="icon-laptop-phone">Final Design</option>
                          <option value="icon-magnifier">Exploration</option>
                          <option value="icon-pencil3">Sketches</option>
                          <option value="icon-vector">Digitization</option>
                          <option value="icon-palette">Color</option>
                          <option value="icon-star">Final Graphic</option>
                          <option value="icon-smartphone-embed">Development</option>
                          <option value="icon-phone">Final App</option>
                          <option value="icon-magnifier">Discover</option>
                          <option value="icon-archery">Startegy</option>
                          <option value="icon-puzzle">Optimization</option>
                          <option value="icon-papers">Content</option>
                          <option value="icon-pie-chart">Reporting</option>
                          <option value="icon-papers">Content</option>
                          <option value="icon-flag">Campaign</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                           <label class="col-md-3 label-control">Upload Image</label>
                           <div class="col-md-9">
                             <label id="projectinput8" class="file center-block">
                               <input type="file" id="file" name="image">
                               <span class="file-custom"></span>
                             </label> <br>
                             <img src="{{asset($process->image)}}" class="img-responsive" style="width: 200px;">
                           </div>
                         </div>

                    <div class="form-group row">
                          <label class="col-md-3 label-control" for="description">Content</label>
                          <div class="col-md-9">
                            <textarea id="content" rows="5" class="form-control" name="content" placeholder="Content">
                            {{$process->content}}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->
@endsection
