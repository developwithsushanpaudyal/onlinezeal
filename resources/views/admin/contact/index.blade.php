@extends('admin.includes.app')
@section('content')

    <!-- Content types section start -->
    <section id="content-types">
        <div class="row">
            <div class="col-12 mt-3 mb-1">
                <h4 class="text-uppercase">Contact Page Details</h4>

            </div>
        </div>
        <div class="row match-height">
            @foreach($contact as $d)
                <div class="col-xl-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="card-body">

                                        <h4 class="card-title">Mobile:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->mobile}}</h6>
                                        <br>
                                        <h4 class="card-title">Phone No:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->phone}}</h6>
                                        <br>
                                        <h4 class="card-title">Email:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->email}}</h6>


                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="card-body">
                                        <h4 class="card-title">Facebook:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->facebook}}</h6>
                                        <br>
                                        <h4 class="card-title">Twitter:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->twitter}}</h6>
                                        <br>
                                        <h4 class="card-title">Linkedin:</h4>
                                        <h6 class="card-subtitle text-muted">{{$d->linkedin}}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="{{route('contact.edit', ['id' => $d->id])}}">
                                    <button class="btn btn-success"><i class="la la-edit"></i> Edit Info</button>
                                </a>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </section>
    <!-- Content types section end -->

@endsection
