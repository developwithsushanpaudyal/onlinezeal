@extends('admin.includes.app')
@section('content')

   <!-- Basic form layout section start -->
   <section id="horizontal-form-layouts">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Edit Contact Info</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                     </ul>
                  </div>
               </div>
               <div class="card-content collpase show">
                  <div class="card-body">
                     <form class="form form-horizontal" action="{{route('contact.update', ['id' => $contact->id ])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                           <h4 class="form-section"><i class="ft-user"></i> Contact Info</h4>


                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="mobile">Mobile Number</label>
                              <div class="col-md-9">
                                 <input type="text" id="mobile" class="form-control" placeholder="Mobile Number"
                                        name="mobile" value="{{$contact->mobile}}">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="phone">Phone Number</label>
                              <div class="col-md-9">
                                 <input type="text" id="phone" class="form-control" placeholder="Phone Number"
                                        name="phone" value="{{$contact->phone}}">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="email">Email Address</label>
                              <div class="col-md-9">
                                 <input type="text" id="email" class="form-control"
                                        name="email" value="{{$contact->email}}">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="facebook">Facebook</label>
                              <div class="col-md-9">
                                 <input type="text" id="facebook" class="form-control"
                                        name="facebook" value="{{$contact->facebook}}">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="twitter">Twitter</label>
                              <div class="col-md-9">
                                 <input type="text" id="twitter" class="form-control"
                                        name="twitter" value="{{$contact->twitter}}">
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="linkedin">Linkedin</label>
                              <div class="col-md-9">
                                 <input type="text" id="linkedin" class="form-control"
                                        name="linkedin" value="{{$contact->linkedin}}">
                              </div>
                           </div>


                        </div>
                        <div class="form-actions text-center">
                           <button type="submit" class="btn btn-primary" name="submit">
                              <i class="la la-check-square-o"></i> Save
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>


   </section>
   <!-- // Basic form layout section end -->
@endsection
