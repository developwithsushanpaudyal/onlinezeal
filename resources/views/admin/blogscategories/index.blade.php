@extends('admin.includes.app')
@section('content')
<!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Blogs Category Lists</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>S.N.</th>
                          <th>Name</th>
                          <th>Icon</th>
                          <th>Description</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($blogscategories as $blogscategory)
                        <tr>
                          <td>{{$loop->index +1}}</td>
                          <td>{{$blogscategory->name}}</td>
                          <td><i class="{{$blogscategory->icon}}"></i></td>
                          <td>{{$blogscategory->description}}</td>
                          <td>
                            <a href="{{route('blogcategory.edit', ['id' => $blogscategory->id])}}">
                              <button class="btn btn-success"><i class="la la-edit"></i></button>
                            </a>
                          </td>
                          <td>
                            <a href="{{route('blogcategory.delete', ['id' => $blogscategory->id])}}">
                              <button class="btn btn-danger"><i class="la la-trash-o"></i></button>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Zero configuration table -->
@endsection


@section('styles')
@endsection

@section('scripts')

@endsection
