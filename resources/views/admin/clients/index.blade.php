@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
   <section id="basic-examples">
     <div class="row">
       <div class="col-12 mt-3 mb-1">
         <h4 class="text-uppercase">View Clients</h4>
       </div>
     </div>
     <div class="row match-height">
       @foreach($clients as $client)
       <div class="col-xl-3 col-md-6 col-sm-12">
         <div class="card">
           <div class="card-content">
             <div class="card-body">
               <h4 class="card-title">{{$client->title}}</h4>
             </div>
             <img class="img-fluid" src="{{asset($client->image)}}" alt="{{$client->title}}">
             <div class="card-body text-center">
               <p class="card-text">{{$client->links}}</p>
               <a href="{{route('clients.edit',['id' => $client->id ])}}">
                 <button class="btn btn-success"><i class="la la-edit"></i></button>
               </a>
               <a href="{{route('clients.delete',['id' => $client->id ])}}" onclick="return confirm('Are you sure?')">
                 <button class="btn btn-warning" className="btn-warning"><i class="la la-trash-o"></i></button>
               </a>
             </div>
           </div>
         </div>
       </div>
       @endforeach





     </div>
   </section>
   <!-- // Basic example section end -->
@endsection
