@extends('admin.includes.app')
@section('content')

<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Edit Client</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('clients.update',['id' => $clients->id])}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Clients Info</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="clientname">Client Name</label>
                        <div class="col-md-9">
                          <input type="text" id="clientname" class="form-control" placeholder="Name"
                          name="title" value="{{$clients->title}}">
                        </div>
                      </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="link">Website Link</label>
                      <div class="col-md-9">
                        <input type="text" id="link" class="form-control" placeholder="Website URL"
                        name="links"  value="{{$clients->links}}">
                      </div>
                    </div>



                  <div class="form-group row">
                         <label class="col-md-3 label-control">Image</label>
                         <div class="col-md-9">
                           <label id="projectinput8" class="file center-block">
                             <input type="file" id="file" name="image">
                             <span class="file-custom"></span>
                           </label> <br>
                           <img src="{{asset($clients->image)}}" class="img-responsive" style="width: 200px;">

                         </div>
                       </div>


                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->

@endsection
