@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
   <section id="basic-examples">
     <div class="row">
       <div class="col-12 mt-3 mb-1">
         <h4 class="text-uppercase">View Sliders</h4>
       </div>
     </div>
     <div class="row match-height">
       @foreach($sliders as $slider)
       <div class="col-xl-3 col-md-6 col-sm-12">
         <div class="card">
           <div class="card-content">
             <div class="card-body">
               <h4 class="card-title">{{$slider->title}}</h4>
               <h6 class="card-subtitle text-muted">{{$slider->subtitle}}</h6>
             </div>
             <img class="img-fluid" src="{{asset($slider->image)}}" alt="{{$slider->title}}">
             <div class="card-body">
               <p class="card-text">{{$slider->link}}</p>
               <a href="{{route('slider.edit', ['id' => $slider->id])}}">
                 <button class="btn btn-success"><i class="la la-edit"></i></button>
               </a>
               <a href="{{route('slider.delete', ['id' => $slider->id])}}" onclick="return confirm('Are you sure?')">
                 <button class="btn btn-warning" className="btn-warning"><i class="la la-trash-o"></i></button>
               </a>
             </div>
           </div>
         </div>
       </div>
       @endforeach





     </div>
   </section>
   <!-- // Basic example section end -->
@endsection
