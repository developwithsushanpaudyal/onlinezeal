@extends('admin.includes.app')
@section('content')

<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Create a New Category</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Services Categories</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="categoryname">Category Name</label>
                        <div class="col-md-9">
                          <input type="text" id="categoryname" class="form-control" placeholder="Category Name"
                          name="name">
                        </div>
                      </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="icon-picker">Icon</label>
                      <div class="col-md-9">
                        <select class="form-control" name="icon">
                          <option value="icon-palette">Design</option>
                          <option value="icon-laptop-phone">Development</option>
                          <option value="icon-chart-growth">Online Marketing</option>
                          <option value="icon-puzzle">Technology</option>
                          <option value="icon-cloud-upload">Web Hosting</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                          <label class="col-md-3 label-control" for="description">Description</label>
                          <div class="col-md-9">
                            <textarea id="description" rows="5" class="form-control" name="description" placeholder="Description"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->

@endsection

@section('scripts')

@endsection
