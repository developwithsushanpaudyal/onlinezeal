@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
        <section id="basic-examples">
          <div class="row">
            <div class="col-12 mt-3 mb-1">
              <h4 class="text-uppercase">Services Listing</h4>

            </div>
          </div>
          <div class="row match-height">
@foreach($services as $service)
            <div class="col-xl-3 col-md-6 col-sm-12">
              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h4 class="card-title">{{$service->title}}</h4>
                    <h6 class="card-subtitle text-muted">{{$service->subtitle}}</h6>
                  </div>
                  <img class="img-fluid" src="{{asset($service->image)}}">
                  <div class="card-body">
                    <p class="card-text">{!! substr($service->content , 0 ,100) !!} {{ strlen($service->content )>100? "..." : "" }}</p>
                    <a href="{{route('service.edit', ['id' => $service->id])}}">
                      <button class="btn btn-success"><i class="la la-edit"></i></button>
                    </a>
                    <a href="{{route('service.delete', ['id' => $service->id])}}" onclick="return confirm('Are you sure?')">
                      <button class="btn btn-warning" className="btn-warning"><i class="la la-trash-o"></i></button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
@endforeach
<p class="m-b-0 m-t-10">{!! substr($service->body , 0 ,200) !!} {{ strlen($service->body )>200? "..." : "" }}</p>
          </div>
        </section>
        <!-- // Basic example section end -->
@endsection
