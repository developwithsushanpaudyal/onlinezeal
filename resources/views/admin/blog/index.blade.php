@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
        <section id="basic-examples">
          <div class="row">
            <div class="col-12 mt-3 mb-1">
              <h4 class="text-uppercase">Blogs List</h4>

            </div>
          </div>
          <div class="row match-height">
@foreach($blog as $blogs)
            <div class="col-xl-3 col-md-6 col-sm-12">
              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h4 class="card-title">{{$blogs->title}}</h4>
                    <h6 class="card-subtitle text-muted">{{$blogs->subtitle}}</h6>
                    <h5 class="card-text">{{$blogs->author}}</h5>
                  </div>
                  <img class="img-fluid" src="{{ asset('public/front/images/blog/large/'.$blogs->image)}}">
                  <div class="card-body">
                    <p class="card-text">{!! substr($blogs->body , 0 ,100) !!} {{ strlen($blogs->body )>100? ".." : "" }}</p>
                    <a href="{{route('blog.edit', ['id' => $blogs->id])}}">
                      <button class="btn btn-success"><i class="la la-edit"></i></button>
                    </a>
                    <a href="{{route('blog.delete', ['id' => $blogs->id])}}" onclick="return confirm('Are you sure?')">
                      <button class="btn btn-warning" className="btn-warning"><i class="la la-trash-o"></i></button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
@endforeach

          </div>
        </section>
        <!-- // Basic example section end -->
@endsection
