@extends('admin.includes.app')
@section('content')
<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Create a New Post</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('blog.update', ['id' => $blog->id ])}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Blogs</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="title">Title</label>
                        <div class="col-md-9">
                          <input type="text" id="title" class="form-control" placeholder="Title"
                          name="title" value="{{$blog->title}}">
                        </div>
                      </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="title">Sub Title</label>
                      <div class="col-md-9">
                        <input type="text" id="subtitle" class="form-control" placeholder="Sub Title"
                        name="subtitle" value="{{$blog->subtitle}}">
                      </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 label-control" for="author">Author name</label>
                        <div class="col-md-9">
                          <input type="text" id="author" class="form-control" placeholder="Author name"
                          name="author" value="{{$blog->author}}">
                        </div>
                    </div>  

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="icon-picker">Categories</label>
                      <div class="col-md-9">
                        <select class="form-control" name="blogscategory_id">
                          @foreach($blogscategories as $blogcategory)
                          <option value="{{$blogcategory->id}}" @if($blogcategory->id == $blog->blogscategory_id) selected @endif>{{$blogcategory->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>




                    <div class="form-group row">
                      <label class="col-md-3 label-control">Change Image</label>
                      <div class="col-md-9">
                        <label id="" class="file center-block">
                          <input type="file" id="file" onchange="return fileValidation()" name="image">
                          <span class="file-custom"></span>
                        </label>
                       <input type="hidden" name="current_image" value="{{$blog->image}}">
                       @if(!empty($blog->image))
                       <img src="{{ asset('public/front/images/blog/large/'.$blog->image ) }}" alt="Img" style="width:100px;height:80px;">
                       @endif
                      </div>
                    </div>

                         <div class="form-group row">
                          <label class="col-md-3 label-control" for="video">Video Tag</label>
                          <div class="col-md-9">
                            <input type="text" id="video" class="form-control" placeholder="Add video tag"
                            name="video" value="{{$blog->video}}">
                          </div>
                        </div>

                    <div class="form-group row">
                          <label class="col-md-3 label-control" for="description">Body</label>
                          <div class="col-md-9">
                            <textarea id="body" rows="15" class="form-control my-editor" name="body" placeholder="Write a blog here..">
                              {{$blog->body}}
                            </textarea>
                          </div>
                        </div>
                      </div>

  

                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->
@endsection

@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.6/tinymce.min.js"></script>
  <script>
      var editor_config = {
          // path_absolute : "/",
          path_absolute:"{{ url('/') }}/",

          selector: "textarea.my-editor",
          plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          relative_urls: false,
          file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
              if (type == 'image') {
                  cmsURL = cmsURL + "&type=Images";
              } else {
                  cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.open({
                  file : cmsURL,
                  title : 'Filemanager',
                  width : x * 0.8,
                  height : y * 0.8,
                  resizable : "yes",
                  close_previous : "no"
              });
          } ,

          //  Add Bootstrap Image Responsive class for inserted images
          image_class_list: [
              {title: 'None', value: ''},
              {title: 'Bootstrap responsive image', value: 'img-responsive'},
          ]

      };

      tinymce.init(editor_config);
  </script>
  @endsection
