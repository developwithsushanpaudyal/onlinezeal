@extends('admin.includes.app')
@section('content')

<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Update Package</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                  <form class="form form-horizontal" action="{{ route('package.update',$packageDetails->id) }}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Package Info</h4>


                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="packagename">Package Name</label>
                            <div class="col-md-9">
                                <input type="text" id="packagename" class="form-control" placeholder="Name"
                            name="name" value="{{ $packageDetails->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="packageprice">Package Price</label>
                            <div class="col-md-9">
                                <input type="text" id="packageprice" class="form-control" placeholder="Price"
                                name="price" value="{{ $packageDetails->price }}">
                            </div>
                        </div>

                        <?php $details = json_decode($packageDetails->description); ?> 
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="desc">Description</label>
                            {{-- <div id="parent"> --}}
                            <div class="col-md-9" id="parent">                    
                            @for($i=0;$i< sizeof($details[0]);$i++)    
                              <input type="text" name="desc[]"  id="desc" class="form-control" value="{{$details[0][$i]}}">
                            @endfor
                            </div>
                            {{-- </div> --}}
                            
                          </div> <br>
                          <div class="text-center">
                          <button class="btn btn-success" type="button" style="margin-right:10px;" onclick="addItem_desc();"> + </button>

                          </div>

                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->

        <script>

        var itemID = 0;
      
      
      function addElement(parentId, elementTag, elementId, html) { // Adds an element to the document 
      
      var p = document.getElementById(parentId);
      
      var newElement = document.createElement(elementTag);
      
        newElement.setAttribute('class', 'controls');
      
        newElement.setAttribute('id',elementId);
      
        newElement.innerHTML = html;
      
         p.appendChild(newElement);
      
        }
      
      
      
      function removeElement(elementId) {
      
      // Removes an element from the document
      
      var element = document.getElementById(elementId);
      
        element.parentNode.removeChild(element);
      
      }
      
      function addItem_desc(){ 
      
      itemID++; // increment fileId to get a unique ID for the new element
      
      var html = "<input type='text' class='form-control' name='desc[]' id='desc"+itemID+"' >"+
      
      " <a onclick=javascript:removeElement('desc_div"+itemID+"');" +
      
      " return false;'><button class='btn btn-danger buttonwa'> - </button></a>";      
      
      addElement('parent', 'div', 'desc_div'+itemID, html);
      
      }
      
      
</script>      

@endsection


@section('styles')
  <style>
    .buttonwa{
      margin-top: 20px;
      margin-bottom: 20px;
    }
    #desc1{
      margin-top: 20px;
    }
  </style>
@endsection