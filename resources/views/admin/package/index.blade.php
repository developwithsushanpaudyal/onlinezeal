@extends('admin.includes.app')
@section('content')
<!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Package Lists</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>ID.</th>
                          <th>Name</th>
                          <th>Price</th>
                          <th>Description</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($packages as $package )
                        <tr>
                          <td>{{$package->id}}</td>
                          <td>{{$package->name}}</td>
                          <td>{{$package->price}}</td>
                          <td><?php $details = json_decode($package->description); ?> 
                          <ul>
                            @for($i=0;$i< sizeof($details[0]);$i++) 
                            <li>
                                {{$details[0][$i]}}
                            </li>
                            @endfor
                          </ul> 
                          </td>
                          <td>
                            <a href="{{ route('package.edit', $package->id) }}">
                              <button class="btn btn-success"><i class="la la-edit"></i></button>
                            </a>
                          </td>
                          <td>
                            <a href="{{ route('package.delete',$package->id) }}">
                              <button onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="la la-trash-o"></i></button>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>S.N.</th>
                          <th>Name</th>
                          <th>Icon</th>
                          <th>Description</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Zero configuration table -->
@endsection


@section('styles')
@endsection

@section('scripts')

@endsection
