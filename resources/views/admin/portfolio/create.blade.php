@extends('admin.includes.app')
@section('content')

<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Create a Portfolio</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('portfolio.store')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Portfolio Info</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="slidername">Name</label>
                        <div class="col-md-9">
                          <input type="text" id="slidername" class="form-control" placeholder="Name"
                          name="name">
                        </div>
                      </div>


                    <div class="form-group row">
                    <label class="col-md-3 label-control" for="link">Link</label>
                    <div class="col-md-9">
                      <input type="text" id="link" class="form-control" placeholder="Link"
                      name="link">
                    </div>
                  </div>

                  <div class="form-group row">
                  <label class="col-md-3 label-control" for="icon-picker">Categories</label>
                  <div class="col-md-9">
                    <select class="form-control" name="categories">
                      @foreach($categories as $category)
                      <option value="{{$category->name}}" >{{$category->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>


                <div class="form-group row">
                  <label class="col-md-3 label-control">Upload Image</label>
                  <div class="col-md-9">
                    <label id="image" class="file center-block">
                      <input type="file" id="file" name="image">
                      <span class="file-custom"></span>     
                    </label>
                   </div>                        
               </div>
                   
                     <div class="form-group row">
                        <label class="col-md-3 label-control" for="description">Description</label>
                        <div class="col-md-9">
                          <textarea id="description" rows="15" class="form-control my-editor" name="description" placeholder="Write a description here.."></textarea>
                        </div>
                      </div>

                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->







@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
            $('.select2').select2();
        });
    @endsection
    @section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.6/tinymce.min.js"></script>
    <script>
        var editor_config = {
            // path_absolute : "/",
            path_absolute:"{{ url('/') }}/",
  
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
  
                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
  
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            } ,
  
            //  Add Bootstrap Image Responsive class for inserted images
            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Bootstrap responsive image', value: 'img-responsive'},
            ]
  
        };
  
        tinymce.init(editor_config);
    </script>
    @endsection
  
  