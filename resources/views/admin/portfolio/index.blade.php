@extends('admin.includes.app')
@section('content')
<!-- Basic example section start -->
   <section id="basic-examples">
     <div class="row">
       <div class="col-12 mt-3 mb-1">
         <h4 class="text-uppercase">View Sliders</h4>
       </div>
     </div>
     <div class="row match-height">
       @foreach($portfolios as $portfolio)
       <div class="col-xl-3 col-md-6 col-sm-12">
         <div class="card">
           <div class="card-content">
             <div class="card-body">
               <h4 class="card-title">{{$portfolio->name}}</h4>
             </div>
             <img class="img-fluid" src="{{ asset('public/front/images/portfolio/large/'.$portfolio->image)}}" alt="{{$portfolio->title}}">
             <div class="card-body">
               <p class="card-text">{{$portfolio->link}}</p>
               <p class="card-text">{!! substr($portfolio->description , 0 ,100) !!} {{ strlen($portfolio->description )>100? ".." : "" }}</p>
               <a href="{{route('portfolio.edit', ['id' => $portfolio->id])}}">
                 <button class="btn btn-success"><i class="la la-edit"></i></button>
               </a>
               <a href="{{route('portfolio.delete', ['id' => $portfolio->id])}}" onclick="return confirm('Are you sure?')">
                 <button class="btn btn-warning" className="btn-warning"><i class="la la-trash-o"></i></button>
               </a>
             </div>
           </div>
         </div>
       </div>
       @endforeach





     </div>
   </section>
   <!-- // Basic example section end -->
@endsection
