@extends('admin.includes.app')
@section('content')

    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic">Edit Project</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" action="{{route('school.update',$school->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> Project Info</h4>


                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="name" class="form-control" placeholder="Name"
                                                   name="name" value="{{$school->name}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="website">Website Link</label>
                                        <div class="col-md-9">
                                            <input type="url" id="website" class="form-control" placeholder="Website URL"
                                                   name="website" value="{{$school->website}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="description">Description</label>
                                        <div class="col-md-9">
                                          <textarea id="description" rows="5" class="form-control my-editor" name="description" placeholder="Description">{!! $school->description !!}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                     
                                    
                    <div class="form-group row">
                            <label class="col-md-3 label-control">Change logo</label>
                            <div class="col-md-9">
                              <label id="" class="file center-block">
                                <input type="file" id="file" onchange="return fileValidation()" name="logo">
                                <span class="file-custom"></span>
                              </label>
                             <input type="hidden" name="current_logo" value="{{$school->logo}}">
                             @if(!empty($school->logo))
                             <img src="{{ asset('public/uploads/schools/logo/small/'.$school->logo ) }}" alt="Img" style="width:100px;height:80px;">
                             @endif
                            </div>
                          </div>   

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="icon-picker">Category</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="category">
                                                <option value="{{ $school->category }}">{{ $school->category }}</option>
                                                <option value="Government Schools">Government Schools</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-actions text-center">
                                    <button type="submit" class="btn btn-primary" name="submit">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!-- // Basic form layout section end -->

@endsection
