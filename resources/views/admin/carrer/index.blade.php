@extends('admin.includes.app')
@section('content')

    <!-- Content types section start -->
    <section id="content-types">
        <div class="row">
            <div class="col-12 mt-3 mb-1">
                <h4 class="text-uppercase">Carrer Page Details</h4>

            </div>
        </div>
        <div class="row match-height">
            @foreach($carrer as $d)
                <div class="col-xl-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Subtitle :  &nbsp; &nbsp; {{$d->subtitle}}</h4>

                            </div>
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img src="{{asset($d->image)}}" class="d-block w-100" alt="First slide">
                                    </div>

                                </div>

                            </div>

                            <div class="card-body">
                                <h4 class="card-title">Description</h4>
                                <h6 class="card-subtitle text-muted">{{$d->description}}</h6>

                            </div>

                            <div class="text-center">
                                <a href="{{route('carrer.edit', ['id' => $d->id])}}">
                                    <button class="btn btn-success"><i class="la la-edit"></i> Edit Info</button>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </section>
    <!-- Content types section end -->

@endsection
