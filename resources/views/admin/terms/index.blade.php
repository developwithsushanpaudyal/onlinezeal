@extends('admin.includes.app')
@section('content')

    <!-- Content types section start -->
    <section id="content-types">
        <div class="row">
            <div class="col-12 mt-3 mb-1">
                <h4 class="text-uppercase">Terms and Condition Page Details</h4>

            </div>
        </div>
        <div class="row match-height">
            @foreach($terms as $d)
                <div class="col-xl-12 col-md-12 col-sm-12" >
                    <div class="card" style="padding: 20px;">
                       {!! $d->terms !!}
                    </div>


                    <div class="text-center">
                        <a href="{{route('terms.edit', ['id' => $d->id])}}">
                            <button class="btn btn-success"><i class="la la-edit"></i> Edit Info</button>
                        </a>
                    </div>
                </div>


            @endforeach
        </div>

    </section>
    <!-- Content types section end -->

@endsection
