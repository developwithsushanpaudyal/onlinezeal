@extends('admin.includes.app')
@section('content')

<!-- Content types section start -->
        <section id="content-types">
          <div class="row">
            <div class="col-12 mt-3 mb-1">
              <h4 class="text-uppercase">About Us Page Details</h4>

            </div>
          </div>
          <div class="row match-height">
@foreach($about as $d)
            <div class="col-xl-12 col-md-12 col-sm-12">
              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h4 class="card-title">Slogan :  &nbsp; &nbsp; {{$d->slogan}}</h4>

                    <h4 class="card-title">Our Mission</h4>
                    <h6 class="card-subtitle text-muted">{{$d->our_mission}}</h6>
<br>
                    <h4 class="card-title">Our Vision</h4>
                    <h6 class="card-subtitle text-muted">{{$d->our_vision}}</h6>
<br>
                    <h4 class="card-title">Objectives</h4>
                    <h6 class="card-subtitle text-muted">{{$d->objectives}}</h6>
                  </div>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">
                      <div class="carousel-item active">
                        <img src="{{asset($d->image)}}" class="d-block w-100" alt="First slide">
                      </div>
                      <div class="carousel-item">
                        <img src="{{asset($d->cover_image)}}" class="d-block w-100" alt="Second slide">
                      </div>

                    </div>
                    <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                      <span class="la la-angle-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                      <span class="la la-angle-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title">Why Choose Us</h4>
                    <h6 class="card-subtitle text-muted">{{$d->chooseus}}</h6>
<br>
                    <div class="text-center">
                      <a href="{{route('about.edit', ['id' => $d->id])}}">
                        <button class="btn btn-success"><i class="la la-edit"></i> Edit Info</button>
                      </a>
                    </div>
                  </div>


                </div>
              </div>
            </div>
            @endforeach
          </div>

        </section>
        <!-- Content types section end -->

@endsection
