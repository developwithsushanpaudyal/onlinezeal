@extends('admin.includes.app')
@section('content')

<!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Insert About Us</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <form class="form form-horizontal" action="{{route('about.update', ['id' => $about->id ])}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> About Us Info</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="slogan">Slogan</label>
                        <div class="col-md-9">
                          <input type="text" id="slogan" class="form-control" placeholder="Slogan"
                          name="slogan" value="{{$about->slogan}}">
                        </div>
                      </div>

                      <div class="form-group row">
                             <label class="col-md-3 label-control">Cover Image</label>
                             <div class="col-md-9">
                               <label id="projectinput8" class="file center-block">
                                 <input type="file" id="file" name="cover_image">
                                 <span class="file-custom"></span>
                               </label> <br>
                               <img src="{{asset($about->cover_image)}}" class="img-responsive" style="width: 200px;">

                             </div>
                           </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="our_mission">Our Mission</label>
                      <div class="col-md-9">
                        <textarea id="our_mission" rows="5" class="form-control" name="our_mission" placeholder="Our Objectives">
                                       {{$about->our_mission}}
                        </textarea>
                      </div>
                    </div>

                    <div class="form-group row">
                    <label class="col-md-3 label-control" for="our_vision">Our Vision</label>
                    <div class="col-md-9">
                      <textarea id="our_vision" rows="5" class="form-control" name="our_vision" placeholder="Our Vision">
                      {{$about->our_vision}}</textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                  <label class="col-md-3 label-control" for="objectives">Objectives</label>
                  <div class="col-md-9">
                    <textarea id="objectives" rows="5" class="form-control" name="objectives" placeholder="Our Objectives">
                    {{$about->objectives}}</textarea>
                  </div>
                </div>

                <div class="form-group row">
                      <label class="col-md-3 label-control" for="description">Why Choose us</label>
                      <div class="col-md-9">
                        <textarea id="chooseus" rows="5" class="form-control" name="chooseus" placeholder="Why Choose Us">
                        {{$about->chooseus}}</textarea>
                      </div>
                    </div>

              <div class="form-group row">
                     <label class="col-md-3 label-control">Featured Image</label>
                     <div class="col-md-9">
                       <label id="projectinput8" class="file center-block">
                         <input type="file" id="file" name="image">
                         <span class="file-custom"></span>
                       </label> <br>
                       <img src="{{asset($about->image)}}" class="img-responsive" style="width: 200px;">

                     </div>
                   </div>




                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </section>
        <!-- // Basic form layout section end -->

@endsection
