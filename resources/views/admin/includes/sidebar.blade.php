<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
      @if(Auth::check())
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{route('home')}}"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-sliders"></i><span class="menu-title" data-i18n="nav.templates.main">Sliders</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('slider.index')}}" data-i18n="nav.templates.vert.main">View Slider</a>
                    </li>
                    <li><a class="menu-item" href="{{route('slider.create')}}" data-i18n="nav.templates.vert.main">Create Slider</a>
                    </li>


                </ul>
            </li>


            <li class=" nav-item"><a href="#"><i class="la la-television"></i><span class="menu-title" data-i18n="nav.templates.main">Services</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('category.index')}}" data-i18n="nav.templates.vert.main">View Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('category.create')}}" data-i18n="nav.templates.vert.main">Create Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('service.index')}}" data-i18n="nav.templates.vert.main">View Services</a>
                    </li>
                    <li><a class="menu-item" href="{{route('service.create')}}" data-i18n="nav.templates.vert.main">Create Services</a>
                    </li>


                </ul>
            </li>
            {{--<li class=" nav-item"><a href="#"><i class="la la-tasks"></i><span class="menu-title" data-i18n="nav.templates.main">Process</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li><a class="menu-item" href="{{route('process.index')}}" data-i18n="nav.templates.vert.main">View Process</a>--}}
                    {{--</li>--}}
                    {{--<li><a class="menu-item" href="{{route('process.create')}}" data-i18n="nav.templates.vert.main">Create Process</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class=" nav-item"><a href="#"><i class="icon-layers"></i><span class="menu-title" data-i18n="nav.templates.main">Title Page</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('titlepage.index')}}" data-i18n="nav.templates.vert.main">View Titlepage</a>
                    </li>
                    <li><a class="menu-item" href="{{route('titlepage.create')}}" data-i18n="nav.templates.vert.main">Create Titlepage</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="icon-layers"></i><span class="menu-title" data-i18n="nav.templates.main">Packages</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('package.index')}}" data-i18n="nav.templates.vert.main">View Packages</a>
                    </li>
                    <li><a class="menu-item" href="{{route('package.create')}}" data-i18n="nav.templates.vert.main">Create Packages</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-tasks"></i><span class="menu-title" data-i18n="nav.templates.main">Hostings</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('hosting.index')}}" data-i18n="nav.templates.vert.main">View Hostings</a>
                    </li>
                    <li><a class="menu-item" href="{{route('hosting.create')}}" data-i18n="nav.templates.vert.main">Create Hostings</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="icon-briefcase"></i><span class="menu-title" data-i18n="nav.templates.main">Demo</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('demo.index')}}" data-i18n="nav.templates.vert.main">View Demo</a>
                    </li>
                    <li><a class="menu-item" href="{{route('demo.create')}}" data-i18n="nav.templates.vert.main">Create Demo</a>
                    </li>
                </ul>
            </li>



            <li class=" nav-item"><a href="#"><i class="icon-picture"></i><span class="menu-title" data-i18n="nav.templates.main">Portfolio</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('portfoliocategory.index')}}" data-i18n="nav.templates.vert.main">View Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('portfolicategory.create')}}" data-i18n="nav.templates.vert.main">Create Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('portfolio.create')}}" data-i18n="nav.templates.vert.main">Create Portfolio</a>
                    </li>
                    <li><a class="menu-item" href="{{route('portfolio.index')}}" data-i18n="nav.templates.vert.main">View Portfolio</a>
                    </li>
                </ul>
            </li>
            
            <li class=" nav-item"><a href="#"><i class="la la-group"></i><span class="menu-title" data-i18n="nav.templates.main">Team</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('team.view')}}" data-i18n="nav.templates.vert.main">Team Member</a>
                    </li>
                    <li><a class="menu-item" href="{{route('team.create')}}" data-i18n="nav.templates.vert.main">Team Create </a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-list"></i><span class="menu-title" data-i18n="nav.templates.main">Designation</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('designation.view')}}" data-i18n="nav.templates.vert.main">View Designation</a>
                    </li>
                    <li><a class="menu-item" href="{{route('designation.create')}}" data-i18n="nav.templates.vert.main">Create Designation</a>
                    </li>
                </ul>
            </li>


            <li class=" nav-item"><a href="#"><i class="la la-book"></i><span class="menu-title" data-i18n="nav.templates.main">Blogs</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('blogcategory.index')}}" data-i18n="nav.templates.vert.main">View Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('blogcategory.create')}}" data-i18n="nav.templates.vert.main">Create Category</a>
                    </li>
                    <li><a class="menu-item" href="{{route('blog.index')}}" data-i18n="nav.templates.vert.main">View Blogs</a>
                    </li>
                    <li><a class="menu-item" href="{{route('blog.create')}}" data-i18n="nav.templates.vert.main">Create Blogs</a>
                    </li>
                    <li><a class="menu-item" href="{{route('blogtag.index')}}" data-i18n="nav.templates.vert.main">View Tags</a>
                    </li>
                    <li><a class="menu-item" href="{{route('blogtag.create')}}" data-i18n="nav.templates.vert.main">Create Tags</a>
                    </li>


                </ul>
            </li>
        

            <li class=" nav-item"><a href="#"><i class="icon-briefcase"></i><span class="menu-title" data-i18n="nav.templates.main">Clients</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('clients.index')}}" data-i18n="nav.templates.vert.main">View Clients</a>
                    </li>
                    <li><a class="menu-item" href="{{route('clients.create')}}" data-i18n="nav.templates.vert.main">Create Clients</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">Vacancy</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('vacancy.create')}}" data-i18n="nav.templates.vert.main">Insert Vacancy</a>
                    </li>
                    <li><a class="menu-item" href="{{route('vacancy.index')}}" data-i18n="nav.templates.vert.main">View Vacancy</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">School</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('school.create')}}" data-i18n="nav.templates.vert.main">Insert Projects</a>
                    </li>
                    <li><a class="menu-item" href="{{route('school.index')}}" data-i18n="nav.templates.vert.main">View Projects</a>
                    </li>
                </ul>
            </li>


            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">Settings</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('success.index')}}" data-i18n="nav.templates.vert.main">Success</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">Messages</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('email.show')}}" data-i18n="nav.templates.vert.main">Contact Messages</a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">Get a quote</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('getaquote.view')}}" data-i18n="nav.templates.vert.main">User details</a>
                    </li>
                </ul>
            </li>



            <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.templates.main">Page Settings</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('about.index')}}" data-i18n="nav.templates.vert.main">About Us</a>
                    </li>
                    <li><a class="menu-item" href="{{route('contact.index')}}" data-i18n="nav.templates.vert.main">Contact</a>
                    </li>
                    <li><a class="menu-item" href="{{route('carrer')}}" data-i18n="nav.templates.vert.main">Carrer</a>
                    </li>
                    <li><a class="menu-item" href="{{route('termsandconditions')}}" data-i18n="nav.templates.vert.main">Terms & Conditions</a>
                    </li>
                    <li><a class="menu-item" href="{{route('privacypolicy')}}" data-i18n="nav.templates.vert.main">Privacy Policy</a>
                    </li>
                </ul>
            </li>



        </ul>
        @endif
    </div>
</div>
