<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  @include('admin.includes.header')
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
      data-open="click" data-menu="vertical-menu" data-col="2-columns">
<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a class="navbar-brand" href="#">
                        <img class="brand-logo"  src="{{asset('public/admin/app-assets/images/logo/logo.png')}}">
                        <h3 class="brand-text">Admin</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>

                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">Hello,
                    <?php
                    $current_user = Auth::user();
                    ?>
                    <span class="user-name text-bold-700">{{$current_user->name}}</span>
                </span>
                            <span class="">
                  <img src="{{asset($current_user->profile->avatar)}}" alt="avatar"><i></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{route('profile')}}"><i class="ft-user"></i> Edit Profile</a>
                            <div class="dropdown-divider"></div>
                            <a href="{{route('logout')}}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                                {{ __('Logout') }}
                                <i class="la la-power-off"></i> </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- ////////////////////////////////////////////////////////////////////////////-->



@include('admin.includes.sidebar')



<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Active Orders -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Services</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <td>
                                    <a href="{{route('service.index')}}">
                                        <button class="btn btn-sm round btn-primary btn-glow">View All</button>
                                    </a>
                                </td>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-de mb-0">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Subtitle</th>
                                        <th>Image</th>
                                        <th>Content</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($servicesdashboard as $service)
                                        <tr>
                                            <td  class="primary">{{$service->title}}</td>
                                            <td>{{$service->subtitle}}</td>
                                            <td><img src="{{asset($service->image)}}" alt="" class="img-responsive" height="100" width="100"></td>
                                            <td>{!! substr($service->content , 0 ,200) !!} {!! strlen($service->content )>200? "..." : "" !!}</td>
                                            <td>
                                                <a href="{{route('service.delete', ['id' => $service->id])}}" onclick="return confirm('Are you sure?')">
                                                <button class="btn btn-sm round btn-outline-danger"> Delete</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Active Orders -->


            <a href="{{route('success.index')}}">
                <div class="row">
                    <div class="col-12">
                        <div class="card bg-gradient-x-success">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-12 border-right-info border-right-lighten-3">
                                        <div class="card-body text-center">
                                            <h1 class="display-4 text-white"><i class="icon-camera font-large-2"></i> {{$success->projects}}</h1>
                                            <span class="text-white">Projects</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 border-right-info border-right-lighten-3">
                                        <div class="card-body text-center">
                                            <h1 class="display-4 text-white"><i class="icon-user font-large-2"></i> {{$success->clients}}</h1>
                                            <span class="text-white">Clients</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-sm-12">
                                        <div class="card-body text-center">
                                            <h1 class="display-4 text-white"><i class="icon-wallet font-large-2"></i> {{$success->hours}}</h1>
                                            <span class="text-white">Work Hours</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-sm-12">
                                        <div class="card-body text-center">
                                            <img src="{{asset($success->coverimage)}}" alt="" width="200" height="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@include('admin.includes.footer')
