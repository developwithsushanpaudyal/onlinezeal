<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="#"
                                                                                     target="_blank">OnlineZeal </a>, All rights reserved. </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
</footer>
<!-- BEGIN VENDOR JS-->
<script src="{{asset('public/admin/assets/js/toastr.min.js')}}" type="text/javascript"></script>

<script src="{{asset('public/admin/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{asset('public/admin/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>

<script src="{{asset('public/admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>

<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="{{asset('public/admin/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{asset('public/admin/app-assets/js/scripts/pages/dashboard-crypto.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="{{asset('public/admin/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

<!-- Toastr JS -->

<script src="{{asset('public/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}" type="text/javascript"></script>



<script src="{{asset('public/admin/app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>

@yield('scripts')

<script>
    @if(Session::has('success'))
    toastr.success('{{Session::get('success') }}')
    @endif

    @if(Session::has('delete'))
        toastr.warning('{{Session::get('delete')}}')
        @endif

        @if(Session::has('info'))
        toastr.info('{{Session::get('info')}}')
    @endif
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.6/tinymce.min.js"></script>
<script>
    var editor_config = {
        // path_absolute : "/",
        path_absolute:"{{ url('/') }}/",

        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        } ,

        //  Add Bootstrap Image Responsive class for inserted images
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Bootstrap responsive image', value: 'img-responsive'},
        ]

    };

    tinymce.init(editor_config);
</script>
<script>
        (function($) {
            $.fn.autosubmit = function() {
              this.submit(function(event) {
                event.preventDefault();
                var form = $(this);
                $.ajax({
                  type: form.attr('method'),
                  url: form.attr('action'),
                  data: form.serialize()
                }).done(function(data) {
                  // Optionally alert the user of success here...
                }).fail(function(data) {
                  // Optionally alert the user of an error here...
                });
              });
              return this;
            }
          })(jQuery)
</script>

</body>
</html>
