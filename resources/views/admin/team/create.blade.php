@extends('admin.includes.app')
@section('content')

        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Create a New Team Member</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                  <form class="form form-horizontal" action="{{route('team.save')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i>Team Member</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="name">Name</label>
                        <div class="col-md-9">
                          <input type="text" id="name" class="form-control" placeholder="Enter your name"
                          name="name">
                        </div>
                      </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="designation-picker">Designation</label>
                      <div class="col-md-9">
                        <select class="form-control" name="designation">
                          @foreach($designation as $d)
                        <option value={{$d->name}}>{{$d->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="sort_id">Sort Id</label>
                      <div class="col-md-9">
                        <input type="text" id="sort_id" class="form-control" placeholder="Enter your sort id"
                        name="sort_id">
                      </div>
                    </div>

                    <div class="form-group row">
                           <label class="col-md-3 label-control">Upload Image</label>
                           <div class="col-md-9">
                             <label id="" class="file center-block">
                               <input type="file" id="file" name="image">
                               <span class="file-custom"></span>
                             </label>
                           </div>
                         </div>

                    <div class="form-group row">
                          <label class="col-md-3 label-control" for="description">Description</label>
                          <div class="col-md-9">
                            <textarea id="content" rows="5" class="form-control my-editor" name="description" placeholder="Description"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="facebook">Facebook</label>
                        <div class="col-md-9">
                          <input type="text" id="facebook" class="form-control" placeholder="Enter your facebook link"
                          name="facebook">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="twitter">Twitter</label>
                        <div class="col-md-9">
                          <input type="text" id="twitter" class="form-control" placeholder="Enter your twitter link"
                          name="twitter">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="instagram">Instagram</label>
                        <div class="col-md-9">
                          <input type="text" id="instagram" class="form-control" placeholder="Enter your instagram link"
                          name="instagram">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="pinterest">Pinterest</label>
                        <div class="col-md-9">
                          <input type="text" id="pinterest" class="form-control" placeholder="Enter your pinterest"
                          name="pinterest">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="likedin">likedin</label>
                        <div class="col-md-9">
                          <input type="text" id="likedin" class="form-control" placeholder="Enter your LikedIn"
                          name="likedin">
                        </div>
                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
@endsection

