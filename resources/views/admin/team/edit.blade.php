@extends('admin.includes.app')
@section('content')
  <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="horz-layout-basic">Team Member Update</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                  <form class="form form-horizontal" action="{{route('team.update',$team->id)}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i>Edit Team Member</h4>


                        <div class="form-group row">
                        <label class="col-md-3 label-control" for="name">Name</label>
                        <div class="col-md-9">
                          <input type="text" id="name" class="form-control" placeholder="Enter your name"
                          name="name" value="{{ $team->name }}">
                        </div>
                      </div>

                      <div class="form-group row">
                      <label class="col-md-3 label-control" for="designation-picker">Designation</label>
                      <div class="col-md-9">
                        <select class="form-control" name="designation">
                        <option value="{{$team->designation}}"
                            @if($team->designation==$team->designation)selected
                            @endif>{{$team->designation}}
                        </option>
                        @foreach($designation as $d)
                            <option value="{{$d->name}}">{{$d->name}}</option>
                          @endforeach
                         </select>
                      </div>
                    </div>

                    <div class="form-group row">
                           <label class="col-md-3 label-control">Change Image</label>
                           <div class="col-md-9">
                             <label id="" class="file center-block">
                               <input type="file" id="file" onchange="return fileValidation()" name="image">
                               <span class="file-custom"></span>
                             </label>
                            <input type="hidden" name="current_image" value="{{$team->image}}">
                            @if(!empty($team->image))
                            <img src="{{ asset('public/uploads/team/small/'.$team->image ) }}" alt="Img" style="width:100px;height:80px;">
                            @endif
                           </div>
                         </div>                           
                         

                    <div class="form-group row">
                          <label class="col-md-3 label-control" for="description">Description</label>
                          <div class="col-md-9">
                            <textarea id="content" rows="5" class="form-control my-editor" name="description" placeholder="Description">{!! $team->description !!}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="facebook">Facebook</label>
                        <div class="col-md-9">
                          <input type="text" id="facebook" class="form-control" placeholder="Enter your facebook link"
                          name="facebook" value="{{ $team->facebook }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="twitter">Twitter</label>
                        <div class="col-md-9">
                          <input type="text" id="twitter" class="form-control" placeholder="Enter your twitter link"
                          name="twitter" value={{ $team->twitter }}>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="instagram">Instagram</label>
                        <div class="col-md-9">
                          <input type="text" id="instagram" class="form-control" placeholder="Enter your instagram link"
                          name="instagram" value={{ $team->instagram }}>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="pinterest">Pinterest</label>
                        <div class="col-md-9">
                          <input type="text" id="pinterest" class="form-control" placeholder="Enter your pinterest"
                          name="pinterest" value={{ $team->pinterest }}>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 label-control" for="likedin">likedin</label>
                        <div class="col-md-9">
                          <input type="text" id="likedin" class="form-control" placeholder="Enter your LikedIn"
                          name="likedin" value={{ $team->likedin }}>
                        </div>
                      </div>
                      <div class="form-actions text-center">
                        <button type="submit" class="btn btn-primary" name="submit">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>        
@endsection
