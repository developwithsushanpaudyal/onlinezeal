@include('frontend.includes.header')


<section class="banner  o-hidden banner-inner pricing-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Pricing Plans </h1>
            <p class="semi-bold">Based on market demand, we have created 3 packages that will cover all
                <br> your business needs .</p>
            <a href="#pricing-plans" class="medium-btn2 btn btn-nofill page-scroll">SEE OUR PRICING</a>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<section id="pricing-plans" class="pricing-plans">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Our Website Development packages</h2>
        <hr class="dark-line" />
        <p class="regular-text">
            We offer affordable website developmet Packages along with the Web hosting facilities.You can choose suitable web Development packages.     </p>
        <!--end section title -->
        <div class="row">
            @foreach($packages as $package)
            <div class="col-sm-6 col-md-4">
                <ul class="pricing">
                    <li class="price">
                        <i class="flaticon-vegetable-shop"></i> {{$package->name}}
                        <span>
                                <sup>Nrs.</sup>{{$package->price}} </span>
                    </li>
                    <?php $details = json_decode($package->description); ?>
                    @for($i=0;$i< sizeof($details[0]);$i++)

                    <li> {{$details[0][$i]}} </li>
                    @endfor




                </ul>


                <!-- End of .pricing -->
                <!-- <a href="" class="btn btn-fill">Get offer</a> -->
            </div>
            <!-- End of .col-sm-6 -->
                @endforeach
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .pricing-plans -->



<!--get a quote modal-->
<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                        <span class="icon-chevron-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time & cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end get a quote modal-->
<!--portfolio details  modal-->
<div class="modal fade verticl-center-modal" id="portfolioDetModal" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal">
    <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <!-- main slider carousel -->
                        <div id="slider">
                            <div id="carousel-bounding-box">
                                <div id="myCarousel" class="carousel slide">
                                    <!-- main slider carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item" data-slide-number="0" style="background: url('images/portfolio/portfolio-details-slider-one.jpg')">
                                        </div>
                                        <div class="item" data-slide-number="1" style="background: url('images/portfolio/portfolio-details-slider-two.jpg')"> </div>
                                        <div class="item" data-slide-number="2" style="background: url('images/portfolio/portfolio-details-slider-three.jpg')"> </div>
                                        <div id="slider-thumbs">
                                            <!-- thumb navigation carousel items -->
                                            <ul class="list-inline  thumb-list">
                                                <li>
                                                    <a id="carousel-selector-0" class="carousel-selector selected">
                                                        <img src="images/portfolio/portfolio-slider-thum-1.jpg" class="img-responsive" alt=""> </a>
                                                </li>
                                                <li>
                                                    <a id="carousel-selector-1" class="carousel-selector">
                                                        <img src="images/portfolio/portfolio-slider-thum-2.jpg" class="img-responsive" alt=""> </a>
                                                </li>
                                                <li>
                                                    <a id="carousel-selector-2" class="carousel-selector">
                                                        <img src="images/portfolio/portfolio-slider-thum-3.jpg" class="img-responsive" alt=""> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="port-modal-content">
                            <p class="gray-text">Featured - Design</p>
                            <h2 class="b-clor">Featured Projects</h2>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consect etuer adipi scing elit, sed diam nonum mLorem ipsum dolor
                                sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
                                tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            <p class="regular-text">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                                vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim
                                qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                        </div>
                        <h3>We delivered:</h3>
                        <ul class="list-with-arrow">
                            <li>A strategy to grow business online.</li>
                            <li>A unique website with great user experience design.</li>
                            <li>A custom content management system to maintain </li>
                            <li>the website flowlessly.</li>
                        </ul>
                        <a href="#" class="medium-btn2  btn btn-fill">Launch website</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end portfolio details modal-->

@include('frontend.includes.footer')
