@include('frontend.includes.header')

<section class="banner  o-hidden banner-inner portfolio-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Demo</h1>
                <p class="semi-bold">We use strategic approaches to provide our clients with high-end.
                    <br /> services that ensure superior customer satisfaction.</p>
                <a href="#" class="medium-btn2 btn btn-nofill page-scroll">DISCOVER MORE</a>
            </div>
            <!--end banner text-->
        </div>
    </section>

<section class="portfolio-full portfolio clearfix" id="more-portfolio">
        <div class="container">
        <h2 class="b-clor">Our Product</h2>
         <hr class="dark-line" />
            <div class="grid clearfix row">
                <!-- End of .grid-item -->
                <div class="a3 grid-item">
                    <div class="img_container">
                        <img src="{{ asset('public/uploads/dashain.jpg') }}" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal2">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal2">Jet Airplane
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                
                
                
                <!-- End of .grid-item -->
                                <!-- End of .grid-item -->
                <!-- to add more items to the portfolio section copy and paste any
                of the items underneath the last item -->
            </div>
            <!-- End of .grid -->
            <div class="text-center port-dms">
                <a href="portfolio.html" class="dmb-p btn btn-fill full-width">Discover more</a>
            </div>
        </div>
    </section>

@include('frontend.includes.footer')