@include('frontend.includes.header')

<section class="blog-title" style="    padding: 220px 0 90px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Terms &amp; Conditions</h1>
            </div>
        </div>
    </div>
</section>
<!-- ++++ Most Bold Title ++++ -->
<!-- ++++ blog content ++++ -->
<section class="bg-white privecy-content ">
    <!--blog content row one-->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {!! $terms->terms !!}
            </div>
        </div>
    </div>
    <!--end blog content row one-->
</section>

@include('frontend.includes.footer')
