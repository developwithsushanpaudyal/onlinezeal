
@include('frontend.includes.header')


<!-- ++++ Most Bold Title ++++ -->
<section class="banner  o-hidden banner-inner blog-banner" >
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Blog</h1>
            <p class="semi-bold">We think of us as journalists; the medium we work in is blogging.</p>
            <a href="#blog_item" class="medium-btn3 btn btn-nofill page-scroll">Discover more</a>
        </div>
        <!--end banner text-->
    </div>
</section> 
    <!-- ++++ Most Bold Title ++++ -->
    <!-- ++++ blog content ++++ -->
    <section id="blog_item" class="bg-white o-hidden blog-p2 blog-content">
       @foreach($blogcategory as $blogcat_id)
        <!--blog content row one-->
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">{{ $blogcat_id->name }}</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                @foreach($blogs as $blog_data)
                @if($blog_data->blogscategory_id == $blogcat_id->id )
                <!--blog content box-->
                <div class="col-xs-12 col-sm-4">
                    <div class="box-content-with-img equalheight is-featured">
                        <img src="{{ asset('public/front/images/blog/medium/'.$blog_data->image) }}" class="img-responsive" alt="blog image" />
                        <div class="box-content-text">
                            <p class="gray-text">
                                <span class="icon-calendar-full"></span>{{$blog_data->created_at->diffForHumans()}}</p>
                            <h3 class="semi-bold">
                                <a href="{{route('blogdetail',$blog_data->slug)}}">{{$blog_data->title}}</a>
                            </h3>
                            <p class="regular-text">{!! substr($blog_data->body,0,200) !!} </p>
                            <p> {{strlen($blog_data->body)>200 ? "....":"..."}} </p>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
               
                
                <!--end blog content box-->
            </div>
        </div>
        
        <!--end blog content row one-->
        @endforeach
    </section>


@include('frontend.includes.footer')
