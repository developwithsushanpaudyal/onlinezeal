<!--counter section-->
<div class="clearfix"></div>
<div class="banner  o-hidden success-number">
    <div class="clearfix"></div>
    <div class="parallax-container">
        <div class="clearfix"></div>
        <section>
            <div class="clearfix"></div>
            @foreach($success as $d)
            <div class="stuff" data-type="content" style="background: url({{asset($d->coverimage)}}) center top no-repeat; background-attachment: fixed; background-size: cover;">
                <div class="container">
                    <!--section title -->
                    <h2>Success In Numbers</h2>
                    <!--end section title -->
                    <div class="row counter-inner">
                        <!-- features box -->
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="icon-holder pull-left">
                                <span class="icon-users2"></span>
                            </div>
                            <div class="pull-left counter-text">
                                <div class="counter no_count b-clor">{{$d->clients}}</div>
                                <p class="semi-bold">Happy Clients</p>
                            </div>
                        </div>
                        <!--end features box -->
                        <!-- features box -->
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="icon-holder pull-left">
                                <span class="icon-calendar-check"></span>
                            </div>
                            <div class="pull-left counter-text">
                                <div class="counter no_count b-clor">{{$d->projects}}</div>
                                <p class="semi-bold">projects completed</p>
                            </div>
                        </div>
                        <!--end features box -->
                        <!-- features box -->
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="icon-holder pull-left">
                                <span class="icon-clock"></span>
                            </div>
                            <div class="pull-left counter-text">
                                <div class="counter no_count b-clor">{{$d->hours}}</div>
                                <p class="semi-bold">Projects Hours Spent</p>
                            </div>
                        </div>
                        <!--end features box -->
                    </div>
                </div>
            </div>
            @endforeach

            <div class="clearfix"></div>
        </section>
        <div class="clearfix"></div>
    </div>
</div>
<!--end counter section-->
