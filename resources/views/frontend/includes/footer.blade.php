

<!-- ++++ footer ++++ -->
<footer id="footer">
    <!--scroll to top-->
    <a class="top-btn page-scroll" href="#top">
        <span class="icon-chevron-up b-clor regular-text text-center"></span>
    </a>
    <!--end scroll to top-->
    <!--newsletter section-->

    <!--end newsletter section-->
    <!--footer content-->
    <div class="light-ash-bg">
        <div class="container">
            <ul>
                <li>
                    <!-- footer left content-->
                    <ul>
                        <li>
                            <a href="index.php">
                                <img src="{{asset('public/front/images/logo-footer.png')}}" alt="logo" class="img-responsive logo">
                            </a>
                        </li>
                        <li>
                            <p class="extra-small-text">&copy; <?php echo date('Y'); ?></p>
                        </li>
                        <li>
                            <p class="extra-small-text">OnlineZeal PVT. LTD</p>
                        </li>
                        <li>
                            <p class="extra-small-text">Tinkune,Kathmandu</p>
                        </li>
                    </ul>
                    <!--end footer left content-->
                </li>
                <li>
                    <!--footer service list-->
                    <ul>
                        <li>
                            <a class="regular-text text-color-light">Services  </a>
                        </li>
                        @foreach($servicesfooter as $service)
                        <li>
                            <a href="{{route('services.single', $service->slug)}}" class="extra-small-text">{{$service->title}}</a>
                        </li>
                            @endforeach
                    </ul>
                    <!--end footer service list-->
                </li>
                <li>
                    <!--footer Resources list-->
                    <ul>
                        <li>
                            <a class="regular-text text-color-light">Resources</a>
                        </li>
                        <li>
                            <a href="{{route('portfolio.single')}}" class="extra-small-text">Portfolio</a>
                        </li>
                        <li>
                            <a href="{{route('about')}}" class="extra-small-text">About Us</a>
                        </li>
                        <!-- <li>
                            <a href="blog.php" class="extra-small-text">Blog</a>
                        </li> -->
                    </ul>
                    <!--end footer Resources list-->
                </li>
                <li>
                    <!--footer Support list-->
                    <ul>
                        <li>
                            <a class="regular-text text-color-light">Support</a>
                        </li>
                        <li>
                            <a href="{{route('contact')}}" class="extra-small-text">Contact</a>
                        </li>
                        <li>
                            <a href="{{route('termsfront')}}" class="extra-small-text">Terms & Conditions</a>
                        </li>
                        <li>
                            <a href="{{route('privacyfront')}}" class="extra-small-text">Privacy Policy</a>
                        </li>
                        <!-- <li>
                            <a href="privacy-policy.php" class="extra-small-text">Privacy Policy</a>
                        </li> -->
                        <!-- <li>
                            <a href="terms-conditions.php" class="extra-small-text">Terms &amp; Conditions</a>
                        </li> -->
                    </ul>
                    <!--end footer Support list-->
                </li>
                <li class="big-width">
                    <!--footer social media list-->
                    <ul class="list-inline">
                        <li>
                            <p class="regular-text text-color-light">Get in Touch</p>
                            <ul class="social-links">
                                <li>
                                    <a href="{{$contacts->facebook}}" target="_blank">
                                        <span class="icon-facebook"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{$contacts->twitter}}" target="_blank">
                                        <span class="icon-twitter"></span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{$contacts->linkedin}}" target="_blank">
                                        <span class="icon-linkedin"></span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="#">
                                        <span class="icon-google-plus"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon-instagram"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon-pinterest"></span>
                                    </a>
                                </li> -->

                            </ul>
                        </li>
                    </ul>
                    <!--end footer social media list-->
                </li>
            </ul>
        </div>
    </div>
    <!--end footer content-->
</footer>
<!--end footer-->


<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

<!-- ++++ Javascript libraries ++++ -->
<!--js library of jQuery-->
<script type="text/javascript" src="{{asset('public/front/js/jquery.min.js')}}"></script>
<!--js library of bootstrap-->
<script type="text/javascript" src="{{asset('public/front/js/bootstrap.min.js')}}"></script>
<!--js library for number counter-->
<script src="{{asset('public//front/js/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/js/jquery.counterup.min.js')}}"></script>
<!--js library for video popup-->
<script type="text/javascript" src="{{asset('public/front/js/jquery.magnific-popup.min.js')}}"></script>
<!-- js library for owl carousel -->
<script type="text/javascript" src="{{asset('public/front/js/owl.carousel.min.js')}}"></script>
<!--modernizer library-->
<script type="text/javascript" src="{{asset('public/front/js/modernizr.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/js/isotope.min.js')}}">
</script>
<!-- Slider Revolution core JavaScript files -->
<script type="text/javascript" src="{{asset('public/front/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- Slider Revolution extension scripts. Remove these scripts before uploading
to server -->
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/front/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<!--custom js-->
<script type="text/javascript" src="{{asset('public//front/js/script.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123520068-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123520068-1');
</script>
<meta name="google-site-verification" content="QaBrpv5rVpruicP0obE6sQdGwfYgfnU9W6J4EJX_5yk" />

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c1c93afef33579e"></script>
<script id="dsq-count-scr" src="//http-localhost-laravel-onlinezeal.disqus.com/count.js" async></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
<script >
 @if(Session::has('success'))

toastr.options.positionClass = 'toast-top-right';
toastr.success('{{ Session::get('success') }}')


@endif
@if(Session::has('error'))

toastr.options.positionClass = 'toast-top-right';
toastr.error('{{ Session::get('error') }}')

@endif
</script>

</body>

</html>
