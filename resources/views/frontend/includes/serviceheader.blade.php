<!DOCTYPE html>
<html lang="zxx">

<head>

@include('frontend.partials.meta_dynamic')


<!-- ++++ favicon ++++ -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/front/icon/favicon-32x32.png')}}">
    <!-- <link rel="icon" type="image/png" sizes="96x96" href="icon/favicon-96x96.png">
    <link rel="apple-touch-icon" sizes="57x57" href="icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-touch-icon-114x114.png"> -->
    <!-- ++++ bootstrap ++++ -->
    <link rel="stylesheet" href="{{asset('public/front/css/bootstrap.min.css')}}" />
    <!-- ++++ owl carousel ++++ -->
    <link rel="stylesheet" href="{{asset('public/front/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('public/front/css/owl.theme.default.min.css')}}" type="text/css">
    <!-- ++++ magnific-popup  ++++ -->
    <link rel="stylesheet" href="{{asset('public/front/css/magnific-popup.css')}}" />
    <!-- ++++ font Icon ++++ -->
    <link rel="stylesheet" href="{{asset('public/front/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/fonts.css')}}" />
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/front/revolution/css/settings.css')}}">
    <!-- ++++ style ++++ -->
    <link rel="stylesheet" href="{{asset('public/front/css/style.css')}}" />
    <!-- responsive css -->
    <link rel="stylesheet" href="{{asset('public/front/css/responsive.css')}}" />

    @yield('styles')
    <!-- [if IE]>
    <script src="{{asset('public/front/js/html5shiv.js')}}"></script>
    <![endif]  -->
</head>

<body>

<div class="header-wrapper">
    <!-- most top information -->
    <header id="top">
        <div class="container">
            <div class="form-element hidden-xs pull-left">
                <form action="{{route('search')}}" method="POST" class="form-inline" role="search">
                    @csrf
                    <input type="text" name="q" class="form-control" placeholder="Search">
                    <button type="submit" class="search-btn">
                        <i class="icon-magnifier"></i>
                    </button>
                </form>
            </div>
            <!-- End of .form-element -->
            <div class="contact-info clearfix">
                <ul class="pull-right list-inline">
                    <li>
                        <a href="tel:012.345.1234">
                            <i class="icon-telephone"></i>01-5199625 , 9801079608</a>
                    </li>
                    <li>
                        <a href="mailto:info@company.com">
                            <i class="icon-envelope"></i>contact@onlinezeal.com</a>
                    </li>
                </ul>
            </div>
            <!-- End of .contact-info -->
        </div>
    </header>
    <!-- end most top information -->
    <!--navigation-->
    <div class="container no-padding">
        <nav id="navbar-main" class="navbar main-menu">
            <!--Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('index')}}">
                    <img src="{{asset('public/front/images/logo.png')}}" alt="Brand" class="img-responsive" />
                </a>
            </div>
            <!--Collect the nav links, and other content for toggling-->
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown not-relative">
                        <a href="services.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <i class="fa fa-chevron-down text-muted"></i>  </a>
                        <div class="dropdown-menu dropdown-megamenu">
                            <ul class="megamenu">
                                <li>

                                    <ul class="dropdown-submenu clearfix">
                                        <li class="submenu-heading">Design</li>
                                        @foreach($services1 as $d)
                                            <li>
                                                <a href="{{route('services.single', $d->slug)}}">
                                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <!-- End of .dropdown-submenu -->

                                </li>

                                <li>

                                    <ul class="dropdown-submenu clearfix">
                                        <li class="submenu-heading">Development</li>
                                        @foreach($services2 as $d)
                                            <li>
                                                <a href="{{route('services.single', $d->slug)}}">
                                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <!-- End of .dropdown-submenu -->

                                </li>

                                <li>

                                    <ul class="dropdown-submenu clearfix">
                                        <li class="submenu-heading">Online Marketing</li>
                                        @foreach($services3 as $d)
                                            <li>
                                                <a href="{{route('services.single', $d->slug)}}">
                                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <!-- End of .dropdown-submenu -->

                                </li>

                                <li>

                                    <ul class="dropdown-submenu clearfix">
                                        <li class="submenu-heading">Technology</li>
                                        @foreach($services4 as $d)
                                            <li>
                                                <a href="{{route('services.single', $d->slug)}}">
                                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <!-- End of .dropdown-submenu -->

                                </li>

                                <li>

                                    <ul class="dropdown-submenu clearfix">
                                        <li class="submenu-heading">Web Hosting</li>
                                        @foreach($services5 as $d)
                                            <li>
                                                <a href="{{route('services.single', $d->slug)}}">
                                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <!-- End of .dropdown-submenu -->

                                </li>

                            </ul>
                        </div>
                        <!-- End of .dropdown-menu -->
                    </li>
                    <li>
                        <a href="{{route('portfolio.single')}}">Portfolio</a>
                    </li>

                    <li class="dropdown">
                        <a href="about.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <i class="fa fa-chevron-down text-muted"></i>  </a>
                        <div class="dropdown-menu">
                            <ul class="megamenu">
                                <li>
                                    <a href="{{route('about')}}">Overview</a>
                                </li>
                                <li>
                                    <a href="{{route('team')}}">Team</a>
                                </li>

                            </ul>
                            <!-- End of .dropdown-menu -->
                        </div>
                    </li>
                    <li>
                        <a href="{{route('price')}}">Pricing</a>
                    </li>

                    <li>
                        <a href="{{route('blog')}}">Blog</a>
                    </li>
                    
                    <li>
                        <a href="{{route('contact')}}">Contact Us</a>
                    </li>
                    <li>
                        <a href="{{route('carrer')}}">Carrer</a>
                    </li>

                    <li class="menu-btn" data-toggle="modal" data-target="#getAQuoteModal">
                        <a class="btn btn-fill" href="#">GET A QUOTE
                            <span class="icon-chevron-right"></span>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>
        <!--end navigation-->
    </div>
</div>
<!-- End of .header-wrapper -->

<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post" action="{{route('getaquote.store')}}" data-autosubmit>
                                @csrf
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="username" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="useremail" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="userphone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="userwebsite" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="usermessage" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <input type="submit" class="btn btn-fill full-width">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time &amp; cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>