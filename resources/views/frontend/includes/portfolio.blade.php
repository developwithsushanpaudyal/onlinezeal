<!--portfolio section-->
<section class="portfolio-full portfolio clearfix" id="portfolio-home">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Full Portfolio</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="button-group filter-button-group clearfix">
            <button class="button is-checked" data-filter="*">All Work</button>
        @foreach($portfoliocategories as $portfoliocategory)
            <button class="button " data-filter=".{{$portfoliocategory->name}}">{{$portfoliocategory->name}}</button>
            @endforeach
            <button class="button" data-filter=".school">School</button>
            <!-- <button class="button" data-filter=".a4">E-commerce</button> -->
        </div>
        <!-- button-group ends -->
        
        <div class="grid clearfix row">
            <!-- Items for portfolio
            currently using 12 items. More Items can be added. -->
           
                
            
            @foreach($portfolios as $port)
            <div class="{{$port->categories}} grid-item col-md-4">
                <div class="img_container">
                    <img src="{{asset('public/front/images/portfolio/small/'.$port->image)}}" alt="port_img" class="img-responsive">
                    <div class="overlay">
                            <a class="btn btn-nofill portfolioDetModal{{ $port->id}}" data-toggle="modal" data-target="#portfolioDetModal{{ $port->id }}" >View</a>
                    </div>
                    <!-- End of .overlay -->
                </div>
                <!-- End of .img_container -->
                <div class="text-content">
                    <h3>
                        <a data-toggle="modal" data-target="#portfolioDetModal{{ $port->id }}" >{{$port->name}}
                        </a>
                    </h3>
                </div>
            </div>

                <div class="modal fade verticl-center-modal" id="portfolioDetModal{{$port->id}}" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal{{$port->id}}">
                    <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="icon-cross-circle"></span>
                            </button>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <!-- main slider carousel -->
                                        <div id="slider">
                                            <div id="carousel-bounding-box">
                                                <div id="myCarousel" class="carousel slide myCarousel">
                                                    <!-- main slider carousel items -->
                                                    <div class="carousel-inner">
                                                        <div class="active item" data-slide-number="0">
                                                            <img src="{{asset('public/front/images/portfolio/medium/'.$port->image)}}" alt="images" class="img-responsive">
                                                        </div>
                                                        <div class="item" data-slide-number="1">
                                                            <img src="{{asset('public/front/images/portfolio/medium/'.$port->image)}}" alt="images" class="img-responsive">
                                                        </div>
                                                        <div class="item" data-slide-number="2">
                                                            <img src="{{asset('public/front/images/portfolio/medium/'.$port->image)}}" alt="images" class="img-responsive">
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="port-modal-content">
                                            <p class="gray-text">{{$port->categories}}</p>
                                            <h2 class="b-clor">{{$port->name}}</h2>
                                            <p class="regular-text">
                                          
                                              {!! $port->description !!}
                                        </div>
                                        
                                        <a href="{{$port->link}}" class="medium-btn2  btn btn-fill">Launch website</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- End of .grid-item -->
            <div class="school col-md-4 grid-item">
                <div class="img_container">
                     <img src="{{asset('public/front/images/portfolio/school.jpg')}}" alt="schools" class="img-responsive">
                        <div class="overlay">
                         <a class="btn btn-nofill" href="{{route('schoolprojects') }}">View</a>
                    </div>
                <!-- End of .overlay -->
                </div>
                <!-- End of .img_container -->
                <div class="text-content">
                   <h3>
                        <a href="{{route('schoolprojects')}}">School and colleges
                            <span>Website</span>
                        </a>
                    </h3>
                 </div>
            </div>
            

        </div>
    

        <!-- End of .grid -->
        <div class="text-center port-dms">
            <a href="{{route('portfolio.single')}}" class="dmb-p btn btn-fill full-width">View more</a>
        </div>
    </div>
</section>
<!--end portfolio section-->
