<!--services Section-->
<section class="bg-white o-hidden services dms-modern" id="services">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Our Services</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            <!-- features box -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="box-green-border">
                  @foreach($categories1 as $d)
                    <a href="#">
                        <span class="{{$d->icon}}"></span>
                        <div>{{$d->name}}</div>
                    </a>
                  @endforeach

                      <p>{!! str_limit($d->description, 100) !!}</p>
                    <div class="service-overlay">
                        <ul class="clearfix">
                          @foreach($services1 as $d)

                        <li>
                                <a href="{{route('services.single', $d->slug)}}">
                                    <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                            </li>
                          @endforeach

                            {{-- <li>
                                <a href="website-design.php">
                                    <i class="icon-laptop-phone"></i> Web Design</a>
                            </li> --}}
                        </ul>
                    </div>
                    <!-- End of .service-overlay -->
                </div>
            </div>
            <!--end features box -->


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="box-green-border">
                    @foreach($categories2 as $d)
                        <a href="#">
                            <span class="{{$d->icon}}"></span>
                            <div>{{$d->name}}</div>
                        </a>
                    @endforeach
                    <p>{!! str_limit($d->description, 100) !!}</p>

                    <div class="service-overlay">
                        <ul class="clearfix">
                            @foreach($services2 as $d)
                                <li>
                                    <a href="{{route('services.single', $d->slug)}}">
                                        <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End of .service-overlay -->
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="box-green-border">
                    @foreach($categories3 as $d)
                        <a href="#">
                            <span class="{{$d->icon}}"></span>
                            <div>{{$d->name}}</div>
                        </a>
                    @endforeach
                    <p>{!! str_limit($d->description, 100) !!}</p>

                    <div class="service-overlay">
                        <ul class="clearfix">
                            @foreach($services3 as $d)
                                <li>
                                    <a href="{{route('services.single', $d->slug)}}">
                                        <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End of .service-overlay -->
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="box-green-border">
                    @foreach($categories4 as $d)
                        <a href="#">
                            <span class="{{$d->icon}}"></span>
                            <div>{{$d->name}}</div>
                        </a>
                    @endforeach
                    <p>{!! str_limit($d->description, 100) !!}</p>

                    <div class="service-overlay">
                        <ul class="clearfix">
                            @foreach($services4 as $d)
                                <li>
                                    <a href="{{route('services.single', $d->slug)}}">
                                        <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End of .service-overlay -->
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="box-green-border">
                    @foreach($categories5 as $d)
                        <a href="#">
                            <span class="{{$d->icon}}"></span>
                            <div>{{$d->name}}</div>
                        </a>
                    @endforeach
                    <p>{!! str_limit($d->description, 100) !!}</p>

                    <div class="service-overlay">
                        <ul class="clearfix">
                            @foreach($services5 as $d)
                                <li>
                                    <a href="{{route('services.single', $d->slug)}}">
                                        <i class="{{$d->icon}}"></i>{{$d->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End of .service-overlay -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--end services Section-->
