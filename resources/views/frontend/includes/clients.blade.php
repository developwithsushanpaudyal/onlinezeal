

<section class="">
    <div class="container-fluid">
        <!--section title -->
        {{--  <h2 class="b-clor">Our  Valuable Clients</h2>
        <hr class="dark-line" />  --}}
        <!--end section title -->

        <div class="row">
         {{-- @foreach($clients as $client)--}}

            <!--clients section-->
                 <div class="clients">
            <!-- clients item slider-->
                <div class="">
                    <div class="clients-logos owl-carousel owl-theme">
                        @foreach($clients as $client)
                       <div class="item">
                         <img src="{{asset($client->image)}}" alt="{{ $client->title }}" class="img-responsive" title="{{ $client->title }}">
                       </div>
                       @endforeach
                   <!-- End of .item -->
                   <!-- to add more items copy and paste one of
                          the items underneath the last item -->
                   </div>
           <!-- End of .partner_logos -->
         </div>
               <!--end clients item slider -->
     </div>
               

            {{--@endforeach
        </div>

        {{--<div class="col-xs-12 text-center">
            <a href="{{route('portfolio.single')}}" class="btn btn-fill full-width m60">View more Client list</a>
        </div>--}}
        <!--read more blog button-->

</section>


    
