<!-- Section hosting-plans starts -->
<section class="hosting-plans">
    <div class="container">
        <div class="content">
            <h2>High Performance Web Hosting</h2>
            <div class="price-lebel">Starting at
                NRS
                <span class="price">2000</span>
                <sub> /year</sub>
            </div>
            <a href="{{route('hosting')}}" class="btn btn-fill">SEE HOSTING PLANS</a>
        </div>
    </div>
    <!-- End of .container -->
</section>
<!-- End of .hosting-plans -->