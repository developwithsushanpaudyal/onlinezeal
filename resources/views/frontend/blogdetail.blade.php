@include('frontend.includes.header')

<section class="banner  o-hidden banner-inner blog-banner" >
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Blog</h1>
            <p class="semi-bold">We think of us as journalists; the medium we work in is blogging.</p>
            <a href="#blog_item" class="medium-btn3 btn btn-nofill page-scroll">Discover more</a>
        </div>
        <!--end banner text-->
    </div>
</section> 
    <!-- ++++ end Most Bold Title ++++ -->
    <!-- ++++ blog content ++++ -->
    <section id="blog_item" class="bg-white o-hidden blog-content">
        <!--blog content row one-->
        <div class="container">
            <!--section title -->
            <h1 class="gray-title">{{ $blogDetails->title }}</h1>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!--blog content box-->
                <div class="col-xs-12">
                    <div class="blog-details-content details-v2">
                        <ul>
                            <li>
                                <p class="gray-text">
                                    <span class="icon-calendar-full"></span>{{ $blogDetails->created_at->diffForHumans() }}</p>
                            </li>
                           
                            <li>
                                <p class="gray-text">
                                    <span class="icon-list4"></span>
                                    <a href="#">Design category</a>
                                </p>
                            </li>
                           
                            <li>
                                <p class="gray-text">
                                    <a href="">
                                        <i class="icon-user"></i> {{ $blogDetails->author }}</a>
                                </p>
                            </li>
                            
                        </ul>
                        <img src="{{ asset('public/front/images/blog/medium/'.$blogDetails->image) }}" class="img-responsive" alt="blog details" />
                        <div>
                             {!! $blogDetails->body !!}
                        </div>
                        <!--blog share-->
                    </div>

                   
                    <div class="blog-post-share clearfix">
                        <div class="addthis_inline_share_toolbox"></div>

                    </div>
                    <div id="disqus_thread"></div>
                    <br><br>
                    <div class="row">
                            <h5 class="widget-title font-alt">Related Post</h5>
                            @foreach($blogs as $blog_data)
                            <!--blog content box-->
                            <div class="col-xs-12 col-sm-4">
                                <div class="box-content-with-img equalheight is-featured">
                                    <img src="{{ asset('public/front/images/blog/medium/'.$blog_data->image) }}" class="img-responsive" alt="blog image" />
                                    <div class="box-content-text">
                                        <p class="gray-text">
                                            <span class="icon-calendar-full"></span>{{$blog_data->created_at->diffForHumans()}}</p>
                                        <h3 class="semi-bold">
                                            <a href="{{route('blogdetail',$blog_data->slug)}}">{{$blog_data->title}}</a>
                                        </h3>
                                        <!--<p class="regular-text"><p> {!! substr($blog_data->body,0,200) !!} </p>-->
                                        <!--<p> {{strlen($blog_data->body)>200 ? "....":"..."}} </p></p>-->
                                    </div>
                                </div>
                            </div>
                            @endforeach                                                      
                    </div>
                    
                <!--end blog content box-->
            </div>
        </div>
    </section>
  
    
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://http-localhost-laravel-onlinezeal.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


@include('frontend.includes.footer')

