<!-- clients item slider-->

<div class="container">
    <div class="clients-logos owl-carousel owl-theme">
        <div class="item">
            <img src="images/client1.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client2.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client3.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client4.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client5.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client6.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client1.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <div class="item">
            <img src="images/client2.png" alt="partners logo" class="img-responsive">
        </div>
        <!-- End of .item -->
        <!-- to add more items copy and paste one of
        the items underneath the last item -->
    </div>
    <!-- End of .partner_logos -->
</div>
<!--end clients item slider -->
</div>
<!--end clients section-->

@section('scripts')

<script>
    $('.multi-item-carousel').carousel({
        interval: false
      });
      
      // for every slide in carousel, copy the next slide's item in the slide.
      // Do the same for the next, next item.
      $('.multi-item-carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        
        if (next.next().length>0) {
          next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
      });

</script>
@endsection