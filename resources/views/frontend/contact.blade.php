@include('frontend.includes.header')

<section style="position: relative;">

    <div style="padding-top:120px;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m19!1m8!1m3!1d4395.939061897711!2d85.34636155033397!3d27.682807906262738!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x39eb198cb72ca34f%3A0xb9105c2f4f2e4fe9!2snline+zeal+google+maps!3m2!1d27.683542199999998!2d85.3488261!5e0!3m2!1sen!2snp!4v1531830070386"
                width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

</section>

<!-- ++++ contact form design ++++ -->
<section class="bg-white o-hidden common-form-section contact-form-wrapper">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Send Us a Message</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div class="customise-form contact-form">

                    <form class="email_form" method="post" action="{{route('emailme')}}">
                        @csrf
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="name" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group customised-formgroup">
                                    <span class="icon-map-marker"></span>
                                    <input type="text" name="address" class="form-control" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                            <input class="btn btn-success" type="submit" value="SEND MESSAGE">

                    </form>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="contact-info-box-wrapper">
                    <div class="contact-info-box">
                        <span class="icon-telephone"></span>
                        <div>
                            <h6>Give us a call</h6>
                            <p>{{$contact->phone}} , {{$contact->mobile}}</p>
                        </div>
                    </div>
                    <div class="contact-info-box">
                        <span class="icon-envelope"></span>
                        <div>
                            <h6>Send an email</h6>
                            <p>{{$contact->email}}</p>
                        </div>
                    </div>
                    <div class="contact-info-box">
                        <ul class="social-links">
                            <li>
                                <a href="{{$contact->facebook}}" target="_blank">
                                    <span class="icon-facebook"></span>
                                </a>
                            </li>
                            <li>
                                <a href="{{$contact->twitter}}" target="_blank">
                                    <span class="icon-twitter"></span>
                                </a>
                            </li>

                            <!-- <li>
                              <a href="#">
                                  <span class="icon-instagram"></span>
                              </a>
                          </li> -->

                            <li>
                                <a href="{{$contact->linkedin}}" target="_blank">
                                    <span class="icon-linkedin"></span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="#">
                                    <span class="icon-google-plus"></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <span class="icon-pinterest"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon-linkedin"></span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end contact form design -->



<!--get a quote modal-->
<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-marker"></span>
                                    <input type="text" name="address" class="form-control" placeholder="address">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                        <span class="icon-chevron-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time & cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end get a quote modal-->


@include('frontend.includes.footer')
