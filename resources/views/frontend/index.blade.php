
@include('frontend.includes.header')

@include('frontend.includes.slider')

@include('frontend.includes.clients')

@include('frontend.includes.services')

@include('frontend.includes.portfolio')

@include('frontend.includes.counter')

@include('frontend.includes.hosting')



<!--get a quote modal-->
<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post" action="{{route('getaquote.store')}}" data-autosubmit>
                                @csrf
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="username" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="useremail" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="userphone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="userwebsite" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="usermessage" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <input type="submit" class="btn btn-fill full-width" name="submit">GET A QUOTE
                                        <span class="icon-chevron-right"></span>
                                </div> 
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time &amp; cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--end get a quote modal-->






@include('frontend.includes.footer')
