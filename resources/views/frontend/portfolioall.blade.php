@include('frontend.includes.header')

<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner portfolio-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Portfolio</h1>
            <p class="semi-bold">We use strategic approaches to provide our clients with high-end.
                <br /> services that ensure superior customer satisfaction.</p>
            <a href="#portfolio-home" class="medium-btn2 btn btn-nofill page-scroll">DISCOVER MORE</a>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->

<!--portfolio section-->
<section class="portfolio-full portfolio clearfix" id="portfolio-home">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Full Portfolio</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="button-group filter-button-group clearfix">
            <button class="button is-checked" data-filter="*">All Work</button>
            @foreach($portfoliocategories as $portfoliocategory)
                <button class="button " data-filter=".{{$portfoliocategory->name}}">{{$portfoliocategory->name}}</button>
        @endforeach
        </div>
        <!-- button-group ends -->
        <div class="grid clearfix row">
            <!-- Items for portfolio
            currently using 12 items. More Items can be added. -->
            @foreach($portfolios as $port)
                <div class="{{$port->categories}} grid-item">
                    <div class="img_container">
                        <img src="{{asset('public/front/images/portfolio/small/'.$port->image)}}" alt="port_img" class="img-responsive">
                        <div class="overlay">
                             <a class="btn btn-nofill" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#portfolioDetModal{{$port->id}}">View</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a data-toggle="modal" data-target="#portfolioDetModal{{ $port->id }}" >{{$port->name}}


                            </a>
                        </h3>
                    </div>
                </div>
        <!-- End of .grid-item -->

      

<div class="modal fade verticl-center-modal" id="portfolioDetModal{{$port->id}}" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal{{$port->id}}">
    <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="icon-cross-circle"></span>
            </button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <!-- main slider carousel -->
                        <div id="slider">
                            <div id="carousel-bounding-box">
                                <div id="myCarousel" class="carousel slide myCarousel">
                                    <!-- main slider carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item" data-slide-number="0">
                                            <img src="{{asset('public/front/images/portfolio/large/'.$port->image)}}" alt="images" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="1">
                                            <img src="{{asset('public/front/images/portfolio/large/'.$port->image)}}" alt="images" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="2">
                                            <img src="{{asset('public/front/images/portfolio/large/'.$port->image)}}" alt="images" class="img-responsive">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="port-modal-content">
                            <p class="gray-text">{{$port->categories}}</p>
                            <h2 class="b-clor">{{$port->name}}</h2>
                            <p class="regular-text">
                          
                              {!! $port->description !!}
                        </div>
                        
                        <a href="{{$port->link}}" class="medium-btn2  btn btn-fill">Launch website</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End of .grid-item -->
<div class="school col-md-4 grid-item">
    <div class="img_container">
         <img src="{{asset('public/front/images/portfolio/school.jpg')}}" alt="schools" class="img-responsive">
            <div class="overlay">
             <a class="btn btn-nofill" href="{{route('schoolprojects') }}">View</a>
        </div>
    <!-- End of .overlay -->
    </div>
    <!-- End of .img_container -->
    <div class="text-content">
       <h3>
            <a href="{{route('schoolprojects')}}">School and colleges
                <span>Website</span>
            </a>
        </h3>
     </div>
</div>


</div>
<!-- End of .grid -->
<div class="text-center port-dms">
<a href="{{route('portfolio.single')}}" class="dmb-p btn btn-fill full-width">View more</a>
</div>
</div>



<!--get a quote modal-->
<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                        <span class="icon-chevron-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time &amp; cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end get a quote modal-->

@include('frontend.includes.footer')
