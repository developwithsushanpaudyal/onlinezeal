
@include('frontend.includes.serviceheader')


<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner website-banner" style="background: url({{asset($service->image)}});
">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>{{$service->title}}</h1>
            <p class="semi-bold">{{$service->subtitle}}</p>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++ responsive web design ++++ -->
<section class="bg-white o-hidden common-form-section  service-description-section">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">{{$service->title}}</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">

                <p class="regular-text"> {!! $service->content !!}   </p>

            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="customise-form">
                    <form class="email_form" method="post" action="{{route('emailme')}}">
                    <div class="form-element-wrapper">
                            <h3>Get a Free Quote</h3>
                            <div class="form-group customised-formgroup">
                                <span class="icon-user"></span>
                                <input type="text" name="name" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-envelope"></span>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-telephone"></span>
                                <input type="text" name="phone" class="form-control" placeholder="Phone">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-laptop"></span>
                                <input type="text" name="address" class="form-control" placeholder="Address">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-bubble"></span>
                                <textarea name="message" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                <span class="icon-chevron-right"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ++++ end responsive web design ++++ -->


@section('styles')
    <style>
        li {
            list-style: disc;
        }
    </style>
@endsection


@include('frontend.includes.footer')
