@include('frontend.includes.header')
<!-- ++++ Most Bold Title ++++ -->
<section class="banner  o-hidden banner-inner join-our-team-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Team</h1>
            <p class="semi-bold">Grow your business online by awesomely designed mobile apps
                <br /> that fits all types.</p>
            <a href="#team_members" class="medium-btn3 btn btn-nofill page-scroll">Meet our team</a>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++ Mobile first concept ++++ -->
<section id="team_members" class="bg-white team_members">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor"> It Takes Two Flints to Make a Fire</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            @foreach($teams as $team )
            <div class="col-md-4 col-xs-6">
                <div class="content">
                    <div class="img_container">
                        <img src="{{asset('public/uploads/team/medium/'.$team->image)}}" alt="team members" class="img-responsive">
                        <div class="por-overlay">
                            <div class="text-inner">
                                <a class="btn btn-nofill tmDetModal{{ $team->id}}" data-target="#teamDetModal{{ $team->id }}">View Details</a>
                            </div>
                            <!-- End of .text-inner -->
                        </div>
                        <!-- End of .por-overlay -->
                    </div>
                    <!-- End of .img_container -->
                    
                    <div class="member_details">
                        <h3>
                            <a class="member_intro ">{{ $team->name }}</a>{{ $team->designation }}</h3>
                    </div>
                   
                    <!-- End of .member_details -->
                </div>
                <!-- End of .content -->
            </div>
                <div class="modal fade verticl-center-modal{{ $team->id }}" id="teamDetModal{{ $team->id }}" tabindex="-1" role="dialog" aria-labelledby="teamDetModal">
                    <div class="modal-dialog getguoteModal-dialog potfolio-modal team-modal" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="icon-cross-circle"></span>
                            </button>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="img_container">
                                            <img src="{{asset('public/uploads/team/large/'.$team->image)}}" alt="team details image" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="port-modal-content team-modal-content">
                                            <h2 class="b-clor">{{ $team->name }}</h2>
                                            <p class="gray-text">{{ $team->designation }}</p>
                                            <p class="regular-text">{!! $team->description !!}</p>
                                            <ul class="list-inline social_icons text-left">
                                                <li>
                                                <a href="{{$team->facebook}}" target="_blank" title ="Facebook" class="text-center">
                                                        <i class="icon-facebook"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{$team->twitter}}" target="_blank" title ="Twitter" class="text-center">
                                                        <i class="icon-twitter"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{$team->instagram}}" target="_blank"  title ="Instagram"class="text-center">
                                                        <i class="icon-instagram"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{$team->pinterest}}" target="_blank" title ="Pinterest" class="text-center">
                                                        <i class="icon-pinterest"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{$team->linkedin}}" target="_blank"  title ="LinkedIn"class="text-center">
                                                        <i class="icon-linkedin"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            @endforeach
            <!-- End of .col-sm-4 -->
        </div>
        <!-- End of .row -->
        {{--<div class="col-sm-12 text-center join-our-team-btn clearfix">
            <a href="career.html" class="btn btn-fill full-width">Join Our Team</a>
        </div>--}}
    </div>
    <!-- End of .container -->
</section>
@include('frontend.includes.footer')