@include('frontend.includes.header')
@if(isset($details))
<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner portfolio-banner" >
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Search Results</h1>

        </div>
        <!--end banner text-->
    </div>
    <!-- End of .container -->
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++ Search-results ++++ -->
<section class="search-results">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor"> {{$details->count()}} search results found</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            @foreach($details as $service)
                <a href="{{route('services.single', $service->slug)}}">
                    <div class="col-sm-6 col-md-4">
                        <div class="content">
                            <h3>{{$service->title}}</h3>
                            <p class="regular-text">{!! substr($service->content , 0 ,200) !!} {!! strlen($service->content )>200? "..." : "" !!}</p>
                        </div>
                        <!-- End of .content -->
                    </div>
                </a>
            @endforeach

            <!-- End of .col-sm-4 -->
        </div>

    <!-- End of .row -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .search-results -->
@elseif(isset($message))
<!-- ++++ contact-us-now ++++ -->
<br><br>
<section class="contact-us-now grey-dark-bg">
    <div class="container text-center">
        <h3>{{$message}}</h3>
        <a href="{{route('contact')}}" class="btn btn-fill">contact us now</a>
    </div>
    <!-- End of .container -->
</section>
<!-- End of .search-results -->
@endif


@include('frontend.includes.footer')
