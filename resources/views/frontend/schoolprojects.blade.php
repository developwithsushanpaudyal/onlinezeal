@include('frontend.includes.header')


<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner portfolio-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Portfolio</h1>
            <p class="semi-bold">We use strategic approaches to provide our clients with high-end.
                <br> services that ensure superior customer satisfaction.</p>
            <!-- <a href="#more-portfolio" class="medium-btn2 btn btn-nofill page-scroll">DISCOVER MORE</a> -->
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++  join-our-team ++++ -->
<section id="faqs" class="faqs">
    <div class="container">
        <!--section title -->
        <div class="faqs-content bg-white">
            <h2 class="b-clor">Websites</h2>
            <hr class="dark-line">

            <div class="accordion-section">

                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse-parent-next" href="#">
                                    Government Schools

                                    <i class="more-less icon-plus"></i>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse">
                            <div class="panel-body">

                                <table class="table table-striped table-portfolio table-responsive hover" >

                                    <tr>
                                        <th>School's Name</th>
                                        <th>Website</th>
                                        <th>School logo </th>
                                    </tr>
                         @foreach($schools1 as $school)
                                    <tr>
                                        <td>{{$school->name}}</td>
                                    <td> <a class="portfolioDetModal{{ $school->id}}" data-toggle="modal" data-target="#portfolioDetModal{{ $school->id }}"  > {{$school->website}}</a>  </td>
                                        <td> <img src="{{asset('public/uploads/schools/logo/small/'.$school->logo)}}" alt="schools" class="img-responsive"> </td> 
                                    </tr>
                                            
    <div class="modal fade verticl-center-modal{{ $school->id }}" id="portfolioDetModal{{ $school->id }}" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal">
            <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="icon-cross-circle"></span>
                    </button>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-7">
                                <img src="{{asset('public/uploads/schools/logo/medium/'.$school->logo)}}" alt="images" class="img-responsive">
                            </div>
                            <div class="col-md-5">
                                <div class="port-modal-content">
                                    {{--<p class="gray-text">Featured - Design</p>--}}
                                    <h2 class="b-clor">{{ $school->name }}</h2>
                                    <p class="regular-text">{!! $school->description !!}</p>
                                </div>
                            {{-- <h3>We delivered:</h3>
                                <ul class="list-with-arrow">
                                    <li>
                                        <i class="icon-chevron-right"></i> A strategy to grow business online.</li>
                                    <li>
                                        <i class="icon-chevron-right"></i> A unique website with great user experience design.</li>
                                    <li>
                                        <i class="icon-chevron-right"></i> A custom content management system to maintain
                                        <br> the website flowlessly. </li>
                                </ul>--}}
                                <a href="{{$school->website}}" target ="_blank" class="medium-btn2  btn btn-fill">Launch website</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ++++ end join our team ++++ -->
    
                             @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse-parent-next" href="#">
                                    Other Websites

                                    <i class="more-less icon-plus"></i>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table table-striped table-portfolio table-responsive hover" >

                                    <tr>
                                        <th>Company</th>
                                        <th>Website</th>
                                        <th>School logo </th>
                                    </tr>
                                    @foreach($schools2 as $school)
                                    <tr>
                                        <td>{{$school->name}}</td>
                                        <td> <a class="portfolioDetModal{{ $school->id}}" data-toggle="modal" data-target="#portfolioDetModal{{ $school->id }}"> {{$school->website}} </a>  </td>
                                        <td> <img src="{{asset('public/uploads/schools/logo/small/'.$school->logo)}}" alt="schools" class="img-responsive"> </td> 

                                    </tr>
                                    
<div class="modal fade verticl-center-modal{{ $school->id }}" id="portfolioDetModal{{ $school->id }}" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal">
    <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="icon-cross-circle"></span>
            </button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <img src="{{asset('public/uploads/schools/logo/medium/'.$school->logo)}}" alt="schools" class="img-responsive">
                    </div>
                    <div class="col-md-5">
                        <div class="port-modal-content">
                            {{--<p class="gray-text">Featured - Design</p>--}}
                            <h2 class="b-clor">{{ $school->name }}</h2>
                            <p class="regular-text">{!! $school->description !!}</p>
                        </div>
                       {{-- <h3>We delivered:</h3>
                        <ul class="list-with-arrow">
                            <li>
                                <i class="icon-chevron-right"></i> A strategy to grow business online.</li>
                            <li>
                                <i class="icon-chevron-right"></i> A unique website with great user experience design.</li>
                            <li>
                                <i class="icon-chevron-right"></i> A custom content management system to maintain
                                <br> the website flowlessly. </li>
                        </ul>--}}
                    <a href="{{$school->website}}" target ="_blank" class="medium-btn2  btn btn-fill ">Launch website</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ++++ end join our team ++++ -->

                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>


                </div><!--panel group-->


            </div>
            <!-- End of .accordion-section -->





        </div>
        <!-- End of .faqs-content -->



    </div>
    <!-- End of .container -->
</section>

@include('frontend.includes.footer')
