@include('frontend.includes.header')

<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner join-our-team-banner" style="background: url({{asset($carrer->image)}});
">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Career</h1>
            <p class="semi-bold">{{$carrer->subtitle}}</p>
            <a href="#join-our-team" class="medium-btn2 btn btn-nofill page-scroll">discover more</a>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++  join-our-team ++++ -->
<section id="join-our-team" class="bg-white o-hidden common-form-section join-our-team">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Why Join Our Team?</h2>
        <hr class="dark-line" />
        <p class="regular-text">{!! $carrer->description !!}</p>
        <!--end section title -->

    </div>
    <!-- End of .container -->
</section>
<!-- ++++ end join our team ++++ -->
<!-- ++++  available-positions ++++ -->
<section class="bg-white o-hidden common-form-section available-positions">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Available Positions</h2>
        <hr class="dark-line" />
        <div class="row">
  @foreach($positions as $position)
            <div class="col-sm-6 col-md-4">
                <div class="content">
                    <h3> {{$position->position}}
                        <span>department: {{$position->department}}</span>
                    </h3>
                    <div class="inner_content">
                        <p class="regular-text">
                            {!! substr($position->information , 0 ,200) !!} {!! strlen($position->information )>200? "..." : "" !!}
                        </p>
                        <a class="btn btn-fill proDetModal">view details</a>
                    </div>
                    <!-- End of .inner_content -->
                </div>
                <!-- End of .content -->
            </div>
@endforeach
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</section>
<!-- ++++ end service description ++++ -->

<!--portfolio details  modal-->
<div class="modal fade verticl-center-modal" id="portfolioDetModal" tabindex="-1" role="dialog" aria-labelledby="portfolioDetModal">
    <div class="modal-dialog getguoteModal-dialog potfolio-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            @foreach($positions as $position)

            <div class="modal-body">
                <div class="port-modal-content team-modal-content">
                    <h2 class="b-clor">{{$position->position}}</h2>
                    <p class="gray-text">Department: {{$position->department}} | Number of Positions: {{$position->positionnumber}}</p>
                    <p class="regular-text">{!! $position->information !!}</p>

                </div>
            </div>
            <!-- End of .modal-body -->
            @endforeach

        </div>
    </div>
</div>
<!--end portfolio details modal-->




@include('frontend.includes.footer')




