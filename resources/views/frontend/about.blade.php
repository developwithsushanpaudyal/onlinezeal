@include('frontend.includes.header')

<!-- ++++ banner ++++ -->
<section class="banner  o-hidden banner-inner about-banner" style="background: url({{asset($about->cover_image)}});">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>About Us</h1>
            <p class="semi-bold">{{$about->slogan}}</p>
            <a href="#more-about" class="medium-btn2 btn btn-nofill page-scroll">EXPERIENCE US</a>
        </div>
        <!--end banner text-->
    </div>
</section>
<!-- ++++ end banner ++++ -->
<!-- ++++ about content ++++ -->
<section class="bg-white o-hidden common-form-section contact-form-wrapper" id="more-about">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">About OnlineZeal</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row about-content">
            <div class="col-md-5 col-sm-12 pull-right">
                <div class="about-img">
                    <img src="{{asset($about->image)}}" alt="about" class="img-responsive" /> </div>
            </div>
            <div class="col-md-7 col-sm-12 pull-left">
                <div>
                    <h4 class="b-clor mb-10">Our Mision</h4>
                    <p class="regular-text">{{$about->our_mission}}</p>

                    <h4 class="b-clor mb-10">Our Vission</h4>
                    <p class="regular-text">{{$about->our_vision}}</p>
                    <!-- <p class="regular-text">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</p> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end contact form design -->


<!--Choosing Us Section-->
<section class="bg-white o-hidden dmb-home why-us common-form-section" id="why-us">
    <div class="container">
        <!--section title -->
        <h2 class="b-clor">Reasons for Choosing Us</h2>
        <hr class="dark-line" />
        <!--end section title -->
        <div class="row">
            <!--text box-->
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                <h3 class="semi-bold">Our Objectives</h3>
                <p class="regular-text">
                       {{$about->objectives}}
                </p>


                <h3 class="semi-bold">Why to Choose Us?</h3>

                <p class="regular-text">                       {{$about->objectives}}
                </p>
            </div>
            <!--end text box-->
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
            <!--form for free quote-->
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="customise-form">
                    <form class="email_form" method="post" action="{{route('emailme')}}">
                        @csrf
                        <div class="form-element-wrapper">
                            <h3>Get a Free Quote</h3>
                            <div class="form-group customised-formgroup">
                                <span class="icon-user"></span>
                                <input type="text" name="name" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-envelope"></span>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-telephone"></span>
                                <input type="text" name="phone" class="form-control" placeholder="Phone">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-laptop"></span>
                                <input type="text" name="address" class="form-control" placeholder="Address">
                            </div>
                            <div class="form-group customised-formgroup">
                                <span class="icon-bubble"></span>
                                <textarea name="message" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                <span class="icon-chevron-right"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!--end form for free quote-->
        </div>
    </div>
</section>
<!--end Choosing Us section-->



<!--get a quote modal-->
<div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
    <div class="modal-dialog getguoteModal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customise-form">
                            <form class="email_form" method="post" action="{{route('getaquote.store')}}" data-autosubmit>
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                        <span class="icon-chevron-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>What’s Next?</h3>
                        <ul class="list-with-arrow">
                            <li>An email and phone call from one of our representatives.</li>
                            <li>A time & cost estimation.</li>
                            <li>An in-person meeting.</li>
                        </ul>
                        <div class="contact-info-box-wrapper">
                            <div class="contact-info-box">
                                <span class="icon-telephone"></span>
                                <div>
                                    <h6>Give us a call</h6>
                                    <p>(123) 456 7890</p>
                                </div>
                            </div>
                            <div class="contact-info-box">
                                <span class="icon-envelope"></span>
                                <div>
                                    <h6>Send an email</h6>
                                    <p>yourcompany@sample.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end get a quote modal-->


@include('frontend.includes.footer')
